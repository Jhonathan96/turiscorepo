// To parse this JSON data, do
//
//     final getSaveSubCategories = getSaveSubCategoriesFromJson(jsonString);

import 'dart:convert';

GetSaveSubCategories getSaveSubCategoriesFromJson(String str) => GetSaveSubCategories.fromJson(json.decode(str));

String getSaveSubCategoriesToJson(GetSaveSubCategories data) => json.encode(data.toJson());

class GetSaveSubCategories {
  GetSaveSubCategories({
    this.code,
    this.data,
    this.message,
  });

  int code;
  Data data;
  String message;

  factory GetSaveSubCategories.fromJson(Map<String, dynamic> json) => GetSaveSubCategories(
    code: json["code"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.categories,
  });

  List<Category> categories;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
  };
}

class Category {
  Category({
    this.categoryId,
    this.categoryName,
    this.categoryColor,
    this.subcategories,
  });

  String categoryId;
  String categoryName;
  String categoryColor;
  List<Subcategory> subcategories;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    categoryId: json["categoryId"],
    categoryName: json["categoryName"],
    categoryColor: json["categoryColor"],
    subcategories: List<Subcategory>.from(json["subcategories"].map((x) => Subcategory.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categoryId": categoryId,
    "categoryName": categoryName,
    "categoryColor": categoryColor,
    "subcategories": List<dynamic>.from(subcategories.map((x) => x.toJson())),
  };
}

class Subcategory {
  Subcategory({
    this.subcategoryId,
    this.subcategoryName,
    this.userSubcategoryId,
    this.status,
    this.subColor
  });

  String subcategoryId;
  String subcategoryName;
  String userSubcategoryId;
  String status;
  String subColor;

  factory Subcategory.fromJson(Map<String, dynamic> json) => Subcategory(
    subcategoryId: json["subcategoryId"],
    subcategoryName: json["subcategoryName"],
    userSubcategoryId: json["userSubcategoryId"],
    status: json["status"],
    subColor: json["subColor"]
  );

  Map<String, dynamic> toJson() => {
    "subcategoryId": subcategoryId,
    "subcategoryName": subcategoryName,
    "userSubcategoryId": userSubcategoryId,
    "status": status,
  };
}
