// To parse this JSON data, do
//
//     final myTravelsResponse = myTravelsResponseFromJson(jsonString);

import 'dart:convert';

MyTravelsResponse myTravelsResponseFromJson(String str) => MyTravelsResponse.fromJson(json.decode(str));

String myTravelsResponseToJson(MyTravelsResponse data) => json.encode(data.toJson());

class MyTravelsResponse {
  MyTravelsResponse({
    this.code,
    this.data,
    this.message,
  });

  int code;
  Data data;
  String message;

  factory MyTravelsResponse.fromJson(Map<String, dynamic> json) => MyTravelsResponse(
    code: json["code"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.sites,
    this.total,
  });

  List<Site> sites;
  int total;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    sites: List<Site>.from(json["sites"].map((x) => Site.fromJson(x))),
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "sites": List<dynamic>.from(sites.map((x) => x.toJson())),
    "total": total,
  };
}

class Site {
  Site({
    this.siteId,
    this.siteName,
    this.locality,
    this.owner,
    this.cityDistance,
    this.siteDescription,
    this.latitude,
    this.longitude,
    this.image,
    this.siteStatus,
    this.siteCreatedAt,
    this.subcategoryId,
    this.subcategoryName,
    this.subcategoryStatus,
    this.categoryId,
    this.categoryName,
    this.categoryStatus,
    this.cityId,
    this.cityName,
    this.cityStatus,
    this.myQualification,
    this.distance,
    this.qualification,
    this.isMyFavorite,
  });

  String siteId;
  String siteName;
  String locality;
  String owner;
  String cityDistance;
  String siteDescription;
  String latitude;
  String longitude;
  String image;
  String siteStatus;
  DateTime siteCreatedAt;
  String subcategoryId;
  String subcategoryName;
  String subcategoryStatus;
  String categoryId;
  String categoryName;
  String categoryStatus;
  String cityId;
  String cityName;
  String cityStatus;
  int myQualification;
  dynamic distance;
  int qualification;
  bool isMyFavorite;

  factory Site.fromJson(Map<String, dynamic> json) => Site(
    siteId: json["siteId"],
    siteName: json["siteName"],
    locality: json["locality"],
    owner: json["owner"],
    cityDistance: json["cityDistance"],
    siteDescription: json["siteDescription"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    image: json["image"],
    siteStatus: json["siteStatus"],
    siteCreatedAt: DateTime.parse(json["siteCreatedAt"]),
    subcategoryId: json["subcategoryId"],
    subcategoryName: json["subcategoryName"],
    subcategoryStatus: json["subcategoryStatus"],
    categoryId: json["categoryId"],
    categoryName: json["categoryName"],
    categoryStatus: json["categoryStatus"],
    cityId: json["cityId"],
    cityName: json["cityName"],
    cityStatus: json["cityStatus"],
    myQualification: json["myQualification"],
    distance: json["distance"],
    qualification: json["qualification"],
    isMyFavorite: json["isMyFavorite"],
  );

  Map<String, dynamic> toJson() => {
    "siteId": siteId,
    "siteName": siteName,
    "locality": locality,
    "owner": owner,
    "cityDistance": cityDistance,
    "siteDescription": siteDescription,
    "latitude": latitude,
    "longitude": longitude,
    "image": image,
    "siteStatus": siteStatus,
    "siteCreatedAt": siteCreatedAt.toIso8601String(),
    "subcategoryId": subcategoryId,
    "subcategoryName": subcategoryName,
    "subcategoryStatus": subcategoryStatus,
    "categoryId": categoryId,
    "categoryName": categoryName,
    "categoryStatus": categoryStatus,
    "cityId": cityId,
    "cityName": cityName,
    "cityStatus": cityStatus,
    "myQualification": myQualification,
    "distance": distance,
    "qualification": qualification,
    "isMyFavorite": isMyFavorite,
  };
}
