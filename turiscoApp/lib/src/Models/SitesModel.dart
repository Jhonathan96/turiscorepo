// To parse this JSON data, do
//
//     final sitesResponse = sitesResponseFromJson(jsonString);

import 'dart:convert';

SitesResponse sitesResponseFromJson(String str) => SitesResponse.fromJson(json.decode(str));

String sitesResponseToJson(SitesResponse data) => json.encode(data.toJson());

class SitesResponse {
  SitesResponse({
    this.code,
    this.data,
    this.message,
  });

  int code;
  Data data;
  String message;

  factory SitesResponse.fromJson(Map<String, dynamic> json) => SitesResponse(
    code: json["code"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.sites,
  });

  List<Site> sites;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    sites: List<Site>.from(json["sites"].map((x) => Site.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "sites": List<dynamic>.from(sites.map((x) => x.toJson())),
  };
}

class Site {
  Site({
    this.siteId,
    this.siteName,
    this.locality,
    this.owner,
    this.cityDistance,
    this.siteDescription,
    this.latitude,
    this.longitude,
    this.image,
    this.siteStatus,
    this.siteCreatedAt,
    this.subcategoryId,
    this.subcategoryName,
    this.subcategoryStatus,
    this.categoryId,
    this.categoryName,
    this.categoryStatus,
    this.cityId,
    this.cityName,
    this.cityStatus,
    this.qualification,
    this.distance,
    this.myQualification,
    this.isMyFavorite,
  });

  String siteId;
  String siteName;
  String locality;
  String owner;
  String cityDistance;
  String siteDescription;
  String latitude;
  String longitude;
  String image;
  Status siteStatus;
  DateTime siteCreatedAt;
  String subcategoryId;
  String subcategoryName;
  Status subcategoryStatus;
  String categoryId;
  CategoryName categoryName;
  Status categoryStatus;
  String cityId;
  String cityName;
  Status cityStatus;
  int qualification;
  double distance;
  String myQualification;
  bool isMyFavorite;

  factory Site.fromJson(Map<String, dynamic> json) => Site(
    siteId: json["siteId"],
    siteName: json["siteName"],
    locality: json["locality"],
    owner: json["owner"],
    cityDistance: json["cityDistance"],
    siteDescription: json["siteDescription"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    image: json["image"],
    siteStatus: statusValues.map[json["siteStatus"]],
    siteCreatedAt: DateTime.parse(json["siteCreatedAt"]),
    subcategoryId: json["subcategoryId"],
    subcategoryName: json["subcategoryName"],
    subcategoryStatus: statusValues.map[json["subcategoryStatus"]],
    categoryId: json["categoryId"],
    categoryName: categoryNameValues.map[json["categoryName"]],
    categoryStatus: statusValues.map[json["categoryStatus"]],
    cityId: json["cityId"],
    cityName: json["cityName"],
    cityStatus: statusValues.map[json["cityStatus"]],
    qualification: json["qualification"] == null ? null : json["qualification"],
    distance: json["distance"].toDouble(),
    myQualification: json["myQualification"].toString(),
    isMyFavorite: json["isMyFavorite"],
  );

  Map<String, dynamic> toJson() => {
    "siteId": siteId,
    "siteName": siteName,
    "locality": locality,
    "owner": owner,
    "cityDistance": cityDistance,
    "siteDescription": siteDescription,
    "latitude": latitude,
    "longitude": longitude,
    "image": image,
    "siteStatus": statusValues.reverse[siteStatus],
    "siteCreatedAt": siteCreatedAt.toIso8601String(),
    "subcategoryId": subcategoryId,
    "subcategoryName": subcategoryNameValues.reverse[subcategoryName],
    "subcategoryStatus": statusValues.reverse[subcategoryStatus],
    "categoryId": categoryId,
    "categoryName": categoryNameValues.reverse[categoryName],
    "categoryStatus": statusValues.reverse[categoryStatus],
    "cityId": cityId,
    "cityName": cityName,
    "cityStatus": statusValues.reverse[cityStatus],
    "qualification": qualification == null ? null : qualification,
    "distance": distance,
    "myQualification": myQualification,
    "isMyFavorite": isMyFavorite,
  };
}

enum CategoryName { TURISMO_CULTURAL }

final categoryNameValues = EnumValues({
  "Turismo Cultural": CategoryName.TURISMO_CULTURAL
});

enum Status { ACTIVE }

final statusValues = EnumValues({
  "ACTIVE": Status.ACTIVE
});

enum SubcategoryName { MUSEOS, PATRIMONIO_ARQUITECTNICO, ARTESANIAS }

final subcategoryNameValues = EnumValues({
  "Artesanias": SubcategoryName.ARTESANIAS,
  "Museos": SubcategoryName.MUSEOS,
  "Patrimonio Arquitectónico": SubcategoryName.PATRIMONIO_ARQUITECTNICO
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
