

class ResponseAccessToken {
  int code=0;
  DataResponseAccesToken data;
  String message = '';

  ResponseAccessToken.fromJsonMap(Map<String,dynamic> json){
    code = json["code"];
    message = json["message"];
    data = DataResponseAccesToken.fromJsonMap(json["data"]);

  }

  Map<String, dynamic> toJson() => {
      "code":code
  };
}

class DataResponseAccesToken {
  String accessToken = '';

  DataResponseAccesToken.fromJsonMap(Map<String,dynamic> json){
    accessToken = json['accessToken'];

  }
  Map<String, dynamic> toJson() => {
    "accessToken":accessToken
  };

}

class UserSigInUpResponse {

  String uid;
  String fullName;
  String email;
  String accountType;
  String tries;
  String valid;
  String image;
  String description;
  RoleResponse role;

  UserSigInUpResponse.fromJsonMap(Map<String,dynamic> json){
    uid = json["uid"];
    fullName = json["fullName"];
    email = json["email"];
    accountType = json["accountType"];
    tries = json["tries"];
    valid = json["valid"];
    image = json["image"];
    description = json["description"];
    role = RoleResponse.fromJsonMap(json['role']);
  }

  Map<String, dynamic> toJson() => {
    "uid":uid,
    "fullName": fullName,
    "email": email,
    "accountType": accountType,
    "tries":tries,
    "valid":valid,
    "image":image,
    "description":description,
    "role":role.toJson(),
  };

}


class UpdateProfileResponse{
  int code;
  UserSigInUpResponse user;
  String message;

  UpdateProfileResponse.fromJsonMap(Map<String,dynamic> json){
    user =  UserSigInUpResponse.fromJsonMap(json['data']);
    code = json['code'];
    message = code != 100 ? json['message'] : null;

  }
  Map<String, dynamic> toJson() => {
    "user":user.toJson(),
    "code":code

  };
}

class DataSignUpResponse {
  UserSigInUpResponse user;
  String authToken;


  DataSignUpResponse.fromJsonMap(Map<String,dynamic> json){
    user = UserSigInUpResponse.fromJsonMap(json['user']);
    authToken = json['authToken'] ;

  }
  Map<String, dynamic> toJson() => {
    "user":user.toJson(),
    "authToken":authToken

  };


}

class RoleResponse {
  String roleId;
  String name;
  String status;

  RoleResponse.fromJsonMap(Map<String,dynamic> json){
    roleId = json["roleId"];
    name = json["name"];
    status = json["status"];
  }

  Map<String, dynamic> toJson() => {
    "roleId":roleId,
    "name": name,
    "status": status
  };
}


class SignUpResponse {
 int code;
 DataSignUpResponse data;
 String message;

 SignUpResponse.fromJsonMap(Map<String,dynamic> json){
   code = json['code'];
   message = code != 100 ? json['message'] : null;
   data = code == 100 ? DataSignUpResponse.fromJsonMap(json['data']): null;
  }

 Map<String, dynamic> toJson() => {
   "code":code,
   "data": data.toJson()
 };



}


