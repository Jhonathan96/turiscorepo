// To parse this JSON data, do
//
//     final ratingSiteResponse = ratingSiteResponseFromJson(jsonString);

import 'dart:convert';

RatingSiteResponse ratingSiteResponseFromJson(String str) => RatingSiteResponse.fromJson(json.decode(str));

String ratingSiteResponseToJson(RatingSiteResponse data) => json.encode(data.toJson());

class RatingSiteResponse {
  RatingSiteResponse({
    this.code,
    this.data,
    this.message,
  });

  int code;
  Data data;
  String message;

  factory RatingSiteResponse.fromJson(Map<String, dynamic> json) => RatingSiteResponse(
    code: json["code"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.create,
  });

  int create;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    create: json["create"],
  );

  Map<String, dynamic> toJson() => {
    "create": create,
  };
}
