// To parse this JSON data, do
//
//     final saveSubCategoriesResponse = saveSubCategoriesResponseFromJson(jsonString);

import 'dart:convert';

SaveSubCategoriesResponse saveSubCategoriesResponseFromJson(String str) => SaveSubCategoriesResponse.fromJson(json.decode(str));

String saveSubCategoriesResponseToJson(SaveSubCategoriesResponse data) => json.encode(data.toJson());

class SaveSubCategoriesResponse {
  SaveSubCategoriesResponse({
    this.code,
    this.data,
    this.message,
  });

  int code;
  Data data;
  String message;

  factory SaveSubCategoriesResponse.fromJson(Map<String, dynamic> json) => SaveSubCategoriesResponse(
    code: json["code"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.subcategories,
  });

  List<String> subcategories;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    subcategories: List<String>.from(json["subcategories"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "subcategories": List<dynamic>.from(subcategories.map((x) => x)),
  };
}
