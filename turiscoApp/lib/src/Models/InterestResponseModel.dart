import 'dart:convert';

InterestResponse interestResponseFromJson(String str) => InterestResponse.fromJson(json.decode(str));

String interestResponseToJson(InterestResponse data) => json.encode(data.toJson());

class InterestResponse {
  InterestResponse({
    this.code,
    this.data,
    this.message,
  });

  int code;
  Data data;
  String message;

  factory InterestResponse.fromJson(Map<String, dynamic> json) => InterestResponse(
    code: json["code"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.categories,
  });

  List<Category> categories;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    categories: List<Category>.from(json["categories"].map((x) => Category.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categories": List<dynamic>.from(categories.map((x) => x.toJson())),
  };
}

class Category {
  Category({
    this.categoryId,
    this.categoryName,
    this.categoryColor,
    this.categoryStatus,
    this.subcategories,
  });

  String categoryId;
  String categoryName;
  String categoryColor;
  String categoryStatus;
  List<Subcategory> subcategories;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    categoryId: json["categoryId"],
    categoryName: json["categoryName"],
    categoryColor: json["categoryColor"],
    categoryStatus: json["categoryStatus"],
    subcategories: List<Subcategory>.from(json["subcategories"].map((x) => Subcategory.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "categoryId": categoryId,
    "categoryName": categoryName,
    "categoryColor": categoryColor,
    "categoryStatus": categoryStatus,
    "subcategories": List<dynamic>.from(subcategories.map((x) => x.toJson())),
  };
}

class Subcategory {
  Subcategory({
    this.subcategoryId,
    this.subcategoryName,
    this.subcategoryStatus,
  });

  String subcategoryId;
  String subcategoryName;
  String subcategoryStatus;

  factory Subcategory.fromJson(Map<String, dynamic> json) => Subcategory(
    subcategoryId: json["subcategoryId"],
    subcategoryName: json["subcategoryName"],
    subcategoryStatus: json["subcategoryStatus"],
  );

  Map<String, dynamic> toJson() => {
    "subcategoryId": subcategoryId,
    "subcategoryName": subcategoryName,
    "subcategoryStatus": subcategoryStatus,
  };
}
