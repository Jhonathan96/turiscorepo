// To parse this JSON data, do
//
//     final turistasResponse = turistasResponseFromJson(jsonString);

import 'dart:convert';

TuristasResponse turistasResponseFromJson(String str) => TuristasResponse.fromJson(json.decode(str));

String turistasResponseToJson(TuristasResponse data) => json.encode(data.toJson());

class TuristasResponse {
  TuristasResponse({
    this.code,
    this.data,
    this.message,
  });

  int code;
  Data data;
  String message;

  factory TuristasResponse.fromJson(Map<String, dynamic> json) => TuristasResponse(
    code: json["code"],
    data: Data.fromJson(json["data"]),
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "code": code,
    "data": data.toJson(),
    "message": message,
  };
}

class Data {
  Data({
    this.users,
    this.total,
  });

  List<User> users;
  int total;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    users: List<User>.from(json["users"].map((x) => User.fromJson(x))),
    total: json["total"],
  );

  Map<String, dynamic> toJson() => {
    "users": List<dynamic>.from(users.map((x) => x.toJson())),
    "total": total,
  };
}

class User {
  User({
    this.uid,
    this.fullName,
    this.email,
    this.description,
    this.image,
    this.userStatus,
    this.userCreatedAt,
    this.friend,
    this.sentFriendRequest,
    this.receivedFriendRequest,
    this.iBlockedHim,
    this.blockedMe,
    this.countAllFriends,
    this.countVisitedSites,
    this.countFavoritesSites,
  });

  String uid;
  String fullName;
  String email;
  String description;
  String image;
  String userStatus;
  DateTime userCreatedAt;
  bool friend;
  bool sentFriendRequest;
  bool receivedFriendRequest;
  bool iBlockedHim;
  bool blockedMe;
  int countAllFriends;
  int countVisitedSites;
  int countFavoritesSites;

  factory User.fromJson(Map<String, dynamic> json) => User(
    uid: json["uid"],
    fullName: json["fullName"],
    email: json["email"],
    description: json["description"],
    image: json["image"],
    userStatus: json["userStatus"],
    userCreatedAt: DateTime.parse(json["userCreatedAt"]),
    friend: json["friend"],
    sentFriendRequest: json["sentFriendRequest"],
    receivedFriendRequest: json["receivedFriendRequest"],
    iBlockedHim: json["iBlockedHim"],
    blockedMe: json["blockedMe"],
    countAllFriends: json["countAllFriends"],
    countVisitedSites: json["countVisitedSites"],
    countFavoritesSites: json["countFavoritesSites"],
  );

  Map<String, dynamic> toJson() => {
    "uid": uid,
    "fullName": fullName,
    "email": email,
    "description": description,
    "image": image,
    "userStatus": userStatus,
    "userCreatedAt": userCreatedAt.toIso8601String(),
    "friend": friend,
    "sentFriendRequest": sentFriendRequest,
    "receivedFriendRequest": receivedFriendRequest,
    "iBlockedHim": iBlockedHim,
    "blockedMe": blockedMe,
    "countAllFriends": countAllFriends,
    "countVisitedSites": countVisitedSites,
    "countFavoritesSites": countFavoritesSites,
  };
}
