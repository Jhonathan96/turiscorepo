import 'dart:convert';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:turisco/src/Models/InterestResponseModel.dart';
import 'package:turisco/src/Models/MySubCatgoriesModel.dart' as mySubCat;
import 'package:turisco/src/Models/saveSubCats.dart';
import 'package:turisco/src/Services/Onboarding.dart';
import 'package:turisco/src/Services/ProfileService.dart';
import 'package:turisco/src/UI/Menu/MenuPage.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';


class MyInterestProvider with ChangeNotifier{

  List<mySubCat.Category> _categories = [];
  List<mySubCat.Subcategory> _subCatsSelected = [];
  SharePreference _prefs = SharePreference();


  List<mySubCat.Subcategory> get subCatsSelected => _subCatsSelected;

  set subCatsSelected(List<mySubCat.Subcategory> value) {
    _subCatsSelected = value;
    notifyListeners();
  }

  List<mySubCat.Category> get categories => _categories;

  set categories(List<mySubCat.Category> value) {
    _categories = value;
    notifyListeners();
  }

  serviceGetMySaveSubCategories(BuildContext context) async {
    categories = [];
    subCatsSelected = [];
    utils.checkInternet().then((value) async {
      if (value) {
        utils.startProgress(context);
        Future callUser = ProfileService.instance.getMyInterest(context,_prefs.uid);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          mySubCat.GetSaveSubCategories data = mySubCat.GetSaveSubCategories.fromJson(decodeJSON);

          if(data.code.toString() == "100") {
            categories = data.data.categories;
            for(var cat in data.data.categories){
              for(var subCat in cat.subcategories){
                if(subCat.status == 'ACTIVE') {
                  subCatsSelected.add(subCat);
                }
                }
              }

            Navigator.pop(context);


          }else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Lo sentimos vuelve a intentarlo", "Ha ocurrido un error");

          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }

  serviceSaveSubCategories(BuildContext context) async {
    String subCatsID = "";
    if(subCatsSelected.isEmpty){
      utils.showSnackBarError(context, "Debes seleccionar almenos una subcategoria", "Ups!");
      return;
    }
    for(var subCat in subCatsSelected){
      if(subCatsID.characters.length == 0){
        subCatsID = subCat.subcategoryId;
      }else{
        subCatsID = subCatsID+","+subCat.subcategoryId;
      }

    }

    print('cadena de subcats ${subCatsID}');


    utils.checkInternet().then((value) async {
      if (value) {

        utils.startProgress(context);
        Future callUser = OnboardingService.instance.saveInterest(context, subCatsID);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          SaveSubCategoriesResponse data = SaveSubCategoriesResponse.fromJson(decodeJSON);

          if(data.code.toString() == "100") {

            Navigator.pop(context);
            Navigator.pop(context);
          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Lo sentimos vuelve a intentarlo", "Ha ocurrido un error");
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }
}