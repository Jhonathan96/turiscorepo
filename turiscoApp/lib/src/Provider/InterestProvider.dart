import 'dart:convert';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:turisco/src/Models/InterestResponseModel.dart';
import 'package:turisco/src/Models/saveSubCats.dart';
import 'package:turisco/src/Services/Onboarding.dart';
import 'package:turisco/src/UI/Menu/MenuPage.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';


class InterestProvider with ChangeNotifier{

  List<Category> _categories = [];
  List<Subcategory> _subcategoriesSelected = [];


  List<Subcategory> get subcategoriesSelected => _subcategoriesSelected;

  List<Category> get categories => _categories;
  SharePreference prefs = SharePreference();

  set categories(List<Category> value) {
    _categories = value;
     notifyListeners();
  }
  set subcategoriesSelected(List<Subcategory> value) {
    _subcategoriesSelected = value;
    notifyListeners();
  }


  serviceGetCategories(BuildContext context) async {

    utils.checkInternet().then((value) async {
      if (value) {

        utils.startProgress(context);
        Future callUser = OnboardingService.instance.getInterest(context);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          InterestResponse data = InterestResponse.fromJson(decodeJSON);

          if(data.code.toString() == "100") {
            categories = data.data.categories;
            Navigator.pop(context);
           // print('UID USER ${data.data.user.uid}');
            //prefs.authToken = data.authToken;
            //Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:InterestPage(), duration: Duration(milliseconds: 500)));

            // Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child: HomePage(), duration: Duration(milliseconds: 700)));
            //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => BaseNavigationPage()), (Route<dynamic> route) => false);
          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "No hemos encontrado el usuario", "Ha ocurrido un error");
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }

  serviceSaveSubCategories(BuildContext context) async {
    String subCatsID = "";
    if(subcategoriesSelected.isEmpty){
      utils.showSnackBarError(context, "Debes seleccionar almenos una subcategoria", "Ups!");
      return;
    }
    for(var subCat in subcategoriesSelected){
      if(subCatsID.characters.length == 0){
        subCatsID = subCat.subcategoryId;
      }else{
        subCatsID = subCatsID+","+subCat.subcategoryId;
      }

    }

    print('cadena de subcats ${subCatsID}');


    utils.checkInternet().then((value) async {
      if (value) {

        utils.startProgress(context);
        Future callUser = OnboardingService.instance.saveInterest(context, subCatsID);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          SaveSubCategoriesResponse data = SaveSubCategoriesResponse.fromJson(decodeJSON);

          if(data.code.toString() == "100") {
            var subCats = jsonEncode(data.data);
            prefs.subCatsSave = subCats;

            Navigator.pop(context);
            Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
               MenuPage()), (Route<dynamic> route) => false);
          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Lo sentimos vuelve a intentarlo", "Ha ocurrido un error");
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }
}