import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Services/Onboarding.dart';
import 'package:turisco/src/UI/Menu/MenuPage.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';


class LoginProvider with ChangeNotifier{
  //"code": 140,
  bool _showPassword = true;
  bool  get showPassword => this._showPassword;
  SharePreference prefs = SharePreference();

  set showPassword(bool val){
    this._showPassword = val;
    notifyListeners();
  }

  handleShowPassword(){
    if(showPassword){
      showPassword = false;
    }else{
      showPassword = true;
    }
  }

  serviceLogin(BuildContext context,String email,String password) async {
    print("SHA1 ${utils.convertSha1(password)}");
    utils.checkInternet().then((value) async {
      if (value) {
        utils.startProgress(context);
        Future callUser = OnboardingService.instance.loginUser(context, email, password);
        await callUser.then((responseToken) {

          var decodeJSON = jsonDecode(responseToken);

          SignUpResponse data = SignUpResponse.fromJsonMap(decodeJSON);

          if(data.code.toString() == "100") {
             Navigator.pop(context);
            print('Auth Token ${data.data.authToken}');
            prefs.authToken = data.data.authToken;
            prefs.uid = data.data.user.uid;
            var dataUser = jsonEncode(data.data.user);
            prefs.dataUser = dataUser;
             Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                 MenuPage()), (Route<dynamic> route) => false);


          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Usuario o contraseña incorrectos", "No hemos encontrado el usuario");
          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Lo sentimos ha ocurrido un error", "Upps!!");
        });
      } else {
        print("you has not internet");
        utils.showSnackBarError(context, "Revisa tu conexión a internet", "Error de conexión");
      }
    });
  }

}