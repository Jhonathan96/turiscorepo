import 'dart:convert';
import 'package:flutter/cupertino.dart';


class GeneralProvider with ChangeNotifier{

  var _currentIndex = 2;

  get currentIndex => _currentIndex;

  set currentIndex(value) {
    _currentIndex = value;
    notifyListeners();
  }

}