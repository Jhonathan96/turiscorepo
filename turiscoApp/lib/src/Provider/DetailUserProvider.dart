
import 'dart:convert';

import 'package:turisco/src/Models/MySubCatgoriesModel.dart' as mySubCat;
import 'package:flutter/cupertino.dart';
import 'package:turisco/src/Services/ProfileService.dart';
import 'package:turisco/src/Utilies/utils.dart';

class DetailUserProvider with ChangeNotifier{

  List<mySubCat.Category> _categories = [];
  List<mySubCat.Subcategory> _subCatsSelected = [];


  List<mySubCat.Subcategory> get subCatsSelected => _subCatsSelected;

  set subCatsSelected(List<mySubCat.Subcategory> value) {
    _subCatsSelected = value;
    notifyListeners();
  }

  List<mySubCat.Category> get categories => _categories;

  set categories(List<mySubCat.Category> value) {
    _categories = value;
    notifyListeners();
  }

  serviceGetCategoriesUser(BuildContext context,String uid) async {
    categories = [];

    utils.checkInternet().then((value) async {
      if (value) {
        utils.startProgress(context);
        Future callUser = ProfileService.instance.getMyInterest(context,uid);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          mySubCat.GetSaveSubCategories data = mySubCat.GetSaveSubCategories.fromJson(decodeJSON);

          if(data.code.toString() == "100") {
            categories = data.data.categories;
            for(var cat in data.data.categories){
              for(var subCat in cat.subcategories){
                if(subCat.status == 'ACTIVE') {
                  subCatsSelected.add(subCat);
                }
              }
            }

            Navigator.pop(context);


          }else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Lo sentimos vuelve a intentarlo", "Ha ocurrido un error");

          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }

}