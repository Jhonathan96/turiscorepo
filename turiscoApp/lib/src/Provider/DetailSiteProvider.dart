

import 'package:flutter/cupertino.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailSiteProvider with ChangeNotifier{


  void launchWaze(double lat, double lng) async {
    print("Lat ${lat} Long ${lng}");
    var url = 'waze://?ll=${lat.toString()},${lng.toString()}';
    var fallbackUrl =
        'https://waze.com/ul?ll=${lat.toString()},${lng.toString()}&navigate=yes';
    try {
      bool launched =
      await launch(url, forceSafariVC: false, forceWebView: false);
      if (!launched) {
        await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
      }
    } catch (e) {
      await launch(fallbackUrl, forceSafariVC: false, forceWebView: false);
    }
  }

  openWaze(BuildContext buildContext,double lat, double lng,String title,String desc) async {
    final availableMaps = await MapLauncher.installedMaps;
    print(availableMaps); // [AvailableMap { mapName: Google Maps, mapType: google }, ...]

//final OrderModel order = ModalRoute.of(context).settings.arguments;

    if (await MapLauncher.isMapAvailable(MapType.waze)) {
      print("Latitude _______${lat}");
      await MapLauncher.showMarker(mapType:  MapType.waze, coords: Coords(lat,lng), title: title);


    }
  }

  openGoogleMaps(double lat, double lng,String title) async {

    final availableMaps = await MapLauncher.installedMaps;
    print(availableMaps); // [AvailableMap { mapName: Google Maps, mapType: google }, ...]

    if (await MapLauncher.isMapAvailable(MapType.google)) {
      await MapLauncher.showMarker(
        mapType: MapType.google,
        coords: Coords(lat, lng),
        title:  title,
      );
    }
  }


  launchURLGoogleMaps(double lat, double lng) async {
    var lati = lat ?? 0.0;
    var long = lng ?? 0.0;

    var url = "https://maps.google.com/?q=${lati},${long}&dir_action=navigate&travelmodel=driving";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }



}