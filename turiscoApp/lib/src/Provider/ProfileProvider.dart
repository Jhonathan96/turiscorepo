import 'package:flutter/material.dart';
import 'package:turisco/src/UI/Onboarding/LoginPage.dart';
import 'package:turisco/src/Utilies/share_preference.dart';

class ProfileProvider with ChangeNotifier{
  SharePreference prefs = SharePreference();

  closeSession(BuildContext context){
    prefs.dataUser = "0";
    prefs.uid = "0";
    prefs.authToken = "0";
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
        LoginPage()), (Route<dynamic> route) => false);


  }
}