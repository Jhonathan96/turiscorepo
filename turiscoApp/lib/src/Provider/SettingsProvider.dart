import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Services/ProfileService.dart';
import 'package:turisco/src/UI/Onboarding/LoginPage.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';

class SettingsProvider with ChangeNotifier{
  SharePreference prefs = SharePreference();
  bool _obscureTextPassword = true;
  String _cadSubCats = "";
  UserSigInUpResponse user;


  String get cadSubCats => _cadSubCats;

  set cadSubCats(String value) {
    _cadSubCats = value;
    notifyListeners();
  }


  bool get obscureTextPassword => _obscureTextPassword;

  set obscureTextPassword(bool value) {
    _obscureTextPassword = value;
    notifyListeners();
  }

  switchObscureTextPassword(){
    if(obscureTextPassword){
      obscureTextPassword = false;
    }else{
      obscureTextPassword = true;
    }
  }

  getSubCatsCad(List<String> subcategories){
    for(var subCat in subcategories){
      if(cadSubCats.characters.length == 0){
        cadSubCats = subCat.toString();
      }else{
        cadSubCats = cadSubCats+","+subCat.toString();
      }
    }
  }

  serviceUpdateUser(BuildContext context,String name,String description,String oldPassword,String newPassword) async {

    var dataUser = jsonDecode(prefs.dataUser);
    user = UserSigInUpResponse.fromJsonMap(dataUser);

    if(name == ''){
      name = user.fullName;
    }
    if(description == ''){
      description = user.description;
    }

    utils.checkInternet().then((value) async {
      if (value) {

        utils.startProgress(context);
        Future callUser = ProfileService.instance.updateProfile(context, name, oldPassword, newPassword,description);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          UpdateProfileResponse data = UpdateProfileResponse.fromJsonMap(decodeJSON);

          if(data.code.toString() == "100") {
            Navigator.pop(context);
            var dataUser = jsonEncode(data.user);
            prefs.dataUser = dataUser;
            utils.showSnackBarGood(context, 'Datos Actualizados');

          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, data.message, "Ha ocurrido un error");
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }




}