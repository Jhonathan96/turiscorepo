import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Services/Onboarding.dart';
import 'package:turisco/src/UI/Onboarding/InterestPage.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';


class RegisterProvider with ChangeNotifier{

  bool _showPassword = true;
  bool _checkTerms = false;
  bool  get showPassword => this._showPassword;
  bool  get checkTerms => this._checkTerms;
  SharePreference prefs = SharePreference();

  set showPassword(bool val){
    this._showPassword = val;
    notifyListeners();
  }

  set checkTerms(bool val){
    this._checkTerms = val;
    notifyListeners();
  }

  handleShowPassword(){
    if(showPassword){
      showPassword = false;
    }else{
      showPassword = true;
    }
  }

  handleCheckTerms(){
    if(checkTerms){
      checkTerms = false;
    }else{
      checkTerms = true;
    }
  }

  serviceRegister(BuildContext context,String email,String password,String name) async {

    if(!checkTerms){
      utils.showSnackBarError(context,'Debes aceptar los terminos y condiciones', 'Upps!!');
      return;
    }


    utils.checkInternet().then((value) async {
      if (value) {

        utils.startProgress(context);
        Future callUser = OnboardingService.instance.signUp(context,name, email, password);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          SignUpResponse data = SignUpResponse.fromJsonMap(decodeJSON);

          if(data.code.toString() == "100") {
            Navigator.pop(context);
            print('UID USER ${data.data.user.uid}');
            prefs.authToken = data.data.authToken;
            prefs.uid = data.data.user.uid;
            var dataUser = jsonEncode(data.data.user);
            prefs.dataUser = dataUser;
            Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:InterestPage(), duration: Duration(milliseconds: 500)));

            // Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child: HomePage(), duration: Duration(milliseconds: 700)));
            //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => BaseNavigationPage()), (Route<dynamic> route) => false);
          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "No hemos encontrado el usuario", "Ha ocurrido un error");
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }





}