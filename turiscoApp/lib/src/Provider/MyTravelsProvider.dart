

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:turisco/src/Models/MyTravelsModel.dart';
import 'package:turisco/src/Services/ProfileService.dart';
import 'package:turisco/src/Utilies/utils.dart';

class MyTravelsProvider with ChangeNotifier{

  List<Site> _sites = [];


  List<Site> get sites => _sites;

  set sites(List<Site> value) {
    _sites = value;
    notifyListeners();
  }

  serviceGetMyTravels(BuildContext context) async {
   sites = [];
    utils.checkInternet().then((value) async {
      if (value) {
        utils.startProgress(context);
        Future callUser = ProfileService.instance.getMyTravels(context);
        await callUser.then((user) {

          var decodeJSON = jsonDecode(user);

          MyTravelsResponse data = MyTravelsResponse.fromJson(decodeJSON);

          if(data.code.toString() == "100") {
            sites = data.data.sites;
            Navigator.pop(context);


          }else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Lo sentimos vuelve a intentarlo", "Ha ocurrido un error");

          }
        }, onError: (error) {
          Navigator.pop(context);
          utils.showSnackBarError(context, "Vuelve a intentarlo", "Ha ocurrido un error");


          // Navigator.pop(context);

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion de internet", "Error de conexion");


      }
    });
  }


}