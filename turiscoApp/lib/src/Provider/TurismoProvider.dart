import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_page_transition/page_transition_type.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Models/SitesModel.dart';
import 'package:turisco/src/Services/HomeService.dart';
import 'package:turisco/src/Services/Onboarding.dart';
import 'package:turisco/src/UI/Menu/MenuPage.dart';
import 'package:turisco/src/UI/Onboarding/LoginPage.dart';
import 'package:turisco/src/Utilies/Constans.dart';
import 'package:turisco/src/Utilies/GlobalVariables.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';


class TurismoProvider with ChangeNotifier{
  SharePreference prefs = SharePreference();

  List<Site> _sites = [];



  List<Site> get sites => _sites;

  set sites(List<Site> value) {
    _sites = value;
    notifyListeners();
  }

  serviceGetSites(BuildContext context,double lat,double lng) async {
    sites = [];
    utils.checkInternet().then((value) async {
      if (value) {
        utils.startProgress(context);
        Future callUser = HomeService.instance.getSites(context,lat,lng);
        await callUser.then((responseToken) {

          var decodeJSON = jsonDecode(responseToken);

          SitesResponse data = SitesResponse.fromJson(decodeJSON);

          if(data.code.toString() == "100") {
            sites = data.data.sites;
            Navigator.pop(context);
          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Ha ocurrido un error", 'Ups!!');
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          print('Error ${error}');
          Navigator.pop(context);
          utils.showSnackBarError(context, "Ha ocurrido un error", 'Ups!!');

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion ainternet", 'Error de conexion');


      }
    });
  }


}