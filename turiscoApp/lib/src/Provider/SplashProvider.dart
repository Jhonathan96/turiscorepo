import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_page_transition/page_transition_type.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Services/Onboarding.dart';
import 'package:turisco/src/UI/Menu/MenuPage.dart';
import 'package:turisco/src/UI/Onboarding/LoginPage.dart';
import 'package:turisco/src/Utilies/Constans.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';


class SplashProvider with ChangeNotifier{
  SharePreference prefs = SharePreference();


  serviceAccesToken(BuildContext context) async {

    utils.checkInternet().then((value) async {
      if (value) {
        //utils.startProgress(context);
        Future callUser = OnboardingService.instance.accessToken(context);
        await callUser.then((responseToken) {

          var decodeJSON = jsonDecode(responseToken);

          ResponseAccessToken data = ResponseAccessToken.fromJsonMap(decodeJSON);

          if(data.code.toString() == "100") {
            // Navigator.pop(context);

            prefs.accessToken = data.data.accessToken;


            print('AccessToken ${ prefs.accessToken }');

            // Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child: HomePage(), duration: Duration(milliseconds: 700)));
            //Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => BaseNavigationPage()), (Route<dynamic> route) => false);
          } else {
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          // Navigator.pop(context);

        });
      } else {
        print("you has not internet");

      }
    });
  }

  void openApp(BuildContext context) async {

    if (prefs.uid == "0") {

      Navigator.of(context).pushReplacement(PageTransition(type: PageTransitionType.slideInLeft, child: LoginPage(), duration: Duration(milliseconds: 700)));
    } else {
      Navigator.of(context).pushReplacement(PageTransition(type: PageTransitionType.slideInLeft, curve: Curves.decelerate, duration: Duration(milliseconds: 500), child: MenuPage()));

    }
  }


}