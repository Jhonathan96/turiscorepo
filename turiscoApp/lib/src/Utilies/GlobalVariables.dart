
import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GlobalVariables {

  static final GlobalVariables _instance = GlobalVariables._internal();
  factory GlobalVariables() => _instance;

  GlobalVariables._internal()  {

  }


  double latitude = 0.0;
  double longitude = 0.0;

}