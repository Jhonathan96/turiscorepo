
import 'dart:convert';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:crypto/crypto.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:turisco/src/Models/SitesModel.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/DialogLoading.dart';
import 'package:turisco/src/Utilies/DialogRatingApp.dart';
import 'package:turisco/src/Utilies/DialogRatingSite.dart';
import 'package:turisco/src/Utilies/Strings.dart';

class _Utils {

  Future<bool> checkInternet() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  String getPlatform(){
    if (Platform.isAndroid) {
      return "a";
    } else if (Platform.isIOS) {
      return "i";
    }
    return "a";
  }

  startProgress(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => DialogLoadingAnimated()
    );
  }

  startProgressDialogRating(BuildContext context,Site site) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => DialogRatingSite(site: site,)
    );
  }


  startProgressDialogRatingApp(BuildContext context,Site site) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => DialogRatingApp(site: site,)
    );
  }

  String convertSha1(String password){
    var bytes = utf8.encode(password); // data being hashed

    var digest = sha1.convert(bytes);
    print('Sha1 pass ${digest}');
    return digest.toString().trim();
  }


  Widget showSnackBarGood(BuildContext context,String message){
    return Flushbar(
      animationDuration: Duration(milliseconds: 500),
      margin: EdgeInsets.only(left: 0,right: 0,bottom: 0),
      borderRadius: 30,
      backgroundColor:CustomColors.blueLetter,
      /* icon: Padding(
        padding: const EdgeInsets.only(left: 15),
        child: Image(
          width: 40,
          height: 40,
          image: AssetImage("Assets/images/ic_error.png"),
        ),
      ),*/
      message:  message,
      duration:  Duration(seconds: 3),
    )..show(context);
  }


  Widget showSnackBarError(BuildContext context,String message,String title){
    return IgnorePointer(
      child: Flushbar(
        isDismissible: true,
        titleText: Center(
            child: Padding(
              padding: const EdgeInsets.only(right: 30),
              child: Text(title,style: TextStyle(fontSize: 16,fontFamily: Strings.fontlato,color: CustomColors.white),),
            )
        ),

        padding: EdgeInsets.only(top: 10,bottom: 10,right: 40),
        title:title,


        animationDuration: Duration(milliseconds: 500),

        borderRadius: 30,

        backgroundColor: CustomColors.red,
        icon:  Image(
          width: 20,
          height: 20,
          fit: BoxFit.contain,
          color: Colors.white,
          image: AssetImage("assets/images/close.png"),

        ),
        message: message,
        duration:  Duration(seconds: 3),
      )..show(context),

    );
  }

}
final utils = _Utils();
