import 'package:bottom_bar/bottom_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:spring_button/spring_button.dart';
import 'package:turisco/src/Models/InterestResponseModel.dart';
import 'package:turisco/src/Models/MySubCatgoriesModel.dart' as mySubCat;
import 'package:turisco/src/Models/MyTravelsModel.dart' as travel;
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Models/SitesModel.dart';
import 'package:turisco/src/Models/TuristasModel.dart';
import 'package:turisco/src/Provider/GeneralProvider.dart';
import 'package:turisco/src/Provider/InterestProvider.dart';
import 'package:turisco/src/Provider/LoginProvider.dart';
import 'package:turisco/src/Provider/MyInterestProvider.dart';
import 'package:turisco/src/Provider/RegisterProvider.dart';
import 'package:turisco/src/Provider/SettingsProvider.dart';

import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';


Widget txtEmailLogin(BuildContext context,TextEditingController controller){
  return Container(

    width: double.infinity,
    height: 40,
    child: Center(
      child: TextField(
        controller: controller,

        style: TextStyle(
          fontSize: 15,
          fontFamily: Strings.fontlato,
          color: CustomColors.grayTurisco
        ),
        cursorColor: CustomColors.grayTurisco,
        keyboardType: TextInputType.emailAddress,

        decoration: InputDecoration(
          isDense: true,
          border: InputBorder.none,
          hintText: Strings.email,
          hintStyle:TextStyle(
              fontSize: 15,
              fontFamily: Strings.fontlato,
              color: CustomColors.grayTurisco.withOpacity(.5)
          )
        ),
      ),
    ),
  );
}

Widget txtPasswordLogin(BuildContext context,TextEditingController controller,LoginProvider loginProvider){
  return Container(
    padding: EdgeInsets.only(right: 10),
    child: Row(
      children: [
        Expanded(
          child: Container(

            width: double.infinity,
            height: 40,
            child: Center(
              child: TextField(
                controller: controller,
                obscureText: loginProvider.showPassword,
                style: TextStyle(
                    fontSize: 15,
                    fontFamily: Strings.fontlato,
                    color: CustomColors.grayTurisco
                ),
                cursorColor: CustomColors.grayTurisco,
                keyboardType: TextInputType.emailAddress,

                decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    hintText: Strings.password,
                    hintStyle:TextStyle(
                        fontSize: 15,
                        fontFamily: Strings.fontlato,
                        color: CustomColors.grayTurisco.withOpacity(.5)
                    )
                ),
              ),
            ),
          ),
        ),
        IconButton(icon: Icon(loginProvider.showPassword ? Icons.admin_panel_settings_sharp : Icons.remove_red_eye,color: CustomColors.gray2Turisco),onPressed: (){
          loginProvider.handleShowPassword();
        },)
      ],
    ),
  );
}


Widget btnCustom(Color color, String name, Function action) {
  return SpringButton(
    SpringButtonType.OnlyScale,
    Container(
      height: 55,
      //margin: EdgeInsets.only(right: 27),
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color:  color,
      ),
      child:Center(
        child:
        Text(
          name,
          style: TextStyle(
            fontFamily: Strings.fontlatoBold,
            fontSize: 16,
            color: CustomColors.white
          ),
        ),
      ),

    ),
    onTapDown: (_) {
      action();
    },

  );




}

Widget btnWitImage(Color color, String name,String image, Function action) {
  return SpringButton(
    SpringButtonType.OnlyScale,
    Container(
      height: 55,
      padding: EdgeInsets.all(10),
      //margin: EdgeInsets.only(right: 27),
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color:  color,
      ),
      child:Center(
        child:
        Row(
          children: [
            Image(
              width: 30,
              height: 30,
              fit: BoxFit.contain,
              image: AssetImage(image),
            ),
            SizedBox(width: 12),
            Expanded(
              child: Text(
                name,
                style: TextStyle(
                    fontFamily: Strings.fontlatoBold,
                    fontSize: 16,
                    color: CustomColors.white
                ),
              ),
            ),
          ],
        ),
      ),

    ),
    onTapDown: (_) {
      action();
    },

  );




}

Widget txtBasic(BuildContext context,TextEditingController controller,String hint){
  return Container(

    width: double.infinity,
    height: 40,
    child: Center(
      child: TextField(
        controller: controller,

        style: TextStyle(
            fontSize: 15,
            fontFamily: Strings.fontlato,
            color: CustomColors.grayTurisco
        ),
        cursorColor: CustomColors.grayTurisco,
        keyboardType: TextInputType.emailAddress,

        decoration: InputDecoration(
            isDense: true,
            border: InputBorder.none,
            hintText: hint,
            hintStyle:TextStyle(
                fontSize: 15,
                fontFamily: Strings.fontlato,
                color: CustomColors.grayTurisco.withOpacity(.5)
            )
        ),
      ),
    ),
  );
}

Widget txtEmailRegister(BuildContext context,TextEditingController controller){
  return Container(

    width: double.infinity,
    height: 40,
    child: Center(
      child: TextField(
        controller: controller,

        style: TextStyle(
            fontSize: 15,
            fontFamily: Strings.fontlato,
            color: CustomColors.grayTurisco
        ),
        cursorColor: CustomColors.grayTurisco,
        keyboardType: TextInputType.emailAddress,

        decoration: InputDecoration(
            isDense: true,
            border: InputBorder.none,
            hintText: Strings.email,
            hintStyle:TextStyle(
                fontSize: 15,
                fontFamily: Strings.fontlato,
                color: CustomColors.grayTurisco.withOpacity(.5)
            )
        ),
      ),
    ),
  );
}


Widget txtPasswordRegister(BuildContext context,TextEditingController controller,RegisterProvider loginProvider){
  return Container(
    padding: EdgeInsets.only(right: 10),
    child: Row(
      children: [
        Expanded(
          child: Container(

            width: double.infinity,
            height: 40,
            child: Center(
              child: TextField(
                controller: controller,
                obscureText: loginProvider.showPassword,
                style: TextStyle(
                    fontSize: 15,
                    fontFamily: Strings.fontlato,
                    color: CustomColors.grayTurisco
                ),
                cursorColor: CustomColors.grayTurisco,
                keyboardType: TextInputType.emailAddress,

                decoration: InputDecoration(
                    isDense: true,
                    border: InputBorder.none,
                    hintText: Strings.password,
                    hintStyle:TextStyle(
                        fontSize: 15,
                        fontFamily: Strings.fontlato,
                        color: CustomColors.grayTurisco.withOpacity(.5)
                    )
                ),
              ),
            ),
          ),
        ),
        IconButton(icon: Icon(loginProvider.showPassword ? Icons.admin_panel_settings_sharp : Icons.remove_red_eye,color: CustomColors.gray2Turisco),onPressed: (){
          loginProvider.handleShowPassword();
        },)
      ],
    ),
  );
}

Widget headerBack(BuildContext context, bool hideArrow,String title){
  return  Stack(
    children: [

      Container(

        margin: EdgeInsets.only(top: 30,bottom: 15),
        width: double.infinity,
        alignment: Alignment.center,
        child: Text(
          title,
          style: TextStyle(
              fontSize: 28,
              fontFamily: Strings.fontGrindel,
              color: CustomColors.blueLetter
          ),
        ),
      ),
     hideArrow ? Container() : Positioned(
        left: 25,
        top: 40,
        child: Container(
          height: 30,
          child: GestureDetector(
            child: Image(
              width: 30,
              height: 25,
              image: AssetImage('assets/images/back.png'),
              color: CustomColors.redTour,

            ),
            onTap: (){
              Navigator.pop(context);
            },
          ),
        ),
      ),

    ],
  );
}

Widget itemInterest(InterestProvider interestProvider,Subcategory subcategory,Function actionRefresh,Color colorCat){
  return GestureDetector(
    child: Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: interestProvider.subcategoriesSelected.contains(subcategory) ? colorCat : colorCat.withOpacity(.5),
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Center(
        child: Row(
          children: [
            Image(
              width: 40,
              height: 40,
              image: AssetImage('assets/images/mapa-de-viaje.png')
            ),
            Expanded(
              child: Text(
                subcategory.subcategoryName,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  fontFamily: Strings.fontlatoBold,
                  color: CustomColors.white
                ),
              ),
            ),
          ],
        ),
      ),
    ),
    onTap: (){

      if(interestProvider.subcategoriesSelected.contains(subcategory)){
        interestProvider.subcategoriesSelected.remove(subcategory);
      }else{
        interestProvider.subcategoriesSelected.add(subcategory);
      }

      actionRefresh();
    },
  );
}

Widget myItemInterest(MyInterestProvider myInterestProvider,mySubCat.Subcategory subcategory,Function actionRefresh,Color colorCat){
  return GestureDetector(
    child: Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          color:myInterestProvider.subCatsSelected.contains(subcategory) ? colorCat : colorCat.withOpacity(.5),
          borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Center(
        child: Row(
          children: [
            Image(
                width: 40,
                height: 40,
                image: AssetImage('assets/images/mapa-de-viaje.png')
            ),
            Expanded(
              child: Text(
                subcategory.subcategoryName,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: Strings.fontlatoBold,
                    color: CustomColors.white
                ),
              ),
            ),
          ],
        ),
      ),
    ),
    onTap: (){

      if(myInterestProvider.subCatsSelected.contains(subcategory)){
        myInterestProvider.subCatsSelected.remove(subcategory);
      }else{
        myInterestProvider.subCatsSelected.add(subcategory);
      }
      actionRefresh();
    },
  );
}

Widget interestUser(mySubCat.Subcategory subcategory,Color colorCat){
  return GestureDetector(
    child: Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
          color:colorCat,
          borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Center(
        child: Row(
          children: [
            Image(
                width: 40,
                height: 40,
                image: AssetImage('assets/images/mapa-de-viaje.png')
            ),
            Expanded(
              child: Text(
                subcategory.subcategoryName,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 18,
                    fontFamily: Strings.fontlatoBold,
                    color: CustomColors.white
                ),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}



Widget bottomBar(GeneralProvider generalProvider,BuildContext context,PageController pageController){
  return BottomBar(

    selectedIndex: generalProvider.currentIndex,
    onTap: (int index) {
      pageController.jumpToPage(index);
      generalProvider.currentIndex = index;

    },
    items: <BottomBarItem>[
      BottomBarItem(
        icon: Icon(Icons.home),
        title: Text('Home'),
        activeColor: Colors.blue,
      ),
     /* BottomBarItem(
        icon: Icon(Icons.favorite),
        title: Text('Favorites'),
        activeColor: Colors.red,
        darkActiveColor: Colors.red.shade400,
      ),*/
      BottomBarItem(
        icon: Icon(Icons.person),
        title: Text('Perfil'),
        activeColor: Colors.greenAccent.shade700,
        darkActiveColor: Colors.greenAccent.shade400,
      ),
     /* BottomBarItem(
        icon: Icon(Icons.settings),
        title: Text('Settings'),x
        activeColor: Colors.orange,
      ),*/
    ],
  );
}


Widget photoAndNameUser(UserSigInUpResponse user){
  return Container(
    margin: EdgeInsets.only(left: 50,right: 50),
    alignment: Alignment.center,

    width: double.infinity,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 90,
          height: 90,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            child: FadeInImage(
              fit: BoxFit.fill,
              image: AssetImage('assets/images/placeHolderProfile.png'),
              placeholder: AssetImage('assets/images/placeHolderProfile.png'),
            ),
          ),
        ),
        SizedBox(width: 10),
        Column(
          children: [
            Text(
              "¡Bienvenido!",
              style: TextStyle(
                fontSize: 14,
                fontFamily: Strings.fontlatoBold,
                color: CustomColors.red
              ),
            ),
            Text(
              user.fullName,
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: Strings.fontlatoBold,
                  color: CustomColors.blueLetter
              ),
            ),
            Text(
              user.email,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: Strings.fontlatoBold,
                  color: CustomColors.blueLetter.withOpacity(.3)
              ),
            )
          ],
        )
      ],
    ) ,
  );
}

Widget itemMenu(BuildContext context,String image,String title,String text,Function action){
  return GestureDetector(
      child: Container(
        padding: EdgeInsets.all(20),
        height: 200,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          color: CustomColors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),

              blurRadius: 7,
              offset: Offset(0, 3), // changes position of shadow
            ),
          ]
        ),
        child: Column(
          children: [
            Image(
              width: 60,
              height: 60,

              fit: BoxFit.contain,
              image: AssetImage(image),
            ),

            Text(
              title,
              style: TextStyle(
                fontFamily: Strings.fontlatoBold,
                fontSize: 16,
                color: CustomColors.red,

              ),

            ),
            SizedBox(height: 8),
            Text(
              text,

              style: TextStyle(

                fontSize: 14,
                color: CustomColors.blueLetter.withOpacity(.5)
              ),
            )
          ],
        ),
      ),
      onTap: (){
        action();
      },
  );
}


Widget header(String title,BuildContext context){
  return Container(
    width: double.infinity,
    height: 50,
    decoration: BoxDecoration(

        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15),bottomRight: Radius.circular(15))
    ),
    child: Stack(
      children: [
        Positioned(
          left: 0,
          top: 0,
          child: Container(

            child: Center(
              child: IconButton(
                icon: Icon(Icons.arrow_back_rounded,color: CustomColors.white),
                onPressed: (){
                  Navigator.pop(context);
                },

              ),
            ),
            decoration: BoxDecoration(
                color: CustomColors.red.withOpacity(.6),
                borderRadius: BorderRadius.only(topRight: Radius.circular(5),bottomRight: Radius.circular(5))
            ),
          ),
        ),
        SizedBox(width: 90),
        Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 26,
                fontFamily: Strings.fontGelar,
                fontWeight: FontWeight.bold,
                color: CustomColors.blackLetter
            ),
          ),
        ),

      ],
    ),
  );
}


Widget itemCardTurismo(Site site,Function action){
  return GestureDetector(
    child: Container(
      height: 300,
      width: 200,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              child: FadeInImage(
                fit: BoxFit.cover,
                image: NetworkImage(site.image),
                placeholder: AssetImage('assets/images/placeHolderProfile.png'),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.black.withOpacity(.4),
                borderRadius: BorderRadius.all(Radius.circular(8))
              ),
            
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    site.siteName,
                    style: TextStyle(
                      fontFamily: Strings.fontlatoBold,
                      fontSize: 14,
                      color: CustomColors.white
                    ),
                  ),
                  Text(
                    site.subcategoryName.toString(),
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: Strings.fontlatoBold,
                        fontSize: 12,
                        color: CustomColors.white
                    ),
                  ),
                  Text(
                    site.cityName,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: Strings.fontlatoBold,
                        fontSize: 12,
                        color: CustomColors.white
                    ),
                  ),
                  Text(
                    "Distancia "+site.distance.toString()+" Km",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontFamily: Strings.fontlatoBold,
                        fontSize: 12,
                        color: CustomColors.white
                    ),
                  ),
                ],
              ),
            )
          )
        ],
      ),
    ),
    onTap: (){
      action();
    },
  );
}



Widget itemCardTravel(travel.Site site,Function action){
  return GestureDetector(
    child: Container(
      height: 300,
      width: 200,
      child: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              child: FadeInImage(
                fit: BoxFit.cover,
                image: NetworkImage(site.image),
                placeholder: AssetImage('assets/images/placeHolderProfile.png'),
              ),
            ),
          ),
          Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(.4),
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),

                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      site.siteName,
                      style: TextStyle(
                          fontFamily: Strings.fontlatoBold,
                          fontSize: 14,
                          color: CustomColors.white
                      ),
                    ),
                    Text(
                      site.subcategoryName.toString(),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: Strings.fontlatoBold,
                          fontSize: 12,
                          color: CustomColors.white
                      ),
                    ),
                    Text(
                      site.cityName,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: Strings.fontlatoBold,
                          fontSize: 12,
                          color: CustomColors.white
                      ),
                    ),
                    Text(
                      "Distancia "+site.distance.toString()+" Km",
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontFamily: Strings.fontlatoBold,
                          fontSize: 12,
                          color: CustomColors.white
                      ),
                    ),
                  ],
                ),
              )
          )
        ],
      ),
    ),
    onTap: (){
      action();
    },
  );
}

Widget itemCardTurista(User user,Function action){
  return GestureDetector(
      child: Container(
        height: 300,
        width: 200,
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(12)),
                child: FadeInImage(
                  fit: BoxFit.cover,
                  image: NetworkImage(user.image),
                  placeholder: AssetImage('assets/images/placeHolderProfile.png'),
                ),
              ),
            ),
            Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(.4),
                      borderRadius: BorderRadius.all(Radius.circular(8))
                  ),

                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        user.fullName,
                        style: TextStyle(
                            fontFamily: Strings.fontlatoBold,
                            fontSize: 14,
                            color: CustomColors.white
                        ),
                      ),
                      Text(
                        user.email,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontFamily: Strings.fontlatoBold,
                            fontSize: 12,
                            color: CustomColors.white
                        ),
                      ),

                    ],
                  ),
                )
            )
          ],
        ),
      ),
    onTap: (){
        action();
    },
  );
}

Widget photoAndNameUserProfile(UserSigInUpResponse user){
  return Container(
    margin: EdgeInsets.only(left: 40,right: 40),
    alignment: Alignment.center,

    width: double.infinity,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
          child: Container(
            width: 30,
            height: 30,
            color: Colors.transparent,
            child: Image(
              image: AssetImage('assets/images/camera.JPG'),
            ),
          ),
          onTap: (){
            print('Open Camera');
          },
        ),
        Container(
          width: 90,
          height: 90,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            child: FadeInImage(
              fit: BoxFit.fill,
              image: AssetImage('assets/images/placeHolderProfile.png'),
              placeholder: AssetImage('assets/images/placeHolderProfile.png'),
            ),
          ),
        ),
        SizedBox(width: 10),
        Expanded(
          child: Column(
            children: [
              Text(
                "¡Bienvenido!",
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: Strings.fontlatoBold,
                    color: CustomColors.red
                ),
              ),
              Text(
                user.fullName,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: Strings.fontlatoBold,
                    color: CustomColors.blueLetter
                ),
              ),
              Text(
                user.email,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: Strings.fontlatoBold,
                    color: CustomColors.blueLetter.withOpacity(.3)
                ),
              )
            ],
          ),
        )
      ],
    ) ,
  );
}

Widget photoAndNameDetail(User user){
  return Container(
    margin: EdgeInsets.only(left: 40,right: 40),
    alignment: Alignment.center,

    width: double.infinity,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [

        Container(
          width: 90,
          height: 90,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(100)),
            child: FadeInImage(
              fit: BoxFit.fill,
              image: AssetImage('assets/images/placeHolderProfile.png'),
              placeholder: AssetImage('assets/images/placeHolderProfile.png'),
            ),
          ),
        ),
        SizedBox(width: 10),
        Expanded(
          child: Column(
            children: [

              Text(
                user.fullName,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: Strings.fontlatoBold,
                    color: CustomColors.blueLetter
                ),
              ),
              Text(
                user.email,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    fontSize: 14,
                    fontFamily: Strings.fontlatoBold,
                    color: CustomColors.blueLetter.withOpacity(.3)
                ),
              )
            ],
          ),
        )
      ],
    ) ,
  );
}

Widget itemProfile(BuildContext context, String title,Function action){
  return GestureDetector(
    child: Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 25,right: 25,bottom: 25),
      color: Colors.white,
      child: Column(


        children: [
          Text(
            title,
            style: TextStyle(
              fontSize: 16,
              color: CustomColors.blueLetter,
              fontFamily: Strings.fontlato
            ),
          ),
          SizedBox(height: 15),
          Container(
            width: double.infinity,
            height: 1,
            color: CustomColors.blueLetter.withOpacity(.2),
          )
        ],
      ),
    ),
    onTap: (){
      action();
    },
  );
}

Widget itemSettingsText(BuildContext context,String hint,Icon icon,TextEditingController controller,Function action){
  return GestureDetector(
    child: Container(
      margin: EdgeInsets.only(left: 30,right: 30),
      padding: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: 8),
      width: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(12)),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: CustomColors.blueLetter.withOpacity(.3),
            blurRadius: 7,
            offset: Offset(0, 3)
          )
        ]
      ),
      child: Center(
        child: Row(
          children: [
            icon,
            SizedBox(width: 10),
            Expanded(
                child: TextField(

                  controller: controller,
                  decoration: InputDecoration(

                    isDense: true,
                    border: InputBorder.none,
                    hintStyle: TextStyle(
                      height: 2.0,
                      fontFamily: Strings.fontlato,
                      color: CustomColors.blueLetter.withOpacity(.5),
                      fontSize: 14,
                    ),
                    hintText: hint
                  ),
                  cursorColor: CustomColors.blueLetter,
                  style:TextStyle(
                    fontFamily: Strings.fontlato,
                    color: CustomColors.blueLetter,
                    fontSize: 14,
                  ),
                  onEditingComplete: (){
                    action();
                  },

                )
            )

          ],
        ),
      ),
    ),
    onTap: (){

    },
  );
}


Widget emailSettings(TextEditingController controller){
  return GestureDetector(
    child: Container(
      margin: EdgeInsets.only(left: 30,right: 30),
      padding: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: 8),
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: CustomColors.blueLetter.withOpacity(.3),
                blurRadius: 7,
                offset: Offset(0, 3)
            )
          ]
      ),
      child: Center(
        child: Row(
          children: [
            Icon(Ionicons.mail_open,color: CustomColors.blueLetter.withOpacity(.5),),
            SizedBox(width: 10),
            Expanded(
                child: TextField(
                  controller: controller,
                  enabled: false,
                  decoration: InputDecoration(
                      isDense: true,
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontFamily: Strings.fontlato,
                        color: CustomColors.blueLetter.withOpacity(.5),
                        fontSize: 14,
                      ),
                      hintText: ''
                  ),
                  cursorColor: CustomColors.blueLetter,
                  style:TextStyle(
                    fontFamily: Strings.fontlato,
                    color: CustomColors.blueLetter,
                    fontSize: 14,
                  ),


                )
            )

          ],
        ),
      ),
    ),
    onTap: (){

    },
  );
}

Widget intereces(TextEditingController controller,Function action){
  return GestureDetector(
    child: Container(
      margin: EdgeInsets.only(left: 30,right: 30),
      padding: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: 8),
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: CustomColors.blueLetter.withOpacity(.3),
                blurRadius: 7,
                offset: Offset(0, 3)
            )
          ]
      ),
      child: Center(
        child: Row(
          children: [
            Icon(Ionicons.accessibility_outline,color: CustomColors.blueLetter.withOpacity(.5),),
            SizedBox(width: 10),
            Expanded(
                child: TextField(
                  controller: controller,
                  enabled: false,
                  decoration: InputDecoration(
                      isDense: true,
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontFamily: Strings.fontlato,
                        color: CustomColors.blueLetter.withOpacity(.5),
                        fontSize: 14,
                      ),
                      hintText: ''
                  ),
                  cursorColor: CustomColors.blueLetter,
                  style:TextStyle(
                    fontFamily: Strings.fontlato,
                    color: CustomColors.blueLetter,
                    fontSize: 14,
                  ),


                )
            ),
            Icon(Ionicons.chevron_forward_circle_outline,color: CustomColors.blueLetter.withOpacity(.5),),

          ],
        ),
      ),
    ),
    onTap: (){
      action();
    },
  );
}

Widget passwordSettings(TextEditingController controller,SettingsProvider settingsProvider,String hint){
  return GestureDetector(
    child: Container(
      margin: EdgeInsets.only(left: 30,right: 30),
      padding: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: 8),
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: CustomColors.blueLetter.withOpacity(.3),
                blurRadius: 7,
                offset: Offset(0, 3)
            )
          ]
      ),
      child: Center(
        child: Row(
          children: [
            Icon(Ionicons.lock_closed_outline,color: CustomColors.blueLetter.withOpacity(.5),),
            SizedBox(width: 10),
            Expanded(
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(

                      isDense: true,
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontFamily: Strings.fontlato,
                        color: CustomColors.blueLetter.withOpacity(.5),
                        fontSize: 14,
                      ),
                      hintText: hint
                  ),
                  obscureText: settingsProvider.obscureTextPassword,
                  cursorColor: CustomColors.blueLetter,
                  style:TextStyle(
                    fontFamily: Strings.fontlato,
                    color: CustomColors.blueLetter,
                    fontSize: 14,
                  ),


                )
            ),
            settingsProvider.obscureTextPassword ? IconButton(onPressed: (){
              settingsProvider.switchObscureTextPassword();
            }, icon: Icon(Ionicons.eye_off_outline,color: CustomColors.blueLetter.withOpacity(.5),)) : IconButton(onPressed: (){
              settingsProvider.switchObscureTextPassword();
            }, icon:  Icon(Ionicons.eye_outline,color: CustomColors.blueLetter.withOpacity(.5),),)


          ],
        ),
      ),
    ),
    onTap: (){

    },
  );
}


Widget descriptionSettings(TextEditingController controller,Function action){
  return GestureDetector(
    child: Container(
      margin: EdgeInsets.only(left: 30,right: 30),
      padding: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: 22),
      width: double.infinity,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: CustomColors.blueLetter.withOpacity(.3),
                blurRadius: 7,
                offset: Offset(0, 3)
            )
          ]
      ),
      child: Center(
        child: Row(
          children: [
            Icon(Ionicons.happy_outline,color: CustomColors.blueLetter.withOpacity(.5),),
            SizedBox(width: 10),
            Expanded(
                child: TextField(
                  controller: controller,

                  decoration: InputDecoration(
                      isDense: true,
                      border: InputBorder.none,
                      hintStyle: TextStyle(
                        fontFamily: Strings.fontlato,
                        color: CustomColors.blueLetter.withOpacity(.5),
                        fontSize: 14,
                      ),
                      hintText: 'Descripción'
                  ),
                  cursorColor: CustomColors.blueLetter,
                  style:TextStyle(
                    fontFamily: Strings.fontlato,
                    color: CustomColors.blueLetter,
                    fontSize: 14,
                  ),
                onEditingComplete: (){
                    action();
                },

                )
            )

          ],
        ),
      ),
    ),
    onTap: (){

    },
  );
}