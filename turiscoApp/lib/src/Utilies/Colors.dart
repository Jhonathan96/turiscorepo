import 'dart:ui';

class CustomColors {

  static final whiteBackGround = const Color(0xffF8F8F8);
  static final white = const Color(0xffFFFFFF);
  static final red= const Color(0xffF9423A);

  static final blueBackground = const Color(0xff00315F);
  static final blackLetter = const Color(0xff484848);
  static final gray = const Color(0xffD0D1D7);
  static final grayDark = const Color(0xff333333);
  static final blueLetter = const Color(0xff121D3A);
  static final blueLetter2 = const Color(0xff008BDB);
  static final redShadow = const Color(0xffEC77001D);
  static final orange= const Color(0xffEC7700);
  static final black = const Color(0xff313131);
  static final grayLetter = const Color(0xff767676);
  static final backGroundContainer = const Color(0xffF2F2F2);
  static final blue = const Color(0xff008DD8);
  static final blueTextField = const Color(0xff335A7F);
  static final blueLetter3 = const Color(0xff00315F);
  static final blackLetter2 = const Color(0xff525252);
  static final redHelpMap = const Color(0xffFFF4E8);
  static final grayBorder = const Color(0xffE6E6E6);
  static final gray3 = const Color(0xff9B9B9B);
  static final gray4 = const Color(0xffFAFAFA);
  static final blueCyan = const Color(0xffE5F6FF);
  static final grayBtn = const Color(0xffDEDEDE);
  static final grayLetterBtn = const Color(0xff636363);
  static final grayBackground2 = const Color(0xffF9F9F9);
  static final grayLetter2 = const Color(0xff9B9B9B);
  static final black2 = const Color(0xff000000);
  static final graySeparator = const Color(0xff707070);
  static final grayBorderBox = const Color(0xffC5C5C5);
  static final blackTitle = const Color(0xff2D2D2D);
  static final textColor = const Color(0xff676767);
  static final newBlue = const Color(0xff07213E);
  static final redSplash2 = const Color.fromRGBO(221, 54, 54, 1);
  static final redTour = const Color(0xffE73638);
  static final blueWaze = const Color(0xff228BD4);




  static final grayTurisco = const Color(0xff35424A);
  static final gray2Turisco = const Color(0xff989EB1);
  static final blueFace = const Color(0xff1C2DF0);









}