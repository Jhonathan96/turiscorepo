import 'package:shared_preferences/shared_preferences.dart';

class SharePreference {

  static final SharePreference _instancia = new SharePreference._internal();

  factory SharePreference() {
    return _instancia;
  }

  SharePreference._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  get tokenPush {
    return _prefs.getString('tokenPush') ?? "0";
  }

  set tokenPush(String value) {
    _prefs.setString('tokenPush', value);
  }

  get accessToken {
    return _prefs.getString('accessToken') ?? "0";
  }

  set accessToken(String value) {
    _prefs.setString('accessToken', value);
  }

  get dataUser {
    return _prefs.getString('dataUser') ?? "0";
  }

  set dataUser(String value) {
    _prefs.setString('dataUser', value);
  }


  get authToken {
    return _prefs.getString('authToken') ?? "0";
  }

  set authToken(String value) {
    _prefs.setString('authToken', value);
  }

  get uid {
    return _prefs.getString('uid') ?? "0";
  }

  set uid(String value) {
    _prefs.setString('uid', value);
  }

  get subCatsSave {
    return _prefs.getString('subCatsSave') ?? "0";
  }

  set subCatsSave(String value) {
    _prefs.setString('subCatsSave', value);
  }




}