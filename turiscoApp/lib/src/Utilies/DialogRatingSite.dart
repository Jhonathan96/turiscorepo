import 'dart:convert';

import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:turisco/src/Models/RatingModel.dart';
import 'package:turisco/src/Models/SitesModel.dart';
import 'package:turisco/src/Services/HomeService.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/utils.dart';

import 'Strings.dart';

class DialogRatingSite extends StatefulWidget {
  final Site site;
  const DialogRatingSite({Key key,this.site}) : super(key: key);

  @override
  _DialogRatingSiteState createState() => _DialogRatingSiteState();
}

class _DialogRatingSiteState extends State<DialogRatingSite> {

  double _rating = 0.0;
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black.withOpacity(.2),
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: WillPopScope(
            onWillPop: () async => false,
            child: Stack(
              children: [
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  top: 0,
                  child: GestureDetector(
                    onTap: (){
                      Navigator.pop(context);
                    },
                  ),
                ),
                Center(
                  child: BounceInUp(
                    child: Container(
                      height: 350,
                      width: double.infinity,
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          color: Colors.white
                      ),
                      margin: EdgeInsets.only(left: 20,right: 20),
                      child: Column(
                        children: [
                          Text(
                            "Califica y cuentanos tu experiencia",
                            style: TextStyle(
                                fontSize: 20,
                                color: CustomColors.blueLetter,
                                fontFamily: Strings.fontGelar
                            ),
                          ),
                          SizedBox(height: 25),
                          RatingBar(
                            onRatingChanged: (rating) => setState(() => _rating = rating),
                            filledIcon: Icons.star,

                            emptyIcon: Icons.star_border,
                            halfFilledIcon: Icons.star_half,
                            isHalfAllowed: true,
                            filledColor: Colors.amber,
                            emptyColor: Colors.amber,
                            halfFilledColor: Colors.amber,
                            size: 48,
                          ),

                          SizedBox(height: 15),
                          Container(
                            padding: EdgeInsets.all(10),
                            width: double.infinity,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(12)),
                                color: CustomColors.white,
                                border: Border.all(width: 1,color: CustomColors.grayBorderBox)
                            ),
                            child: TextField(
                              controller: controller,
                              style: TextStyle(
                                fontFamily: Strings.fontlato,
                                color: CustomColors.blueLetter,
                                fontSize: 14,

                              ),
                              maxLines: 5,
                              decoration: InputDecoration(
                                hintText: 'Agregar un comentario',
                                hintStyle: TextStyle(
                                  fontFamily: Strings.fontlato,
                                  color: CustomColors.blueLetter.withOpacity(.5),
                                  fontSize: 14,

                                ),
                                border: InputBorder.none,

                              ),
                              cursorColor: CustomColors.blueLetter,
                            ),
                          ),
                          SizedBox(height: 15),
                          Padding(padding: EdgeInsets.only(left: 20,right: 20),

                              child: btnCustom(CustomColors.blueLetter, 'Calificar',(){
                                serviceRatingSite(context);
                              })
                          )


                        ],
                      ),
                    ),
                  ),
                ),


              ],
            )
          ),
        ));
  }

  serviceRatingSite(BuildContext context) async {
    FocusScope.of(context).unfocus();
    utils.checkInternet().then((value) async {
      if (value) {
        utils.startProgress(context);
        Future callUser =  HomeService.instance.rattingSite(context, widget.site, _rating, controller.text.trim());
        await callUser.then((responseToken) {

          var decodeJSON = jsonDecode(responseToken);

          RatingSiteResponse data = RatingSiteResponse.fromJson(decodeJSON);

          if(data.code.toString() == "100") {

            Navigator.pop(context);
            Navigator.pop(context);
            utils.showSnackBarGood(context, 'Muchas Gracias, Hemos recivido tu calificación');
          } else {
            Navigator.pop(context);
            utils.showSnackBarError(context, "Ha ocurrido un error", 'Ups!!');
            //  Navigator.pop(context);
          }
        }, onError: (error) {
          print('Error ${error}');
          Navigator.pop(context);
          utils.showSnackBarError(context, "Ha ocurrido un error", 'Ups!!');

        });
      } else {
        utils.showSnackBarError(context, "Revisa tu conexion ainternet", 'Error de conexion');


      }
    });
  }



}
