class Strings {
  //Login
  static const String turisco = 'Turisco';
  static const String login = 'Iniciar Sesion';
  static const String loginHellow = '¡Hola! Es un gusto tenerte con nosotros';
  static const String email = 'Correo';
  static const String password = 'Contraseña';
  static const String continuar = 'Continuar';
  static const String register = 'Registrarse';
  static const String fotgotPass = '¿Olvidaste tu contraseña?';
  static const String alsoLogin = 'Tambien puedes iniciar session con';
  static const String facebook = 'Facebook';

  //Register
  static const String register2 = 'Registro';
  static const String registerHellow = 'Tu primer paso para iniciar un mundo de aventuras';
  static const String name = 'Nombre';
  static const String confirmPass = 'Confirmar contraseña';
  static const String termsAndConditions = 'Acepto los terminos y condiciones y las politicas de privacidad.';

  //Interest
  static const String interest = 'Intereses';
  static const String weKnow = 'Dejanos conocerte, selecciona los tipos de turismo que sean de tu interes.';


  //Fonts
  static const String fontGrindel = 'Grindel';
  static const String fontGelar= 'Gelar';
  static const String fontlato = 'Lato';
  static const String fontlatoBold = 'LatoBold';


}