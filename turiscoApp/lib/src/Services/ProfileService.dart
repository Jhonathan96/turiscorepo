import 'package:flutter/material.dart';
import 'package:turisco/src/Utilies/Constans.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:http/http.dart' as http;


class ProfileService {
  //final _prefs = SharePreference();
  static final
  instance = ProfileService._internal();


  factory ProfileService() {
    return instance;
  }


  ProfileService._internal();

  SharePreference prefs = SharePreference();

  Future<dynamic>updateProfile(BuildContext context,String name,String oldPassword,String newPassword,String description) async {



    Map<String,String> header = {
      "X-TP-Auth-Token":prefs.authToken,
    };

    Map jsonData = {
      'uid':prefs.uid,
      'fullName':name,
      'oldPassword':oldPassword,
      'newPassword':newPassword,
      'description':description
    };


    print("Parameters updateProfile ${jsonData}");
    final response = await http.post(Conts.baseUrl+"user/updateProfile",headers: header ,body: jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json updateProfile: ${response.body}");

    return response.body;
  }

  Future<dynamic>getMyInterest(BuildContext context,String uid) async {



    Map<String,String> header = {
      "X-TP-Auth-Token":prefs.authToken,
    };

    Map jsonData = {
      'uid':uid,
    };


    print("Parameters updateProfile ${jsonData}");
    final response = await http.post(Conts.baseUrl+"userSubcategories/getUserSubcategoriesByUid",headers: header ,body: jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json get MyInterest: ${response.body}");

    return response.body;
  }

  Future<dynamic>getMyTravels(BuildContext context) async {

    Map<String,String> header = {
      "X-TP-Auth-Token":prefs.authToken,
    };

    Map jsonData = {
      'uid':prefs.uid,
      'cityId':'',
      'maxKmsDistance':'',
      'usrLatitude':'',
      'usrLongitude':'',
      'limit':'20',
      'offset':'0'
    };

    final response = await http.post(Conts.baseUrl+"sites/getUserVisitedSites",headers: header ,body: jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json get MyTravels: ${response.body}");

    return response.body;
  }


}