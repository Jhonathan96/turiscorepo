import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:http/http.dart' as http;
import 'package:turisco/src/Utilies/Constans.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';


class OnboardingService {
  //final _prefs = SharePreference();
  static final
  instance = OnboardingService._internal();


  factory OnboardingService() {
    return instance;
  }

  OnboardingService._internal();

  bool conected;
  SharePreference prefs = SharePreference();

  Future<dynamic> loginUser(BuildContext context,String email,String password) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
   // var jsonIV = utils.encryptPwdIv(password);

    Map<String,String> header = {
      "X-TP-Access-Token":prefs.accessToken,
    };

    Map jsonData = {
      'email':email,
      'password':utils.convertSha1(password),
      'accountType':"TC",
    };


    print("Parameters LOGIN ${jsonData}");
    final response = await http.post(Conts.baseUrl+"auth/login",headers: header ,body: jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json Login: ${response.body}");

    return response.body;
  }

  Future<dynamic> signUp(BuildContext context,String name, String email,String password) async {

    Map<String,String> header = {
     "X-TP-Access-Token": prefs.accessToken,
    };

    Map jsonData = {
      'fullName':name,
      'email':email,
      'password':utils.convertSha1(password),
      'accountType':"TC"
    };

    var body = jsonEncode(jsonData);
    print("Parameters Registro ${jsonData}");

    final response = await http.post("https://turisco.herokuapp.com/public/v1/auth/signup",headers: header ,body:jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json SignUp: ${response.body}");

    return response.body;

  }

  Future<dynamic> accessToken(BuildContext context) async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    // var jsonIV = utils.encryptPwdIv(password);

    final header = {
      "Content-Type": "application/json",
    };

    final response = await http.get(Conts.baseUrl+"tp/generate-access-token",headers: header).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json AccessToken: ${response.body}");

    return response.body;
  }

  Future<dynamic> getInterest(BuildContext context) async {
  print('Auth Token ${prefs.authToken}');
    Map<String,String> header = {
      "X-TP-Auth-Token": prefs.authToken,
    };

    Map jsonData = {
      'categoryName':"",
    };

    var body = jsonEncode(jsonData);
    print("Parameters getCategories ${jsonData}");

    final response = await http.post(Conts.baseUrl+"subcategories/getSubcategoriesOrganizedByCategory",headers: header ,body:jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json getCategories: ${response.body}");

    return response.body;

  }

  Future<dynamic> saveInterest(BuildContext context,String subCategories) async {


    Map<String,String> header = {
      "X-TP-Auth-Token": prefs.authToken,
    };

    Map jsonData = {
      'uid':prefs.uid,
      'subcategories':subCategories
    };

    print("Parameters getCategories ${jsonData}");

    final response = await http.post(Conts.baseUrl+"userSubcategories/userSubcategoriesRegistry",headers: header ,body:jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("JsonSaveSubCategories: ${response.body}");

    return response.body;

  }


}