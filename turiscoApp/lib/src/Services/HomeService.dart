
import 'package:flutter/cupertino.dart';
import 'package:turisco/src/Models/SitesModel.dart';
import 'package:turisco/src/Utilies/Constans.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:package_info/package_info.dart';
import 'package:http/http.dart' as http;

class HomeService {

  static final
  instance = HomeService._internal();


  factory HomeService() {
    return instance;
  }

  HomeService._internal();

  SharePreference prefs = SharePreference();


  Future<dynamic>getSites(BuildContext context,double lat, double lng) async {

    Map<String,String> header = {
      "X-TP-Auth-Token":prefs.authToken,
    };

    Map jsonData = {
      'uid':prefs.uid,
      'maxKmsDistance':"",
      'usrLatitude':lat.toString(),
      'usrLongitude':lng.toString(),
      'cityId':""
    };


    print("Parameters getSites ${jsonData}");
    final response = await http.post(Conts.baseUrl+"sites/getRecommendSites",headers: header ,body: jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json Sites: ${response.body}");

    return response.body;
  }

  Future<dynamic>getTuristas(BuildContext context) async {

    Map<String,String> header = {
      "X-TP-Auth-Token":prefs.authToken,
    };

    Map jsonData = {
      'myUid':prefs.uid,
      'limit':"10",
      'offset':"0",

    };


    print("Parameters getTuristas ${jsonData}");
    final response = await http.post(Conts.baseUrl+"user/getRecommendedUsers",headers: header ,body: jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json Turistas: ${response.body}");

    return response.body;
  }

  Future<dynamic>rattingSite(BuildContext context,Site site,double ratting,String comment) async {

    Map<String,String> header = {
      "X-TP-Auth-Token":prefs.authToken,
    };

    Map jsonData = {
      'rating':ratting.toInt().toString(),
      'ratingComment':comment,
      'uid':prefs.uid,
      'siteId':site.siteId.toString()

    };

    print("Parameters ${jsonData}");


    print("Parameters getTuristas ${jsonData}");
    final response = await http.post(Conts.baseUrl+"ratings/createSiteRating",headers: header ,body: jsonData).timeout(Duration(seconds: 25))
        .catchError((value){
      print("Ocurrio un errorTimeout"+value);
      throw Exception(value);
    });

    print("Json rating site: ${response.body}");

    return response.body;
  }


}