import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_page_transition/page_transition_type.dart';
import 'package:ionicons/ionicons.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Models/saveSubCats.dart';
import 'package:turisco/src/Provider/SettingsProvider.dart';
import 'package:turisco/src/UI/Profile/MyInterest.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  UserSigInUpResponse user;
  Data subCats;
  SharePreference prefs = SharePreference();
  SettingsProvider settingsProvider;
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController interecesController = TextEditingController();


  @override
  void initState() {
    settingsProvider = Provider.of<SettingsProvider>(context,listen: false);
    var dataUser = jsonDecode(prefs.dataUser);
    user = UserSigInUpResponse.fromJsonMap(dataUser);
    nameController.text = user.fullName;
    emailController.text = user.email;
    descriptionController.text = user.description;
    interecesController.text = "Actualiza tus intereces";
    //var subCatsSelected = jsonDecode(prefs.subCatsSave);
    //subCats = Data.fromJson(subCatsSelected);
   // settingsProvider.getSubCatsCad(subCats.subcategories);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    settingsProvider = Provider.of<SettingsProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: _body(context),
      ),
    );
  }
  Widget _body(BuildContext context){
    return Column(
      children: [
        headerBack(context, false,'Configuración'),
        Expanded(child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(height: 30),
              itemSettingsText(context,'Nombre de usuario', Icon(Ionicons.person,color: CustomColors.blueLetter.withOpacity(.5),), nameController,(){
                FocusScope.of(context).unfocus();
                settingsProvider.serviceUpdateUser(context,nameController.text.trim(), '', '', '');
              }),
              SizedBox(height: 20),
              emailSettings(emailController),
              SizedBox(height: 20),
              intereces(interecesController, (){
                Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:MyInterestPage(), duration: Duration(milliseconds: 500)));
              }),
              SizedBox(height: 20),
              descriptionSettings(descriptionController, (){
                FocusScope.of(context).unfocus();
                settingsProvider.serviceUpdateUser(context,'', descriptionController.text.trim(), '', '');
              }),
              SizedBox(height: 20),
              passwordSettings(passwordController, settingsProvider, 'Contraseña antigua'),
              SizedBox(height: 20),
              passwordSettings(passwordController, settingsProvider,'Nueva contraseña'),
              SizedBox(height: 30),
              Padding(
                  padding:EdgeInsets.only(left: 30,right: 30,bottom: 20),
                child: btnCustom(CustomColors.blueLetter, 'Actualizar contrtaseña', (){
                  FocusScope.of(context).unfocus();
                  settingsProvider.serviceUpdateUser(context,'', '', utils.convertSha1(confirmPasswordController.text.trim()), utils.convertSha1(passwordController.text.trim()));
                }),

              )

            ],
          ),
        )),


      ],
    );
  }
}
