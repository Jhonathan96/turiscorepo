import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Models/SitesModel.dart';
import 'package:turisco/src/Provider/MyTravelsProvider.dart';
import 'package:turisco/src/UI/Home/DetailSite.dart';
import 'package:turisco/src/Utilies/Widgets.dart';

class MyTravels extends StatefulWidget {
  const MyTravels({Key key}) : super(key: key);

  @override
  _MyTravelsState createState() => _MyTravelsState();
}

class _MyTravelsState extends State<MyTravels> {

  MyTravelsProvider myTravelsProvider;

  @override
  void initState() {
    myTravelsProvider = Provider.of<MyTravelsProvider>(context,listen: false);
    myTravelsProvider.serviceGetMyTravels(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    myTravelsProvider = Provider.of<MyTravelsProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context){
    return Column(
      children: [
        headerBack(context, false, 'Mis Viajes'),
        Expanded(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 50),
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 24,right:14),
                  //height: 400,
                  width: double.infinity,
                  child: StaggeredGridView.countBuilder(
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.only(bottom: 0),
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    itemCount: myTravelsProvider.sites.length,
                    itemBuilder: (BuildContext context, int index) =>

                        itemCardTravel(myTravelsProvider.sites[index],(){
                          Site siteModel = Site(
                            image: myTravelsProvider.sites[index].image,
                            categoryId: myTravelsProvider.sites[index].categoryId,
                            cityDistance: myTravelsProvider.sites[index].cityDistance,
                            cityId: myTravelsProvider.sites[index].cityId,
                            cityName: myTravelsProvider.sites[index].cityName,
                            distance: myTravelsProvider.sites[index].distance,
                            isMyFavorite: myTravelsProvider.sites[index].isMyFavorite,
                            latitude: myTravelsProvider.sites[index].latitude,
                            locality: myTravelsProvider.sites[index].locality,
                            longitude: myTravelsProvider.sites[index].longitude,
                            owner: myTravelsProvider.sites[index].owner,
                            qualification: myTravelsProvider.sites[index].qualification,
                            myQualification: myTravelsProvider.sites[index].myQualification.toString(),
                            siteDescription: myTravelsProvider.sites[index].siteDescription,
                            siteId: myTravelsProvider.sites[index].siteId,
                            siteName: myTravelsProvider.sites[index].siteName,
                            subcategoryId: myTravelsProvider.sites[index].subcategoryId,
                            subcategoryName: myTravelsProvider.sites[index].subcategoryName,
                            //subcategoryStatus: myTravelsProvider.sites[index].subcategoryStatus
                          );

                          Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:DetailSite(site:siteModel,isMyTravel: true,), duration: Duration(milliseconds: 500)));

                        } ),


                    staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count( 1, index == 1 ? 1 : 1.17),
                    mainAxisSpacing: 13.07,
                    crossAxisSpacing: 7.74,
                  ),

                ),

                SizedBox(height: 20)
              ],
            ),
          ),
        ),
      ],
    );
  }

}
