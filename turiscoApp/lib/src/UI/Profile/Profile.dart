import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_page_transition/page_transition_type.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Provider/ProfileProvider.dart';
import 'package:turisco/src/UI/Onboarding/RegisterPage.dart';
import 'package:turisco/src/UI/Profile/MyTravels.dart';
import 'package:turisco/src/UI/Profile/Settings.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/share_preference.dart';


class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  UserSigInUpResponse user;
  SharePreference prefs = SharePreference();
  ProfileProvider profileProvider;


  @override
  void initState() {
    var dataUserJson = jsonDecode(prefs.dataUser);
    user = UserSigInUpResponse.fromJsonMap(dataUserJson);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    profileProvider = Provider.of<ProfileProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context){
    return Column(
      children: [
        headerBack(context,true,Strings.turisco),
        Expanded(
            child:SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  photoAndNameUserProfile(user),
                  SizedBox(height: 30),
                  itemProfile(context, 'Configuración',(){
                    Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:SettingsPage(), duration: Duration(milliseconds: 500)));

                  }),
                  itemProfile(context, 'Amigos',(){}),
                  itemProfile(context, 'Bloqueados',(){}),
                  itemProfile(context, 'Mis Viajes',(){
                    Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:MyTravels(), duration: Duration(milliseconds: 500)));

                  }),
                  itemProfile(context, 'Mis Chats',(){}),
                  SizedBox(height: 25),
                  Padding(
                    padding: const EdgeInsets.only(left: 50,right: 50),
                    child: btnCustom(CustomColors.red, 'Cerrar Sesión', (){
                      profileProvider.closeSession(context);
                    }),
                  ),
                  SizedBox(height: 20),

                ],
              ),
            )
        )
      ],
    );
  }
}
