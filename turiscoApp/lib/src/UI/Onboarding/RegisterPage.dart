import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Provider/RegisterProvider.dart';
import 'package:turisco/src/Services/Onboarding.dart';
import 'package:turisco/src/UI/Onboarding/InterestPage.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/utils.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  RegisterProvider registerProvider = RegisterProvider();

  @override
  void initState() {
    registerProvider = Provider.of<RegisterProvider>(context,listen: false);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   registerProvider = Provider.of<RegisterProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context){

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Stack(
          children: [

            Container(

              margin: EdgeInsets.only(top: 30,bottom: 15),
              width: double.infinity,
              alignment: Alignment.center,
              child: Text(
                Strings.turisco,
                style: TextStyle(
                    fontSize: 28,
                    fontFamily: Strings.fontGrindel,
                    color: CustomColors.blueLetter
                ),
              ),
            ),
            Positioned(
              left: 25,
              top: 40,
              child: Container(
                height: 30,
                child: GestureDetector(
                  child: Image(
                    width: 30,
                    height: 25,
                    image: AssetImage('assets/images/back.png'),
                    color: CustomColors.redTour,

                  ),
                  onTap: (){
                    Navigator.pop(context);
                  },
                ),
              ),
            ),

          ],
        ),
        Expanded(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(left:25,right: 25),
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
             
                Image(
                    fit: BoxFit.contain,
                    width: 100,
                    height: 100,
                    image: AssetImage('assets/images/mapa-de-viaje.png')
                ),
                SizedBox(height: 30),
                Container(
                  width: double.infinity,
                  alignment: Alignment.topLeft,
                  child: Text(
                    Strings.register2,
                    style: TextStyle(
                        fontSize: 22,
                        fontFamily: Strings.fontlatoBold,
                        color: CustomColors.grayTurisco
                    ),
                  ),
                ),
                SizedBox(height: 10,),
                Container(
                  width: double.infinity,
                  alignment: Alignment.topLeft,
                  child: Text(
                    Strings.registerHellow,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: Strings.fontlatoBold,
                        color: CustomColors.gray2Turisco
                    ),
                  ),
                ),
                SizedBox(height: 12),
                Container(
                  width: double.infinity,
                  alignment: Alignment.topLeft,
                  child: Text(
                    Strings.name,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: Strings.fontlatoBold,
                        color: CustomColors.redTour
                    ),
                  ),
                ),
                SizedBox(height: 4),
                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: txtBasic(context, nameController,Strings.name),
                ),
                SizedBox(height: 10),
                Container(
                  width: double.infinity,
                  alignment: Alignment.topLeft,
                  child: Text(
                    Strings.email,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: Strings.fontlatoBold,
                        color: CustomColors.redTour
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: txtEmailRegister(context, emailController),
                ),
                SizedBox(height: 10),
                Container(
                  width: double.infinity,
                  alignment: Alignment.topLeft,
                  child: Text(
                    Strings.password,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: Strings.fontlatoBold,
                        color: CustomColors.redTour
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: txtPasswordRegister(context, passwordController,registerProvider),
                ),
                SizedBox(height: 10),
                Container(
                  width: double.infinity,
                  alignment: Alignment.topLeft,
                  child: Text(
                    Strings.confirmPass,
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: Strings.fontlatoBold,
                        color: CustomColors.redTour
                    ),
                  ),
                ),

                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: txtPasswordRegister(context, confirmPasswordController,registerProvider),
                ),

                SizedBox(height: 20),

                Padding(
                  padding: EdgeInsets.only(right: 20),
                  child: Row(
                    children: [
                      registerProvider.checkTerms ? GestureDetector(
                        child: Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            border: Border.all(color: registerProvider.checkTerms ? Colors.transparent : CustomColors.gray2Turisco),
                            image: DecorationImage(
                              image: AssetImage(registerProvider.checkTerms ? 'assets/images/checked.png' : '')
                            )
                          ),

                        ),
                        onTap: (){
                          registerProvider.handleCheckTerms();
                         //
                        },
                      ) : GestureDetector(
                        child: Container(
                          width: 20,
                          height: 20,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(6)),
                            border: Border.all(color: registerProvider.checkTerms ? Colors.transparent : CustomColors.gray2Turisco),
                          ),

                        ),
                        onTap: (){
                          registerProvider.handleCheckTerms();
                          //
                        },
                      ),
                      SizedBox(width: 10,),
                      Expanded(
                        child: Text(
                          Strings.termsAndConditions,
                          style:TextStyle(
                              fontSize: 14,
                              fontFamily: Strings.fontlatoBold,
                              color: CustomColors.gray2Turisco
                          ),
                        ),
                      )
                    ],
                  )
                ),
                SizedBox(height: 40),
                btnCustom(CustomColors.redTour,Strings.continuar, (){
                  registerProvider.serviceRegister(context, emailController.text.trim(), passwordController.text.trim(),nameController.text.trim());
                 //serviceRegister(context,emailController.text.trim(), passwordController.text.trim());
                }),
                SizedBox(height: 40),

              ],
            ),
          ),
        ),
      ],
    );
  }


}
