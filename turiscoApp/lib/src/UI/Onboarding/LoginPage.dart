import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Provider/LoginProvider.dart';
import 'package:turisco/src/UI/Onboarding/RegisterPage.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  LoginProvider loginProvider;

  @override
  void initState() {
    // TODO: implement initState

    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    loginProvider = Provider.of<LoginProvider>(context);
    return Scaffold(
      body: Container(

        child: SafeArea(
            child: _body(context)
        ),
      ),
    );
  }

  Widget _body(BuildContext context){
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 30,bottom: 15),
          width: double.infinity,
          alignment: Alignment.center,
          child: Text(
            Strings.turisco,
            style: TextStyle(
                fontSize: 28,
                fontFamily: Strings.fontGrindel,
                color: CustomColors.blueLetter
            ),
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(left: 25,right: 25),
            physics: BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
               
                  Image(
                    fit: BoxFit.contain,
                      width: 100,
                      height: 100,
                      image: AssetImage('assets/images/mapa-de-viaje.png')
                  ),
                  SizedBox(height: 30),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Text(
                      Strings.login,
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.grayTurisco
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Text(
                      Strings.loginHellow,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.gray2Turisco
                      ),
                    ),
                  ),
                  SizedBox(height: 12),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Text(
                      Strings.email,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.redTour
                      ),
                    ),
                  ),
                  SizedBox(height: 4),
                  Padding(
                      padding: EdgeInsets.only(right: 20),
                    child: txtEmailLogin(context, emailController),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Text(
                      Strings.password,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.redTour
                      ),
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: txtPasswordLogin(context,passwordController, loginProvider)
                  ),
                  SizedBox(height: 20),
                  btnCustom(CustomColors.redTour,Strings.continuar, (){
                    FocusScope.of(context).unfocus();
                    loginProvider.serviceLogin(context, emailController.text.trim(), passwordController.text.trim());

                  }),
                  SizedBox(height: 20,),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    child: Text(
                      Strings.alsoLogin,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.gray2Turisco
                      ),
                    ),
                  ),
                  SizedBox(height: 20,),
                  btnCustom(CustomColors.blueFace,Strings.facebook, (){
                  }),
                  SizedBox(height: 20,),
                  Container(
                    margin: EdgeInsets.only(right: 27),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          child: Container(
                            height: 20,
                            child: Text(
                              Strings.fotgotPass,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: Strings.fontlatoBold,
                                  color: CustomColors.gray2Turisco
                              ),
                            ),
                          ),
                          onTap: (){
                            print('forgot pass');
                          },
                        ),
                        GestureDetector(
                          child: Container(
                            height: 20,
                            child: Text(
                              Strings.register,
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: Strings.fontlatoBold,
                                  color: CustomColors.redTour
                              ),
                            ),
                          ),
                          onTap: (){
                            Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:RegisterPage(), duration: Duration(milliseconds: 500)));
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 40,),
                ],
              )
          ),
        ),
      ],
    );
  }
}
