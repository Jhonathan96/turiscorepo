import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Provider/InterestProvider.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/extencions.dart';

class InterestPage extends StatefulWidget {
  const InterestPage({Key key}) : super(key: key);

  @override
  _InterestPageState createState() => _InterestPageState();
}

class _InterestPageState extends State<InterestPage> {

  InterestProvider interestProvider;

  @override
  void initState() {
    interestProvider = Provider.of<InterestProvider>(context,listen: false);
    interestProvider.serviceGetCategories(context);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context){
    interestProvider = Provider.of<InterestProvider>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
          headerBack(context,false,Strings.turisco),
          Expanded(
            child: SingleChildScrollView(
              padding: EdgeInsets.only(left: 25,right: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image(
                      fit: BoxFit.contain,
                      width: 100,
                      height: 100,
                      image: AssetImage('assets/images/mapa-de-viaje.png')
                  ),
                  SizedBox(height: 30),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Text(
                      Strings.interest,
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.grayTurisco
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Text(
                      Strings.weKnow,
                      style: TextStyle(
                          fontSize: 14,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.gray2Turisco
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),
                  ListView.builder(
                    itemCount: interestProvider.categories.length,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, int index){
                        return Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                interestProvider.categories[index].categoryName,
                                style: TextStyle(
                                    fontSize: 22,
                                    fontFamily: Strings.fontlatoBold,
                                    color: CustomColors.grayTurisco
                                ),
                              ),
                              SizedBox(height: 20),
                              ListView.builder(
                                itemCount: interestProvider.categories[index].subcategories.length,
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int indexSubCat){
                                  final Color color = HexColor.fromHex(interestProvider.categories[index].categoryColor);
                                  return itemInterest(interestProvider,interestProvider.categories[index].subcategories[indexSubCat],(){setState(() {

                                  });},color);
                              })
                            ],
                          ),
                        );
                    }),
                  SizedBox(height: 20),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Ecoturismo',
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: Strings.fontlatoBold,
                          color: CustomColors.grayTurisco
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  btnCustom(CustomColors.redTour,Strings.continuar, (){
                 // Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:HomeScreen(), duration: Duration(milliseconds: 500)));

                  interestProvider.serviceSaveSubCategories(context);
                  }),
                  SizedBox(height: 20),
                  /*Container(
                    margin: EdgeInsets.only(left: 24,right:14),
                    //height: 400,
                    width: double.infinity,
                    child: StaggeredGridView.countBuilder(
                      physics: NeverScrollableScrollPhysics(),
                      padding: EdgeInsets.only(bottom: 0),
                      shrinkWrap: true,

                      crossAxisCount: 2,
                      itemCount: 20,
                      itemBuilder: (BuildContext context, int index) =>
                          itemInterest(),
                      staggeredTileBuilder: (int index) =>
                      new StaggeredTile.count( 1,0.5),
                      mainAxisSpacing: 7,
                      crossAxisSpacing: 7.74,
                    ),

                  ),*/
                ],
              ),
            )
        )
      ],
    );
  }
}
