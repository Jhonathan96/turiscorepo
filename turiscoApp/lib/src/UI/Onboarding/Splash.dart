import 'package:animated_background/animated_background.dart';
import 'package:animated_background/particles.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Provider/GeneralProvider.dart';
import 'package:turisco/src/Provider/SplashProvider.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/GlobalVariables.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/share_preference.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {

  SplashProvider splashProvider = SplashProvider();
  GeneralProvider generalProvider;
  var bandAnimation = false;
  var bandRoute = false;
  GlobalVariables singleton = GlobalVariables();


  ParticleOptions particleOptions = ParticleOptions(
    image: Image.asset('assets/images/ic_circle2.png'),
    baseColor: Colors.white,
    spawnOpacity: 0.0,
    opacityChangeRate: 1.0,
    minOpacity: 1.0,
    maxOpacity: 1.0,
    spawnMinSpeed: 30.0,
    spawnMaxSpeed: 70.0,
    spawnMinRadius: 7.0,
    spawnMaxRadius: 15.0,
    particleCount: 20,
  );

  ParticleOptions particleOptions1 = ParticleOptions(
    image: Image.asset('assets/images/ic_circle2.png'),
    baseColor: Colors.white,
    spawnOpacity: 0.0,
    opacityChangeRate: 1.0,
    minOpacity: 1.0,
    maxOpacity: 1.0,
    spawnMinSpeed: 30.0,
    spawnMaxSpeed: 70.0,
    spawnMinRadius: 7.0,
    spawnMaxRadius: 15.0,
    particleCount: 20,
  );

  var particlePaint = Paint()
    ..style = PaintingStyle.stroke
    ..strokeWidth = 1.0;

  SharePreference prefs = SharePreference();

  @override
  void dispose() {
    super.dispose();
  }

  Behaviour _buildBehaviour() {
    return RandomParticleBehaviour(
      options: particleOptions,
      paint: particlePaint,
    );
  }

  Behaviour _buildBehaviour1() {
    return RandomParticleBehaviour(
      options: particleOptions1,
      paint: particlePaint,
    );
  }



  @override
  void initState() {
    bandAnimation = false;
    bandRoute = false;
    splashProvider = Provider.of<SplashProvider>(context,listen: false);

    Future.delayed(const Duration(seconds: 2), () {
      bandAnimation = true;
      getPermissionGps();
      //splashProvider.openApp(context);
    });
    if(prefs.accessToken == "0"){
      splashProvider.serviceAccesToken(context);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    generalProvider = Provider.of<GeneralProvider>(context);
    generalProvider.currentIndex = 0;
    return Scaffold(
      body: _body(context)
    );
  }
  
  Widget _body(BuildContext context){
    return AnimatedBackground(
      behaviour: _buildBehaviour(),
      vsync: this,
      child: AnimatedBackground(
        behaviour: _buildBehaviour1(),
        vsync: this,
        child: Stack(
          children: [
            Positioned(
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                child: Container(
                  color: CustomColors.red.withOpacity(.3)
                )
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  Image(
                      fit: BoxFit.contain,
                      width: 100,
                      height: 100,
                      image: AssetImage('assets/images/mapa-de-viaje.png')
                  ),
                  Text(
                    Strings.turisco,
                    style: TextStyle(
                        fontFamily: Strings.fontGelar,
                        color: CustomColors.blueLetter,
                        fontSize: 40
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }



  getPermissionGps() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }


    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.GRANTED) {
        splashProvider.openApp(context);
        return;
      }
    }
    _locationData = await location.getLocation();
    getLocation(_locationData);

  }

  void getLocation(LocationData locationData) async {
    singleton.latitude = locationData.latitude;
    singleton.longitude = locationData.longitude;
    splashProvider.openApp(context);
  }








}

