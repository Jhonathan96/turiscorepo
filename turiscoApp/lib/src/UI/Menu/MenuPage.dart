import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Provider/GeneralProvider.dart';
import 'package:turisco/src/UI/Home/HomePage.dart';
import 'package:turisco/src/UI/Profile/Profile.dart';
import 'package:turisco/src/Utilies/Widgets.dart';

class MenuPage extends StatefulWidget {
  const MenuPage({Key key}) : super(key: key);

  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {

  GeneralProvider generalProvider;
  List<Widget> listPages = [Container(),Container(),Container(),Container(),Container()];
  final _pageController = PageController();

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    generalProvider = Provider.of<GeneralProvider>(context);
    return Scaffold(
      body: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        children: [
          HomePage(),
          //Container(color: Colors.red),
          ProfilePage(),
          //Container(color: Colors.orange),
        ],
        onPageChanged: (index) {
          generalProvider.currentIndex = index;

        },
      ),
      bottomNavigationBar: bottomBar(generalProvider,context,_pageController),

    );
  }


}
