import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/Models/TuristasModel.dart';
import 'package:turisco/src/Provider/DetailUserProvider.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/extencions.dart';

class DetailUser extends StatefulWidget {
  final User user;
  const DetailUser({Key key,this.user}) : super(key: key);

  @override
  _DetailUserState createState() => _DetailUserState();
}

class _DetailUserState extends State<DetailUser> {

  DetailUserProvider detailUserProvider;

  @override
  void initState() {
    detailUserProvider = Provider.of<DetailUserProvider>(context,listen: false);
    detailUserProvider.serviceGetCategoriesUser(context, widget.user.uid);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    detailUserProvider = Provider.of<DetailUserProvider>(context);
    return Scaffold(body: SafeArea(child: _body(context)));
  }

    Widget _body(BuildContext context){
      return Column(
        children: [
          Container(
            height: 30,
            margin: EdgeInsets.only(left: 15,top: 10,bottom: 10),
            alignment: Alignment.topLeft,
            child: GestureDetector(
              child: Image(
                width: 30,
                height: 25,
                image: AssetImage('assets/images/back.png'),
                color: CustomColors.redTour,

              ),
              onTap: (){
                Navigator.pop(context);
              },
            ),
          ),
          Expanded(
              child:SingleChildScrollView(
                padding: EdgeInsets.only(left: 20,right: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    photoAndNameDetail(widget.user),
                    SizedBox(height: 20),
                    Text('Descripcion',style: TextStyle(
                      fontSize: 14,
                      fontFamily: Strings.fontlatoBold,
                      color: CustomColors.blueLetter
                    ),),
                    Text(
                      widget.user.description,
                      style: TextStyle(
                        fontFamily: Strings.fontlato,
                        fontSize: 15,
                        color: CustomColors.blueLetter
                      ),
                    ),
                    SizedBox(height: 20),
                    Text('Mis Intereces',style: TextStyle(
                        fontSize: 14,
                        fontFamily: Strings.fontlatoBold,
                        color: CustomColors.blueLetter
                    ),),

                      SizedBox(height: 20),
                    ListView.builder(
                        itemCount: detailUserProvider.subCatsSelected.length,
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index){
                          final Color color = HexColor.fromHex(detailUserProvider.subCatsSelected[index].subColor);
                          return interestUser(detailUserProvider.subCatsSelected[index], color);
                        })


                  ],
                ),
              )
          )
        ],
      );
    }
}
