import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Provider/TurismoProvider.dart';
import 'package:turisco/src/UI/Home/DetailSite.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/GlobalVariables.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';

class TurismoPage extends StatefulWidget {
  const TurismoPage({Key key}) : super(key: key);

  @override
  _TurismoPageState createState() => _TurismoPageState();
}

class _TurismoPageState extends State<TurismoPage> {

  TurismoProvider turismoProvider;
  LocationData location;
  GlobalVariables globalVariables = GlobalVariables();

  @override
  void initState() {
    turismoProvider = Provider.of<TurismoProvider>(context,listen: false);

  getPermissionGps();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    turismoProvider = Provider.of<TurismoProvider>(context);

    return Scaffold(
      body: SafeArea(child: _body(context)),
    );
  }

  Widget _body(BuildContext context){
    return Column(
      children: <Widget>[
        header('Turismo', context),
        Expanded(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 50),
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 24,right:14),
                  //height: 400,
                  width: double.infinity,
                  child: StaggeredGridView.countBuilder(
                    physics: NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.only(bottom: 0),
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    itemCount: turismoProvider.sites.length,
                    itemBuilder: (BuildContext context, int index) =>
                        itemCardTurismo(turismoProvider.sites[index],(){
                          Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:DetailSite(site: turismoProvider.sites[index],isMyTravel: false,), duration: Duration(milliseconds: 500)));

                        }),
                    staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count( 1, index == 1 ? 1 : 1.17),
                    mainAxisSpacing: 13.07,
                    crossAxisSpacing: 7.74,
                  ),

                ),

                SizedBox(height: 20)
              ],
            ),
          ),
        ),


      ],
    );/* Column(
      children: [
        header('Turismo', context),
        SizedBox(height: 20),
        Expanded(

          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(height: 350,color: Colors.red,),
                StaggeredGridView.countBuilder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 2,
                  itemCount: 15,
                  itemBuilder: (BuildContext context, int index) =>
                      itemCardTurismo(),
                  staggeredTileBuilder: (int index) =>
                  new StaggeredTile.count( 1,  1.17),
                  mainAxisSpacing: 13.07,
                  crossAxisSpacing: 7.74,
                ),
              ],
            )
          ),
        ),

      ],
    );*/
  }



  getPermissionGps() async {
    Location location = new Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }


    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.DENIED) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.GRANTED) {
        turismoProvider.serviceGetSites(context,globalVariables.latitude,globalVariables.longitude);
        return;
      }
    }
    _locationData = await location.getLocation();
    getLocation(_locationData);

  }

  void getLocation(LocationData locationData) async {
    globalVariables.latitude = locationData.latitude;
    globalVariables.longitude = locationData.longitude;
    turismoProvider.serviceGetSites(context,globalVariables.latitude,globalVariables.longitude);
  }


}
