import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:turisco/src/Models/SitesModel.dart';
import 'package:turisco/src/Provider/DetailSiteProvider.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/utils.dart';

class DetailSite extends StatefulWidget {
  final Site site;
  final bool isMyTravel;
  const DetailSite({Key key,this.site,this.isMyTravel}) : super(key: key);

  @override
  _DetailSiteState createState() => _DetailSiteState();
}

class _DetailSiteState extends State<DetailSite> {

  DetailSiteProvider detailSiteProvider;
  double _rating = 0.0;

  @override
  Widget build(BuildContext context) {
    detailSiteProvider = Provider.of<DetailSiteProvider>(context);
    return Scaffold(
      body: SafeArea(
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context){
    return Column(
      children: [
        Container(
          height: 30,
          margin: EdgeInsets.only(left: 15,top: 10,bottom: 10),
          alignment: Alignment.topLeft,
          child: GestureDetector(
            child: Image(
              width: 30,
              height: 25,
              image: AssetImage('assets/images/back.png'),
              color: CustomColors.redTour,

            ),
            onTap: (){
              Navigator.pop(context);
            },
          ),
        ),
        Expanded(
           child:SingleChildScrollView(
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               children: [
                 FadeInImage(
                   width: double.infinity,
                     height: 300,
                     fit: BoxFit.cover,
                     placeholder:AssetImage('assets/images/placeholderSite.png'),
                     image: NetworkImage(widget.site.image)
                 ),
                 SizedBox(height: 20),
                 Container(
                   child: RatingBar.readOnly(
                     initialRating: widget.isMyTravel ? double.parse(widget.site.myQualification) : widget.site.qualification.toDouble(),
                     isHalfAllowed: true,
                     filledColor: Colors.amber,
                     emptyColor: Colors.amber,
                     halfFilledIcon: Icons.star_half,
                     filledIcon: Icons.star,
                     emptyIcon: Icons.star_border,
                   ),
                 ),
                 SizedBox(height: 20,),
                 Padding(padding: EdgeInsets.only(left: 40,right: 40),
                    child: btnCustom(CustomColors.blueLetter, widget.isMyTravel ? 'Actualizar Rating' : 'Agregar Rating', (){
                      utils.startProgressDialogRating(context,widget.site);
                    }),
                  ),
                 SizedBox(height: 20),
                 Padding(
                   padding: const EdgeInsets.only(left: 30,right: 30),
                   child: Text(
                       widget.site.siteName,
                     style: TextStyle(
                       fontSize: 16,
                       fontFamily: Strings.fontlatoBold,
                       color: CustomColors.blueLetter
                     ),

                   ),
                 ),
                 SizedBox(height: 20),
                 Padding(
                   padding: const EdgeInsets.only(left: 30,right: 30),
                   child: Text(
                     widget.site.subcategoryName,
                     style: TextStyle(
                         fontSize: 14,
                         fontFamily: Strings.fontlato,
                         color: CustomColors.blueLetter
                     ),

                   ),
                 ),
                 SizedBox(height: 20),
                 Padding(
                   padding: const EdgeInsets.only(left: 30,right: 30),
                   child: Text(
                     widget.site.siteDescription,
                     style: TextStyle(
                         fontSize: 14,
                         fontFamily: Strings.fontlato,
                         color: CustomColors.blueLetter
                     ),

                   ),
                 ),
                 SizedBox(height: 20),
                 Padding(
                     padding:EdgeInsets.only(left: 40,right: 40),
                   child: btnWitImage(CustomColors.blueWaze, 'Ir con waze', 'assets/images/waze.png', (){

                    detailSiteProvider.openWaze(context, double.parse(widget.site.latitude), double.parse(widget.site.longitude),widget.site.siteName, widget.site.siteDescription);
                    // detailSiteProvider.launchWaze(double.parse(widget.site.latitude),double.parse(widget.site.longitude));
                   })
                 ),
                 SizedBox(height: 20),
                 Padding(
                     padding:EdgeInsets.only(left: 40,right: 40),
                     child: btnWitImage(CustomColors.red.withOpacity(.5), 'Ir con google Maps', 'assets/images/location.JPG', (){
                       // detailSiteProvider.openGoogleMaps(double.parse(widget.site.latitude),double.parse(widget.site.longitude) , widget.site.siteName);
                       detailSiteProvider.launchURLGoogleMaps(double.parse(widget.site.latitude), double.parse(widget.site.longitude));
                       
                       //detailSiteProvider.openWaze(context, double.parse(widget.site.latitude), double.parse(widget.site.longitude),widget.site.siteName, widget.site.siteDescription);
                       // detailSiteProvider.launchWaze(double.parse(widget.site.latitude),double.parse(widget.site.longitude));
                     })
                 ),
                 SizedBox(height: 20)

               ],
             ),
           )
        )
      ],
    );
  }
}
