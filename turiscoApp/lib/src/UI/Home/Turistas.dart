import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:turisco/src/Provider/TurismoProvider.dart';
import 'package:turisco/src/Provider/TuristasProvider.dart';
import 'package:turisco/src/UI/Home/DetailSite.dart';
import 'package:turisco/src/UI/Home/DetailUser.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Strings.dart';
import 'package:turisco/src/Utilies/Widgets.dart';

class TuristasPage extends StatefulWidget {
  const TuristasPage({Key key}) : super(key: key);

  @override
  _TuristasPageState createState() => _TuristasPageState();
}

class _TuristasPageState extends State<TuristasPage> {

  TuristasProvider turistasProvider;

  @override
  void initState() {
    turistasProvider = Provider.of<TuristasProvider>(context,listen: false);
    turistasProvider.serviceGetTuristas(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    turistasProvider = Provider.of<TuristasProvider>(context);

    return Scaffold(
      body: SafeArea(child: _body(context)),
    );
  }
  Widget _body(BuildContext context){
    return Column(
      children: <Widget>[
        header('Turistas', context),
        Expanded(
          child: SingleChildScrollView(
            padding: EdgeInsets.only(top: 20),
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 24,right:14),
                  //height: 400,
                  width: double.infinity,
                  child: StaggeredGridView.countBuilder(

                    physics: BouncingScrollPhysics(),
                    padding: EdgeInsets.only(bottom: 0),
                    shrinkWrap: true,
                    crossAxisCount: 2,
                    itemCount: turistasProvider.turists.length,
                    itemBuilder: (BuildContext context, int index) =>
                        itemCardTurista(turistasProvider.turists[index],(){
                          Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:DetailUser(user:turistasProvider.turists[index] ,), duration: Duration(milliseconds: 500)));

                        }),
                    staggeredTileBuilder: (int index) =>
                    new StaggeredTile.count( 1, index == 1 ? 1 : 1.17),
                    mainAxisSpacing: 13.07,
                    crossAxisSpacing: 7.74,

                  ),

                ),

                SizedBox(height: 20)
              ],
            ),
          ),
        ),


      ],
    );/* Column(
      children: [
        header('Turismo', context),
        SizedBox(height: 20),
        Expanded(

          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(height: 350,color: Colors.red,),
                StaggeredGridView.countBuilder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  crossAxisCount: 2,
                  itemCount: 15,
                  itemBuilder: (BuildContext context, int index) =>
                      itemCardTurismo(),
                  staggeredTileBuilder: (int index) =>
                  new StaggeredTile.count( 1,  1.17),
                  mainAxisSpacing: 13.07,
                  crossAxisSpacing: 7.74,
                ),
              ],
            )
          ),
        ),

      ],
    );*/
  }
}
