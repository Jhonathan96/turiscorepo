import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:turisco/src/Models/OnboardingModel.dart';
import 'package:turisco/src/UI/Home/Turistas.dart';
import 'package:turisco/src/UI/Home/turismo.dart';
import 'package:turisco/src/Utilies/Colors.dart';
import 'package:turisco/src/Utilies/Widgets.dart';
import 'package:turisco/src/Utilies/share_preference.dart';
import 'package:turisco/src/Utilies/utils.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}



class _HomePageState extends State<HomePage> {

  UserSigInUpResponse user;
  SharePreference prefs = SharePreference();

  @override
  void initState() {
    var dataUserJson = jsonDecode(prefs.dataUser);
    user = UserSigInUpResponse.fromJsonMap(dataUserJson);
    Random random = new Random();
    int randomNumber = random.nextInt(10);
    if(randomNumber == 5){
     // utils.startProgress(context);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.whiteBackGround,
      body: SafeArea(
        child: _body(context),
      ),
    );
  }

  Widget _body(BuildContext context){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(height: 40),
              photoAndNameUser(user),
              SizedBox(height: 40),
              Padding(
                padding:EdgeInsets.only(left: 20,right: 20),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(child: itemMenu(context, 'assets/images/Captura.JPG', 'Eventos', 'Conoce tus eventos mas cercanos', (){utils.showSnackBarGood(context, 'Proximamente');})),
                        SizedBox(width: 15),
                        Expanded(child: itemMenu(context, 'assets/images/Perfil2.JPG', 'Turistas', 'Interactua con los turistas mas cercanos a ti.', (){
                          Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:TuristasPage(), duration: Duration(milliseconds: 500)));

                        }))
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(child: itemMenu(context, 'assets/images/location.JPG', 'Turismo', 'El turismo que se acomoda a tus preferencias', (){
                          Navigator.of(context).push(PageTransition(type: PageTransitionType.slideInLeft, child:TurismoPage(), duration: Duration(milliseconds: 500)));

                        })),
                        SizedBox(width: 15),
                        Expanded(child: itemMenu(context, 'assets/images/live.JPG', 'En Vivo', 'Conectate y comparte en vivo tus experiencias', (){utils.showSnackBarGood(context, 'Proximamente');}))
                      ],
                    ),

                  ],
                ),
              )
            ],
          ),
        )),

      ],
    );
  }
}
