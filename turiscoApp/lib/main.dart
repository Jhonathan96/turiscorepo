import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'package:provider/provider.dart';
import 'package:turisco/src/Provider/DetailSiteProvider.dart';
import 'package:turisco/src/Provider/DetailUserProvider.dart';
import 'package:turisco/src/Provider/GeneralProvider.dart';
import 'package:turisco/src/Provider/InterestProvider.dart';
import 'package:turisco/src/Provider/LoginProvider.dart';
import 'package:turisco/src/Provider/MenuProvider.dart';
import 'package:turisco/src/Provider/MyInterestProvider.dart';
import 'package:turisco/src/Provider/MyTravelsProvider.dart';
import 'package:turisco/src/Provider/ProfileProvider.dart';
import 'package:turisco/src/Provider/RegisterProvider.dart';
import 'package:turisco/src/Provider/SettingsProvider.dart';
import 'package:turisco/src/Provider/SplashProvider.dart';
import 'package:turisco/src/Provider/TurismoProvider.dart';
import 'package:turisco/src/Provider/TuristasProvider.dart';
import 'package:turisco/src/UI/Onboarding/LoginPage.dart';
import 'package:turisco/src/UI/Onboarding/Splash.dart';
import 'package:turisco/src/Utilies/share_preference.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = SharePreference();
  await prefs.initPrefs();
  runApp(
      EasyLocalization(
        child: MyApp(),
        path: 'assets/langs',
        supportedLocales: MyApp.list,
        saveLocale: true,
        useOnlyLangCode: true,
      ),
      );
}

class MyApp extends StatelessWidget {
  static const list = [
    Locale('en'),
    Locale('es'),
    Locale('ar'),
  ];
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_)=> LoginProvider()),
        ChangeNotifierProvider(create: (_)=> RegisterProvider()),
        ChangeNotifierProvider(create: (_)=> InterestProvider()),
        ChangeNotifierProvider(create: (_)=> MenuProvider()),
        ChangeNotifierProvider(create: (_)=> SplashProvider()),
        ChangeNotifierProvider(create: (_)=> GeneralProvider()),
        ChangeNotifierProvider(create: (_)=> TurismoProvider()),
        ChangeNotifierProvider(create: (_)=> TuristasProvider()),
        ChangeNotifierProvider(create: (_)=> ProfileProvider()),
        ChangeNotifierProvider(create: (_)=> SettingsProvider()),
        ChangeNotifierProvider(create: (_)=> MyInterestProvider()),
        ChangeNotifierProvider(create: (_)=> DetailSiteProvider()),
        ChangeNotifierProvider(create: (_)=> DetailUserProvider()),
        ChangeNotifierProvider(create: (_)=> MyTravelsProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        title: 'TurisCo',
        initialRoute: 'splash',
        routes: <String,WidgetBuilder>{
          'login': (BuildContext context) => LoginPage(),
          'splash': (BuildContext context) => SplashPage(),
        },
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
        ),

      ),
    );
  }
  /// Languages that are Right to Left
  static List<String> RTL_LANGUAGES = ["ar", "ur", "he", "dv", "fa"];

  // Static function to determine the device text direction RTL/LTR
  static bool isRTL() {
    /// Device language
    final locale = _getLanguageCode();
    print('Locale Detected ${locale}');
    return RTL_LANGUAGES.contains(locale);
  }

  static String _getLanguageCode() {
    try {
      return ui.window.locale.languageCode.toLowerCase();
    } catch (e) {
      return null;
    }
  }

}




