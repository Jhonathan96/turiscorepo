<?php

namespace App\Database\Seeds;

use App\Models\City;

class CitySeeder extends \CodeIgniter\Database\Seeder
{
  public function run()
  {
    $data = file_get_contents(APPPATH . '/Libraries/Kubo/complements/cities.json');
    $data = json_decode($data, true);
    try {
      // Add Data
      $seeder = new City();
      foreach ($data as $key => $value) {
        $seeder->insert($value);
      }
      dump('Success City');
    } catch (\Exception $ex) {
      //throw $th;
      dump($ex->getMessage());
    }
  }
}
