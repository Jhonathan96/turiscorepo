<?php

namespace App\Database\Seeds;

use App\Models\Country;

class CountrySeeder extends \CodeIgniter\Database\Seeder
{
  public function run()
  {
    $data = file_get_contents(APPPATH . '/Libraries/Kubo/complements/countries.json');
    $data = json_decode($data, true);
    $callingCode = file_get_contents(APPPATH . '/Libraries/Kubo/complements/callingCodes.json');
    $callingCode = json_decode($callingCode, true);
    try {
      // Add Data
      $seeder = new Country();
      foreach ($data as $key => $value) {
        $seeder->insert($value);
      }
      // Calling Code
      foreach ($callingCode as $key => $value) {
        $seeder->update($value['countryId'], [
          'callingCode' => $value['code']
        ]);
      }
      dump('Success Country');
    } catch (\Exception $ex) {
      //throw $th;
      dump($ex->getMessage());
    }
  }
}
