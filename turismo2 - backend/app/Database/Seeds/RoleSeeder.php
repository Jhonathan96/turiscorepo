<?php

namespace App\Database\Seeds;

use App\Models\Role;

class RoleSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $seed = new Role();
        $data = [
            // Admin
            'admin',
            // Comun DB
            'onlyUser', 'valid', 'user', 'company',
            // Developers
            'backend', 'frontend', 'ios', 'android'
        ];
        foreach ($data as $key => $value) {
            $seed->insert(['name' => $value]);
        }
    }
}