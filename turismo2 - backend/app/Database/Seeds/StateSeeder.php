<?php

namespace App\Database\Seeds;

use App\Models\State;

class StateSeeder extends \CodeIgniter\Database\Seeder
{
  public function run()
  {
    $data = file_get_contents(APPPATH . '/Libraries/Kubo/complements/states.json');
    $data = json_decode($data, true);
    try {
      // Add Data
      $seeder = new State();
      foreach ($data as $key => $value) {
        $seeder->insert($value);
      }
      dump('Success State');
    } catch (\Exception $ex) {
      //throw $th;
      dump($ex->getMessage());
    }
  }
}
