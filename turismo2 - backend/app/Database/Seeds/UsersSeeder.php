<?php

namespace App\Database\Seeds;

use App\Models\Users;

class UsersSeeder extends \CodeIgniter\Database\Seeder
{
  public function run()
  {
    /**
     * {
     * "name": "Kubo S.A.S.",
     * "email": "admin@kubo.co",
     * "phone": "(+57) 1 7424249",
     * "password": "Kubo.co2020"
     * }
     */
    $seed = new Users();
    $data = [
      "uid" => "dac56d12bffd9ae1f5a82a5b3ab84ce992bb02e95e57d14aacdac",
      "name" => "Kubo S.A.S",
      "email" => "admin@kubo.co",
      "phone" => "(+57) 1 7424249",
      "password" => '$2y$10$ohoqWFAHwBlN34bX4kvMIOyBPli77m0yoxT83y/YuxVHINhBn3Lg2',
      "tries" => 0,
      "valid" => 1,
      "hasCompany" => 0,
      "roleId" => 1
    ];
    $seed->insert($data);
  }
}
