<?php

namespace App\Database\Seeds;

use App\Models\Competition;
use PHP_CodeSniffer\Tokenizers\Comment;

class CompetitionSeeder extends \CodeIgniter\Database\Seeder
{
  public function run()
  {
    $seed = new Competition();
    $data = [];
    $data[] = [
        'name'          => 'Concurso de habilidades Free stryle - Fútbol',
        'startDate'     => date('Y-m-d'),
        'endDate'       => date('Y-m-d', strtotime(date('Y-m-d') . ' +15 day')),
        'description'   => 'Demuestras tus habilidades con el balón y gana 200 mil pesos',
        'status'        => 'ACTIVE'
    ];
    $data[] = [
        'name'          => 'Concurso de Cocina - Mejor Pastel',
        'startDate'     => date('Y-m-d'),
        'endDate'       => date('Y-m-d', strtotime(date('Y-m-d') . ' +15 day')),
        'description'   => 'Demuestras tus habilidades haciendo un rico pastel',
        'status'        => 'ACTIVE'
    ];
    foreach ($data as $key => $record) 
        $seed->insert($record);
    
    
  }
}
