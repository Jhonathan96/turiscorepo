<?php

namespace App\Database\Seeds;

use Exception;
use App\Models\Platform;

class PlatformSeeder extends \CodeIgniter\Database\Seeder
{

	public function run()
	{
		$arrayMain = [
			[
				'name'      => NAME_PROJECT . ' - Application Programming Interface',
				'shortname' => SHORT_NAME_PROJECT . ' - API',
			],
			[
				'name'      => NAME_PROJECT . ' - Application IPhone Os',
				'shortname' => SHORT_NAME_PROJECT . ' - App IOS',
			],
			[
				'name'      => NAME_PROJECT . ' - Application Android',
				'shortname' => SHORT_NAME_PROJECT . ' - App Android',
			],
			[
				'name'      => NAME_PROJECT . ' - Back Office',
				'shortname' => SHORT_NAME_PROJECT . ' - BackOffice',
			],
			[
				'name'      => NAME_PROJECT . ' - WEB',
				'shortname' => SHORT_NAME_PROJECT . ' - Web',
			],
			[
				'name'      => NAME_PROJECT . ' - Flutter',
				'shortname' => SHORT_NAME_PROJECT . ' - Flutter',
			],

		];
		try
		{
			// Add Data
			$seeder = new Platform();
			foreach ($arrayMain as $key => $value)
			{
				$seeder->insert($value);
			}
		}
		catch (Exception $ex)
		{
			//throw $th;
			dump($ex->getMessage());
		}
	}
}