<?php

namespace App\Models;

use CodeIgniter\Model;

class State extends Model
{
    // Data Base
    protected $table = 'state';
    protected $primaryKey = 'stateId';

    // Allowed Fields
    protected $allowedFields = [
        'name', 'longitude', 'latitude',
        'countryId',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    // Foreign Keys
    public function cities()
    {
        $response = $this->join("city", "state.stateId = city.stateId");
        return $response;
    }

    public function country()
    {
        $response = $this->join("country", "state.countryId = country.countryId");
        return $response;
    }
}