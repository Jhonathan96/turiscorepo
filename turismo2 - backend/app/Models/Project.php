<?php

namespace App\Models;

use CodeIgniter\Model;

class Project extends Model
{
    // Data Base
    protected $table = 'project';
    protected $primaryKey = 'projectId';

    // Allowed Fields
    protected $allowedFields = [
        'name', 'changelog', 'version', 'deployment',
        'platformId', 'uid',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';

    // Validation Rules
    protected $skipValidation     = false;
    public $validationRules = [
        'name' => 'required|regex_match[/(\p{L})(.+)/iu]'
    ];

    public $validationMessages = [
        'name' => [
            'required' => 'El nombre es requerido.',
            'regex_match' => 'El nombre tiene caracteres no permitidos'
        ],
    ];

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    // Foreign Keys
    public function users()
    {
        $response = $this->join("users", "users.roleId = role.roleId");
        return $response;
    }
}