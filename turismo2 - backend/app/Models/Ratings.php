<?php

namespace App\Models;

use CodeIgniter\Model;

class Ratings extends Model
{
    // Data Base
    protected $table = 'ratings';
    protected $primaryKey = 'ratingId';

    // Allowed Fields
    protected $allowedFields = [
        'ratingId', 'rating', 'ratingComment', 'uid', 'siteId', 'status'
    ];

    protected $useTimestamps = false;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 
    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function getDataSiteRatingByUser($uid, $siteId){
        return $this->where([
            "uid" => $uid,
            "siteId" => $siteId
        ])->find();
    }

    public function getOnlySiteRatingByUser($uid, $siteId){
        $dataRatings = $this
        ->select("rating")
        ->where([
            "uid" => $uid,
            "siteId" => $siteId
        ])->findAll(1,0);
        foreach ($dataRatings as $dataRating) {
            return round($dataRating->rating,1);
        }
        return null;
    }

    public function createSiteRating($rating, $ratingComment, $uid, $siteId){
        return $this->insert([
            "rating" => $rating,
            "ratingComment" => $ratingComment,
            "uid" => $uid,
            "siteId" => $siteId,
            "status" => "ACTIVE"
        ]);
    }

    public function updateSiteRating($ratingId, $rating, $ratingComment, $uid, $siteId){
        return $this->update(
            $ratingId,
            [
            "rating" => $rating,
            "ratingComment" => $ratingComment,
            "uid" => $uid,
            "siteId" => $siteId,
            "status" => "ACTIVE"
            ]
        );
    }

    public function getSubcategoriesIdsByUserRatings($uid){
        return $this->select("subcategories.subcategoryId")
        ->where([
            "ratings.uid" => $uid,
            "ratings.rating >=" => 4,
            "subcategories.status" => "ACTIVE"
        ])
        ->join("sites","sites.siteId=ratings.siteId","LEFT OUTER")
        ->join("subcategories","subcategories.subcategoryId=sites.subcategoryId","LEFT OUTER")
        ->distinct("subcategories.subcategoryId")
        ->findAll();
    }

    public function getUserRatingsPerSubcategories($uid, $whereSubcategories, $cityId, $inArray = false){

        $whereActiveItems = " AND sites.status='ACTIVE' AND subcategories.status='ACTIVE' AND categories.status='ACTIVE'";
        $whereUid = " AND ratings.uid='" . $uid . "'";
        $queryWhere = $whereSubcategories . $whereUid . $whereActiveItems;
        if ($cityId) {
            $queryWhere .= " AND sites.cityId=". $cityId;
        }

        $dataRatings = $this->select("ratings.siteId, ratings.rating")
        ->join("sites","sites.siteId=ratings.siteId","LEFT OUTER")
        ->join("subcategories","subcategories.subcategoryId=sites.subcategoryId","LEFT OUTER")
        ->join("categories","categories.categoryId=subcategories.categoryId","LEFT OUTER")
        ->where($queryWhere)->findAll();

        if ($inArray) {
            $array = [];
            foreach ($dataRatings as $dataRating) {
                $array["id".$dataRating->siteId] = $dataRating->rating;
            }
            return $array;
        }
        
        return $dataRatings;
    }

    public function getUserRatingsPerSubcategoriesWithLocation($uid, $whereSubcategories, $maxKmsDistance, $usrLatitude, $usrLongitude, $inArray = false){

        $selectLocation = "(6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";

        $whereActiveItems = " AND sites.status='ACTIVE' AND subcategories.status='ACTIVE' AND categories.status='ACTIVE'";
        $whereUid = " AND ratings.uid='" . $uid . "'";
        $queryWhere = $whereSubcategories . $whereUid . $whereActiveItems;

        $dataRatings = $this->select("ratings.siteId, ratings.rating, " . $selectLocation)
        ->join("sites","sites.siteId=ratings.siteId","LEFT OUTER")
        ->join("subcategories","subcategories.subcategoryId=sites.subcategoryId","LEFT OUTER")
        ->join("categories","categories.categoryId=subcategories.categoryId","LEFT OUTER")
        ->where($queryWhere)
        ->having("distance <",  $maxKmsDistance)
        ->findAll();

        if ($inArray) {
            $array = [];
            foreach ($dataRatings as $dataRating) {
                $array["id".$dataRating->siteId] = $dataRating->rating;
            }
            return $array;
        }
        
        return $dataRatings;
    }

    public function getUserAllRatings($uid, $inArray = false){

        $whereActiveItems = " AND sites.status='ACTIVE' AND subcategories.status='ACTIVE' AND categories.status='ACTIVE'";
        $whereUid = "ratings.uid='" . $uid . "'";
        $queryWhere = $whereUid . $whereActiveItems;

        $dataRatings = $this->select("ratings.siteId, ratings.rating")
        ->join("sites","sites.siteId=ratings.siteId","LEFT OUTER")
        ->join("subcategories","subcategories.subcategoryId=sites.subcategoryId","LEFT OUTER")
        ->join("categories","categories.categoryId=subcategories.categoryId","LEFT OUTER")
        ->where($queryWhere)->findAll();

        if ($inArray) {
            $array = [];
            foreach ($dataRatings as $dataRating) {
                $array["id".$dataRating->siteId] = $dataRating->rating;
            }
            return $array;
        }
        
        return $dataRatings;
    }

    public function getAllSiteRatings($myUid, $siteId, $limit, $offset){
        $siteRatings = $this->select("ratings.ratingId, ratings.rating, ratings.ratingComment, 
        ratings.uid, ratings.siteId, ratings.created_at as ratingCreatedAt, 
        users.fullName as userFullName, users.email as userEmail, users.image as userImage")
        ->join("users", "users.uid=ratings.uid", "LEFT OUTER")
        ->where([
            "ratings.siteId" => $siteId,
            "ratings.uid !=" => $myUid,
            "users.status" => "ACTIVE"
        ])->findAll($limit, $offset);

        return $siteRatings;
    }

    public function countAllSiteRatings($myUid, $siteId){
        $siteRatings = $this->select("ratings.ratingId, ratings.rating, ratings.ratingComment, 
        ratings.uid, ratings.siteId, ratings.created_at as ratingCreatedAt, 
        users.fullName as userFullName, users.email as userEmail, users.image as userImage")
        ->join("users", "users.uid=ratings.uid", "LEFT OUTER")
        ->where([
            "ratings.siteId" => $siteId,
            "ratings.uid !=" => $myUid,
            "users.status" => "ACTIVE"
        ])->findAll();
        return count($siteRatings);
    }
    
    public function getSiteRatingByUser($uid, $siteId){
        return $this->select("ratings.ratingId, ratings.rating, ratings.ratingComment, 
        ratings.uid, ratings.siteId, ratings.created_at as ratingCreatedAt, 
        users.fullName as userFullName, users.email as userEmail, users.image as userImage")
        ->join("users", "users.uid=ratings.uid", "LEFT OUTER")
        ->where([
            "ratings.uid" => $uid,
            "ratings.siteId" => $siteId
        ])->first();
    }
}