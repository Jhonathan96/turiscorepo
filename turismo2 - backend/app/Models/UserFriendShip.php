<?php

namespace App\Models;

use CodeIgniter\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class UserFriendShip extends Model
{
    // Data Base
    protected $table = 'userFriendShip';
    protected $primaryKey = 'userFriendshipId';

    // Allowed Fields
    protected $allowedFields = [
        'userFriendshipId', 'senderUid', 'addresseeUid', 'status'
    ];

    protected $useTimestamps = false;
    protected $statusFields = ['status'];

    // Return Data
    protected $returnType = 'object';
 
    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function changeStatusToRelationFriendship($userFriendshipId, $newStatus){
        return $this->update($userFriendshipId, [
            "status" => $newStatus
        ]);
    }

    public function createRelationFriendship($senderUid, $addresseeUid, $status){
        return $this->insert([
            "senderUid" => $senderUid,
            "addresseeUid" => $addresseeUid,
            "status" => $status
        ]);
    }

    public function deleteRelationFriendship($userFriendshipId){
        return $this->delete($userFriendshipId);
    }

    public function getRelationFriendship($senderUid, $addresseeUid){
        
        $relationFriendship = $this->select("userFriendshipId, status as friendshipStatus")->where([
            "senderUid" => $senderUid,
            "addresseeUid" => $addresseeUid
        ])->first();
        if (!$relationFriendship) {
            $relationFriendship = $this->select("userFriendshipId, status as friendshipStatus")->where([
                "addresseeUid" => $senderUid,
                "senderUid" => $addresseeUid
            ])->first();
        }

        return $relationFriendship ? $relationFriendship : null;
    }

    public function getRelationFriendshipWithStatus($senderUid, $addresseeUid, $status){
        
        $relationFriendship = $this->select("userFriendshipId, status as friendshipStatus")->where([
            "senderUid" => $senderUid,
            "addresseeUid" => $addresseeUid,
            "status" => $status
        ])->first();
        if (!$relationFriendship) {
            $relationFriendship = $this->select("userFriendshipId, status as friendshipStatus")->where([
                "addresseeUid" => $senderUid,
                "senderUid" => $addresseeUid,
                "status" => $status
            ])->first();
        }

        return $relationFriendship ? $relationFriendship : null;
    }

    public function getRelationFriendshipSentBy($senderUid, $addresseeUid){
        $relationFriendship = $this->select("userFriendshipId, status as friendshipStatus")->where([
            "senderUid" => $senderUid,
            "addresseeUid" => $addresseeUid
        ])->first();

        return $relationFriendship ? $relationFriendship : null;
    }

    public function getRelationFriendshipStatusSentBy($senderUid, $addresseeUid, $status){
        $relationFriendship = $this->where([
            "senderUid" => $senderUid,
            "addresseeUid" => $addresseeUid,
            "status" => $status
        ])->first();
        
        return $relationFriendship ? $relationFriendship : null;
    }

    public function updateUserFriendship($userFriendshipId, $senderUid, $addresseeUid, $newStatus){
        return $this->update($userFriendshipId, [
            "senderUid" => $senderUid,
            "addresseeUid" => $addresseeUid,
            "status" => $newStatus
        ]);
    }

    public function blockUserWithVerification($myUid, $otherUid, $userFriendshipId){
        $existRelationshipBy = $this->getRelationFriendshipSentBy($myUid, $otherUid);
        if ($existRelationshipBy) {
            $this->changeStatusToRelationFriendship($existRelationshipBy->userFriendshipId, "BLOCKED");
            return $existRelationshipBy->userFriendshipId;
        }else{
            $this->updateUserFriendship($userFriendshipId, $myUid, $otherUid, "BLOCKED");
            return $userFriendshipId;
        }
    }

    public function getAllFriendRelationship($myUid, $limit, $offset){
        $where = "status='FRIEND' AND (senderUid = '" . $myUid . "' OR addresseeUid = '" . $myUid . "')";
        return $this->select("userFriendshipId, senderUid, addresseeUid")->where($where)
        ->orderBy("userFriendshipId", "DESC")
        ->findAll($limit, $offset);
    }

    public function getAllBlockedRelationship($myUid, $limit, $offset){
        return $this->select("userFriendshipId, senderUid, addresseeUid")->where([
            "senderUid" => $myUid,
            "status" => "BLOCKED"
        ])->orderBy("userFriendshipId", "DESC")
        ->findAll($limit, $offset);
    }

    public function getAllFriendRequest($myUid, $limit, $offset){
        return $this->select("userFriendshipId, senderUid, addresseeUid")->where([
            "addresseeUid" => $myUid,
            "status" => "REQUEST"
        ])->orderBy("userFriendshipId", "DESC")
        ->findAll($limit, $offset);
    }

    public function countFriendRequest($myUid){
        $allFriendRequest = $this->where([
            "addresseeUid" => $myUid,
            "status" => "REQUEST"
        ])->findAll();
        return count($allFriendRequest);
    }

    public function countAllFriends($myUid){
        $where = "status='FRIEND' AND (senderUid = '" . $myUid . "' OR addresseeUid = '" . $myUid . "')";
        $allFriends = $this->where($where)->findAll();
        return count($allFriends);
    }

    public function countAllBlocked($myUid){
        $allBlocked = $this->where([
            "senderUid" => $myUid,
            "status" => "BLOCKED"
        ])->findAll();
        return count($allBlocked);
    }

}