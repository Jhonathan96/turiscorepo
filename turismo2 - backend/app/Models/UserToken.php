<?php

namespace App\Models;

use CodeIgniter\Model;

class UserToken extends Model
{
    // Data Base
    protected $table = 'usertoken';
    protected $primaryKey = 'userTokenId';

    protected $allowedFields = [
        'uid', 'authToken',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        // 'status', 
        'created_at', 'updated_at', 'deleted_at'
    ];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }
}