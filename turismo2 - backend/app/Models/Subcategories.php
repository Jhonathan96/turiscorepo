<?php

namespace App\Models;

use CodeIgniter\Model;

class Subcategories extends Model
{
    // Data Base
    protected $table = 'subcategories';
    protected $primaryKey = 'subcategoryId';

    // Allowed Fields
    protected $allowedFields = [
        'subcategoryName', 'categoryId', 'status'
    ];

    // protected $useTimestamps = true;
    protected $useTimestamps = false;
    // protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';
    // protected $deletedField = 'deleted_at';
    // protected $statusFields = [
    //     'status', 'created_at', 'updated_at', 'deleted_at'
    // ];
    protected $statusFields = ['status'];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function allSubcategories(){
        $allSubcategories = $this->findAll();
        return $allSubcategories;
    }

    public function subcategoriesByCategoryId($categoryId){
        return $this->select("subcategoryId, subcategoryName, status as subcategoryStatus")
        ->where([
            "categoryId" => $categoryId
        ])->findAll();
    }

}