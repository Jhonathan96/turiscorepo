<?php

namespace App\Models;

use CodeIgniter\Model;

class PasswordReset extends Model
{
    // Data Base
    protected $table = 'passwordReset';
    protected $primaryKey = 'passwordResetId';

    protected $allowedFields = [
        'uid', 'code', 
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // Return Data
    protected $returnType = 'object';

    public $validationRules = [];

    public $validationMessages = [
        'code' => [
            'required' => 'El código es requerido.',
            'numeric' => 'El código debe contener números.',
        ],
        'password' => [
            'required' => 'La contraseña es requerido.',
            'min_length' => 'La contraseña debe tener mínimo 8 caracteres.',
            'regex_match' => 'La contraseña debe tener mínimo ocho caracteres, al menos una letra mayúscula, una letra minúscula, un número y un carácter especial.'
        ]
    ];

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }
}

