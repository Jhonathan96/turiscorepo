<?php

namespace App\Models;

use CodeIgniter\Model;

class Categories extends Model
{
    // Data Base
    protected $table = 'categories';
    protected $primaryKey = 'categoryId';

    // Allowed Fields
    protected $allowedFields = [
        'categoryName', 'categoryColor', 'status'
    ];

    // protected $useTimestamps = true;
    protected $useTimestamps = false;
    // protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';
    // protected $deletedField = 'deleted_at';
    // protected $statusFields = [
    //     'status', 'created_at', 'updated_at', 'deleted_at'
    // ];
    protected $statusFields = ['status'];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function getAllCategories(){
        return $this->select("categoryId, categoryName, categoryColor, status as categoryStatus")->findAll();
    }

    public function getAllCategoriesFilterByName($categoryNameLike){
        return $this->select("categoryId, categoryName, categoryColor, status as categoryStatus")
        ->like('categoryName', $categoryNameLike, 'both')->findAll();
    }

    public function getAllCategoriesByStatus($status){
        return $this->select("categoryId, categoryName, categoryColor")
        ->where([
            "status" => $status
        ])->findAll();
    }
}