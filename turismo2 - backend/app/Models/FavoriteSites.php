<?php

namespace App\Models;

use CodeIgniter\Model;

class FavoriteSites extends Model
{
    // Data Base
    protected $table = 'favoriteSites';
    protected $primaryKey = 'favoriteSiteId';

    // Allowed Fields
    protected $allowedFields = ['favoriteSiteId', 'uid', 'siteId', 'created_at', 'updated_at'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';
    // protected $deletedField = 'deleted_at';
    // protected $statusFields = ['status', 'created_at', 'updated_at', 'deleted_at'];
    protected $statusFields = [
        'status', 'created_at', 'updated_at'
    ];
    protected $useSoftDeletes = false;

    // Return Data
    protected $returnType = 'object';
 
    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function getSiteFavoriteByUser($uid, $siteId){
        return $this->where([
            "uid" => $uid,
            "siteId" => $siteId
        ])->first();
    }

    public function deleteSiteFavoriteByUser($favoriteSiteId){
        return $this->delete(["favoriteSiteId" => $favoriteSiteId]);
    }

    public function createSiteFavoriteByUser($uid, $siteId){
        return $this->insert([
            "uid" => $uid,
            "siteId" => $siteId
        ]);
    }

}