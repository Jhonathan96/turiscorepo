<?php

namespace App\Models;

use CodeIgniter\Model;
use App\Models\UserFriendShip;
use App\Models\Sites;

class Users extends Model
{
    // Data Base
    protected $table = 'users';

    protected $primaryKey = 'uid';
    protected $allowedFields = [
        'uid', 'fullName', 'email', 'accountType', 'password', 'tries', 'valid', 'roleId', 
        'image', 'description', 'status'
    ];
    protected $hiddenFields = [
        'password', 'codeVerification'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';

    // For use query builder
    protected $builder;

    // Validation Rules
    protected $skipValidation     = false;
    public $validationRules = [
        // 'name' => 'required|regex_match[/(\p{L})(.+)/iu]',
        'email' => 'required|valid_email', // |is_unique[user.email]
        // 'phone' => 'required|is_unique[user.phone]',
        // 'password' => 'required|min_length[8]',
    ];

    public $validationMessages = [
        'fullname' => [
            'required' => 'El nombre es requerido.',
            'regex_match' => 'El nombre tiene caracteres no permitidos'
        ],
        'email' => [
            'required' => 'El correo electrónico es requerido.',
            'valid_email' => 'El correo electrónico es invalido',
            'is_unique' => 'Lo sentimos, ese correo electrónico ya ha sido tomado. Por favor, elige otro.',
        ],
        'password' => [
            'required' => 'La contraseña es requerido.',
            'min_length' => 'La contraseña debe tener mínimo 8 caracteres.',
            //'regex_match' => 'La contraseña debe tener mínimo ocho caracteres, al menos una letra mayúscula, una letra minúscula, un número y un carácter especial.'
        ]
    ];

    public function __construct()
    {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);

        // allow use query builder class
        $this->builder = $this->db->table($this->table);
    }

    // Foreign Keys
    public function role()
    {
        // join(string $table, string $cond, string $type = '', bool $escape = null)
        $response = $this->join("role", "users.roleId = role.roleId");
        return $response;
    }

    public function getUserDataByUid($uid){
        $userData = $this->where(["uid" => $uid])->find();
        return $userData;
    }

    public function getBasicDataUser(string $uid)
    {
        $query =  $this->builder
            ->select('uid, fullName, countryId, country, cityId, city, locationId, location, state, stateId, dateBirth, document, documentType, email, accountType, phone, image, hasCompany, roleId, status, updated_at, created_at, password')
            ->where('uid', $uid)
            ->get();
        $user = $query->getRow();
        // $user->profileImage = checkIsEmpty($user->profileImage) ? null :  BASE_URL_PROFILE_IMAGE . $user->profileImage;
        return $user;
    }

    public function getAllDataUser(string $email)
    {
        var_dump($email);
        $query =  $this
            ->select('uid, fullName, countryId, country, cityId, city, locationId, location, state, stateId, dateBirth, document, documentType, email, accountType, phone, image, hasCompany, roleId, status, updated_at, created_at, password')
            ->where(['email' => $email, 'status' => 'ACTIVE'])
            ->get();
        $user = $query->getRow();
        var_dump($user);
        // $user->profileImage = checkIsEmpty($user->profileImage) ? null :  BASE_URL_PROFILE_IMAGE . $user->profileImage;
        return $user;
    }


    public function updateImageName(string $uid, string $fileName)
    {
        $this->builder->where('uid', $uid);
        $this->builder->update(['image' => $fileName]);
    }

    public function getAllUserIds($inArray = false){
        if($inArray){
            $allUsersIds = [];
            $allUsersData = $this->select("uid")->findAll();

            foreach ($allUsersData as $user) {
                array_push($allUsersIds, $user->uid);
            }

            return $allUsersIds;
        }
        return $this->select("uid")->findAll();
    }

    public function getFullUserDataByUid($myUid, $uid){
        $userData = $this->select("uid, fullName, email, description, CONCAT('".BASE_URL_IMGS_USER."',image) as image, status as userStatus, created_at as userCreatedAt")
        ->where([
            "uid" => $uid
        ])->first();

        $userData->friend = $this->isFriend($myUid, $uid);
        $userData->sentFriendRequest = $this->sentFriendRequest($myUid, $uid);
        $userData->receivedFriendRequest = $this->receivedFriendRequest($myUid, $uid);
        $userData->iBlockedHim = $this->iBlockedHim($myUid, $uid);
        $userData->blockedMe = $this->blockedMe($myUid, $uid);
        $userData->countAllFriends = (new UserFriendShip())->countAllFriends($uid);
        $userData->countVisitedSites = (new Sites())->countUserVisitedSites($uid, "", "", "", "");
        $userData->countFavoritesSites = (new Sites())->countMyFavoritesSites($uid, "", "", "", "");

        return $userData;
    }

    public function isFriend($myUid, $uid){
        $existFriendship = (new UserFriendShip())->getRelationFriendshipWithStatus($myUid, $uid, "FRIEND");
        return $existFriendship ? true : false;
    }

    public function sentFriendRequest($myUid, $uid){
        $existFriendRequest = (new UserFriendShip())->getRelationFriendshipStatusSentBy($myUid, $uid, "REQUEST");
        return $existFriendRequest ? true : false;
    }

    public function receivedFriendRequest($myUid, $uid){
        $existFriendRequest = (new UserFriendShip())->getRelationFriendshipStatusSentBy($uid, $myUid, "REQUEST");
        return $existFriendRequest ? true : false;
    }

    public function iBlockedHim($myUid, $uid){
        $existBlocked = (new UserFriendShip())->getRelationFriendshipStatusSentBy($myUid, $uid, "BLOCKED");
        return $existBlocked ? true : false;
    }

    public function blockedMe($myUid, $uid){
        $existBlocked = (new UserFriendShip())->getRelationFriendshipStatusSentBy($uid, $myUid, "BLOCKED");
        return $existBlocked ? true : false;
    }

    public function getRecommendedUsers($myUid, $limit, $offset, $whereSubcategories){
        $queryWhere = "userSubcategories.status='ACTIVE' AND users.uid!='" . $myUid . "'";
        if ($whereSubcategories!=="") {
            $queryWhere .= $whereSubcategories;
        }
        return $this->select("users.uid, COUNT(userSubcategories.userSubcategoryId) as countSubcategories")
        ->join("userSubcategories", "users.uid = userSubcategories.uid", "LEFT OUTER")
        ->where($queryWhere)->groupBy("users.uid")->orderBy("countSubcategories", "DESC")
        ->findAll($limit, $offset);
    }

    public function countRecommendedUsers($myUid, $whereSubcategories){
        $queryWhere = "userSubcategories.status='ACTIVE' AND users.uid!='" . $myUid . "'";
        if ($whereSubcategories!=="") {
            $queryWhere .= $whereSubcategories;
        }
        $allUsers = $this->select("users.uid, COUNT(userSubcategories.userSubcategoryId) as countSubcategories")
        ->join("userSubcategories", "users.uid = userSubcategories.uid", "LEFT OUTER")
        ->where($queryWhere)->groupBy("users.uid")->orderBy("countSubcategories", "DESC")
        ->findAll();
        return count($allUsers);
    }

    public function getRecentUsers($myUid, $limit, $offset){
        $queryWhere = "users.uid!='" . $myUid . "'";
        return $this->select("users.uid as uid")->where($queryWhere)
        ->orderBy("created_at", "DESC")->findAll($limit, $offset);
    }

    public function countUsersExceptMe($myUid){
        $queryWhere = "users.uid!='" . $myUid . "'";
        $allUsers = $this->where($queryWhere)->findAll();
        return count($allUsers);
    }

}