<?php

namespace App\Models;

use CodeIgniter\Model;

class CodeVerificationAccount extends Model
{
    // Data Base
    protected $table = 'codeVerificationAccount';
    protected $primaryKey = 'codeVerificationAccountId';

    protected $allowedFields = [
        'uid', 'code', 
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // Return Data
    protected $returnType = 'object';

    public $validationRules = [];

    public $validationMessages = [
        'code' => [
            'required' => 'El código es requerido.',
            'numeric' => 'El código debe contener números.',
        ]
    ];

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }
}

