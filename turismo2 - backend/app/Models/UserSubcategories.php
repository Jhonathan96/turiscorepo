<?php

namespace App\Models;

use CodeIgniter\Model;

class UserSubcategories extends Model
{
    // Data Base
    protected $table = 'userSubcategories';
    protected $primaryKey = 'userSubcategoryId';

    // Allowed Fields
    protected $allowedFields = [
        'userSubcategoryId', 'uid', 'subcategoryId', 'status'
    ];

    protected $useTimestamps = false;
    // protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';
    // protected $deletedField = 'deleted_at';
    // protected $statusFields = [
    //     'status', 'created_at', 'updated_at', 'deleted_at'
    // ];
    protected $statusFields = ['status'];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function createInactiveUserSubcategory($uid, $subcategoryId){
        $data = [
            "uid" => $uid,
            "subcategoryId" => $subcategoryId,
            "status" => "INACTIVE"
        ];
        $create = $this->insert($data);
        return $create;
    }

    public function updateUserSubcategoryStatusById($userSubcategoryId, $status){
        return $this->update(
            $userSubcategoryId,
            ["status" => $status]
		);
    }

    public function updateUserSubcategoryStatusByUidAndSubcategoryId($uid, $subcategoryId, $status){
        $row = $this->where([
            "subcategoryId" => $subcategoryId,
            "uid" => $uid
        ])->first();

        return $this->update($row->userSubcategoryId,
            ["status" => $status]
		);
    }

    public function getUserSubcategoryByUidAndSubcategory($uid, $subcategoryId){
        $userSubcategory = $this->where([
            "uid" => $uid,
            "subcategoryId" => $subcategoryId
        ])->find();

        return $userSubcategory;
    }

    public function getAllUserSubcategoryByUidAndSubcategory($uid){
        $userSubcategories = $this->where([
            "uid" => $uid
        ])->findAll();

        return $userSubcategories;
    }

    public function getUserSubcategoryByUidAndCategoryId($uid, $categoryId){
        $userSubcategory = $this->select("
        subcategories.subcategoryId,
        subcategories.subcategoryName,
        userSubcategories.userSubcategoryId,
        userSubcategories.status,
        categories.categoryColor as subColor
        ")
        ->where([
            "userSubcategories.uid" => $uid,
            "subcategories.status" =>"ACTIVE",
            "categories.status" =>"ACTIVE",
            "categories.categoryId" => $categoryId
        ])
        ->join("subcategories","subcategories.subcategoryId=userSubcategories.subcategoryId","LEFT OUTER")
        ->join("categories","subcategories.categoryId=categories.categoryId","LEFT OUTER")
        ->findAll();

        return $userSubcategory;
    }

    public function verifyUserSubcategoryWithUid($uid, $userSubcategoryId){
        $row = $this->where([
            "userSubcategoryId" => $userSubcategoryId,
            "uid" => $uid
        ])->find();
        return $row ? true : false;
    }

    public function verifyUserSubcategoryWithUid2($uid, $subcategoryId){
        $row = $this->where([
            "subcategoryId" => $subcategoryId,
            "uid" => $uid
        ])->find();
        return $row ? true : false;
    }

    public function getUserSubcategoriesIdsActive($uid){
        return $this->select("subcategories.subcategoryId")
        ->where([
            "userSubcategories.uid" => $uid,
            "userSubcategories.status" => "ACTIVE",
            "subcategories.status" => "ACTIVE"
        ])
        ->join("subcategories","subcategories.subcategoryId=userSubcategories.subcategoryId","LEFT OUTER")
        ->findAll();
    }
}