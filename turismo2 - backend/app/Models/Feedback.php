<?php

namespace App\Models;

use CodeIgniter\Model;

class Feedback extends Model
{
    // Data Base
    protected $table = 'feedback';
    protected $primaryKey = 'feedbackId';

    // Allowed Fields
    protected $allowedFields = ['feedbackId', 'rating', 'comment', 'uid', 'date'];

    protected $statusFields = [];

    // Return Data
    protected $returnType = 'object';
 
    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function getFeedbackByUser($uid){
        return $this->where([
            "uid" => $uid
        ])->find();
    }

    public function createFeedback($rating, $comment, $uid){
        return $this->insert([
            "rating" => $rating,
            "comment" => $comment,
            "uid" => $uid
        ]);
    }

    public function updateFeedback($feedbackId, $rating, $comment, $uid){
        return $this->update(
            $feedbackId,
            [
            "rating" => $rating,
            "comment" => $comment,
            "uid" => $uid
            ]
        );
    }
}