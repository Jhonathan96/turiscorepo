<?php

namespace App\Models;

use CodeIgniter\Model;

class Cities extends Model
{
    // Data Base
    protected $table = 'cities';
    protected $primaryKey = 'cityId';

    // Allowed Fields
    protected $allowedFields = [
        'cityName', 'status'
    ];

    protected $useTimestamps = false;
    // protected $createdField = 'created_at';
    // protected $updatedField = 'updated_at';
    // protected $deletedField = 'deleted_at';
    // protected $statusFields = [
    //     'status', 'created_at', 'updated_at', 'deleted_at'
    // ];
    protected $statusFields = ['status'];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    // Foreign Keys
    // public function state()
    // {
    //     $response = $this->join("state", "state.stateId = city.stateId");
    //     return $response;
    // }

    public function existCity($cityId){
        $cityData = $this->find($cityId);
        return ($cityData) ? true : false;
    }
}