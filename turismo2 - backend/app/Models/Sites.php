<?php

namespace App\Models;

use CodeIgniter\Model;

class Sites extends Model
{
    // Data Base
    protected $table = 'sites';
    protected $primaryKey = 'siteId';

    // Allowed Fields
    protected $allowedFields = [
        'siteId', 'siteName', 'locality', 'owner', 'cityDistance', 'siteDescription', 
        'latitude', 'longitude', 'subcategoryId', 'cityId', 'image', 'status', 'created_at'
    ];

    protected $useTimestamps = false;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // protected $statusFields = ['status'];
    protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 
    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    public function getSiteById($siteId){
        return $this->find($siteId);
    }

    public function getOtherSitesWithSubcategories($whereOtherSites, $whereSubcategories, $cityId, $usrLatitude, $usrLongitude){
        $whereActiveItems = " AND sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        ($whereOtherSites!="") 
        ? $queryWhere = $whereOtherSites." AND ".$whereSubcategories . $whereActiveItems 
        : $queryWhere = $whereSubcategories . $whereActiveItems;

        if ($cityId) {
            $queryWhere .= " AND sites.cityId=". $cityId;
        }
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = "(6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }
        $otherSites = $this->select("sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, 
        AVG(ratings.rating) qualification, ". $selectLocation)
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")->where($queryWhere)
        ->groupBy("sites.siteId")->orderBy("qualification", "DESC")->findAll();

        return $otherSites;
    }

    public function getOtherSitesWithSubcategoriesAndLocation($whereOtherSites, $whereSubcategories, $maxKmsDistance, $usrLatitude, $usrLongitude){
        $whereActiveItems = " AND sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        ($whereOtherSites!="") 
        ? $queryWhere = $whereOtherSites." AND ".$whereSubcategories . $whereActiveItems 
        : $queryWhere = $whereSubcategories . $whereActiveItems;
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = "(6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }
        $otherSites = $this->select("sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, " 
        . $selectLocation . ", AVG(ratings.rating) qualification")
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)->having("distance <",  $maxKmsDistance)
        ->groupBy("sites.siteId")->orderBy("qualification", "DESC")->findAll();

        return $otherSites;
    }

    public function getSiteAllDataById($siteId, $usrLatitude, $usrLongitude, $qualification=null){
        $queryWhere = "sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        $selectQuery = "sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus";
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = ", (6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }

        if ($qualification==null) {
            $sites = $this->select($selectQuery.", AVG(ratings.rating) qualification" . $selectLocation)
            ->where($queryWhere."AND sites.siteId=".$siteId)->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
            ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
            ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
            ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
            ->groupBy("sites.siteId")->orderBy("qualification", "DESC")->findAll();
            foreach ($sites as $site) {
                // if ($usrLatitude === "" || $usrLongitude === "") {$site->distance = null;}else{$site->distance = round($site->distance, 1);}
                ($usrLatitude === "" || $usrLongitude === "") ? $site->distance = null : $site->distance = round($site->distance, 1);
                // $site->qualification = round($site->qualification, 2);
                $site->qualification == 0 ? $site->qualification = 0 : $site->qualification = round($site->qualification,1);
                return $site;
            }
        }

        $site = $this->select($selectQuery . $selectLocation)
        ->where($queryWhere)->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->find($siteId);
        
        $site->qualification = $qualification;
        // if ($usrLatitude === "" || $usrLongitude === "") {$site->distance = null;}else{$site->distance = round($site->distance, 1);}
        ($usrLatitude === "" || $usrLongitude === "") ? $site->distance = null : $site->distance = round($site->distance, 1);

        return $site;
    }

    public function getUserVisitedSites($uid, $limit, $offset, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude){
        $whereActiveItems = "sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        $whereUser = " AND ratings.uid = '" . $uid . "'";
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = ", (6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }

        $querySelect = "sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, 
        ratings.rating as myQualification";
        $queryWhere = $whereActiveItems . $whereUser;
        if($cityId){
            $queryWhere .= " AND sites.cityId=" . $cityId;
        }

        $this->select($querySelect . $selectLocation)
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)->groupBy("sites.siteId")->orderBy("myQualification", "DESC");
        if($usrLatitude !== "" && $usrLongitude !== "" && $maxKmsDistance !== ""){
            $this->having("distance <",  $maxKmsDistance);
        }
        $userVisitedSites = $this->findAll($limit, $offset);

        foreach ($userVisitedSites as $userVisitedSite) {
            // if ($usrLatitude === "" || $usrLongitude === "") {$userVisitedSite->distance = null;}
            ($usrLatitude === "" || $usrLongitude === "") ? $userVisitedSite->distance = null : $userVisitedSite->distance = round($userVisitedSite->distance, 1);
            $userVisitedSite->qualification = $this->getSiteAvgQualification($userVisitedSite->siteId);
            $userVisitedSite->myQualification = round($userVisitedSite->myQualification,1);
            $userVisitedSite->isMyFavorite = $this->getIsMyFavoriteSite($uid, $userVisitedSite->siteId);
        }

        return $userVisitedSites;
    }

    public function countUserVisitedSites($uid, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude){
        $whereActiveItems = "sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        $whereUser = " AND ratings.uid = '" . $uid . "'";
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = ", (6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }

        $querySelect = "sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, 
        ratings.rating as myQualification";
        $queryWhere = $whereActiveItems . $whereUser;
        if($cityId){
            $queryWhere .= " AND sites.cityId=" . $cityId;
        }

        $this->select($querySelect . $selectLocation)
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)->groupBy("sites.siteId");
        if($usrLatitude !== "" && $usrLongitude !== "" && $maxKmsDistance !== ""){
            $this->having("distance <",  $maxKmsDistance);
        }
        $userVisitedSites = $this->findAll();

        return count($userVisitedSites);
    }

    public function getRecentsSites($uid, $limit, $offset, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude){
        $whereActiveItems = "sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = ", (6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }

        $querySelect = "sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, 
        AVG(ratings.rating) qualification";
        $queryWhere = $whereActiveItems;
        if($cityId){
            $queryWhere .= " AND sites.cityId=" . $cityId;
        }

        $this->select($querySelect . $selectLocation)
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)
        ->groupBy("sites.siteId")
        ->orderBy("siteCreatedAt", "DESC");
        if($usrLatitude !== "" && $usrLongitude !== "" && $maxKmsDistance !== ""){
            $this->having("distance <",  $maxKmsDistance);
        }
        $recentSites = $this->findAll($limit, $offset);

        foreach ($recentSites as $recentSite) {
            // if ($usrLatitude === "" || $usrLongitude === "") {$recentSite->distance = null;}
            ($usrLatitude === "" || $usrLongitude === "") ? $recentSite->distance = null : $recentSite->distance = round($recentSite->distance,1);
            // $recentSite->qualification = round($recentSite->qualification, 2);
            $recentSite->qualification == 0 ? $recentSite->qualification = 0 : $recentSite->qualification = round($recentSite->qualification,1);
            $recentSite->myQualification = $this->getSiteMyQualification($uid, $recentSite->siteId);
            $recentSite->isMyFavorite = $this->getIsMyFavoriteSite($uid, $recentSite->siteId);
        }

        return $recentSites;
    }

    public function countRecentSites($cityId, $maxKmsDistance, $usrLatitude, $usrLongitude){
        $whereActiveItems = "sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = ", (6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }

        $querySelect = "sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, 
        AVG(ratings.rating) qualification";
        $queryWhere = $whereActiveItems;
        if($cityId){
            $queryWhere .= " AND sites.cityId=" . $cityId;
        }

        $this->select($querySelect . $selectLocation)
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)
        ->groupBy("sites.siteId");
        if($usrLatitude !== "" && $usrLongitude !== "" && $maxKmsDistance !== ""){
            $this->having("distance <",  $maxKmsDistance);
        }
        $recentSites = $this->findAll();

        return count($recentSites);
    }

    public function getMyFavoritesSites($uid, $limit, $offset, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude){
        $whereActiveItems = "sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = ", (6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }

        $querySelect = "sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, 
        AVG(ratings.rating) qualification";
        $queryWhere = $whereActiveItems . " AND favoriteSites.uid='" . $uid . "'";
        if($cityId){
            $queryWhere .= " AND sites.cityId=" . $cityId;
        }

        $this->select($querySelect . $selectLocation)
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->join("favoriteSites", "favoriteSites.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)
        ->groupBy("sites.siteId")
        ->orderBy("favoriteSites.created_at", "DESC");
        if($usrLatitude !== "" && $usrLongitude !== "" && $maxKmsDistance !== ""){
            $this->having("distance <",  $maxKmsDistance);
        }
        $myFavoritesSites = $this->findAll($limit, $offset);

        foreach ($myFavoritesSites as $myFavoriteSite) {
            // if ($usrLatitude === "" || $usrLongitude === "") {$myFavoriteSite->distance = null;}
            ($usrLatitude === "" || $usrLongitude === "") ? $myFavoriteSite->distance = null : $myFavoriteSite->distance = round($myFavoriteSite->distance, 1);
            // $myFavoriteSite->qualification = round($myFavoriteSite->qualification, 2);
            $myFavoriteSite->qualification == 0 ? $myFavoriteSite->qualification = 0 : $myFavoriteSite->qualification = round($myFavoriteSite->qualification,1);
            $myFavoriteSite->myQualification = $this->getSiteMyQualification($uid, $myFavoriteSite->siteId);
            $myFavoriteSite->isMyFavorite = $this->getIsMyFavoriteSite($uid, $myFavoriteSite->siteId);
        }

        return $myFavoritesSites;
    }

    public function countMyFavoritesSites($uid, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude){
        $whereActiveItems = "sites.status='Active' AND subcategories.status='Active' 
        AND categories.status='Active' AND cities.status='Active'";
        $selectLocation = "";
        if ($usrLatitude !== "" && $usrLongitude !== "") {
            $selectLocation = ", (6371*ACOS(SIN(RADIANS(sites.latitude))*SIN(RADIANS(" . $usrLatitude . "))+COS(RADIANS(sites.longitude-" . $usrLongitude . "))*COS(RADIANS(sites.latitude))*COS(RADIANS(" . $usrLatitude . ")))) AS distance";
        }

        $querySelect = "sites.siteId as siteId, sites.siteName as siteName, 
        sites.locality as locality,  sites.owner as owner, sites.cityDistance as cityDistance, 
        sites.siteDescription as siteDescription, sites.latitude as latitude, sites.longitude as longitude, 
        sites.image as image, sites.status as siteStatus, sites.created_at as siteCreatedAt, 
        subcategories.subcategoryId as subcategoryId, subcategories.subcategoryName as subcategoryName, 
        subcategories.status as subcategoryStatus, categories.categoryId as categoryId,
        categories.categoryName as categoryName, categories.status as categoryStatus,
        cities.cityId as cityId, cities.cityName as cityName, cities.status as cityStatus, 
        AVG(ratings.rating) qualification";
        $queryWhere = $whereActiveItems . " AND favoriteSites.uid='" . $uid . "'";
        if($cityId){
            $queryWhere .= " AND sites.cityId=" . $cityId;
        }

        $this->select($querySelect . $selectLocation)
        ->join("cities", "cities.cityId=sites.cityId", "LEFT OUTER")
        ->join("subcategories", "subcategories.subcategoryId=sites.subcategoryId", "LEFT OUTER")
        ->join("categories", "categories.categoryId=subcategories.categoryId", "LEFT OUTER")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->join("favoriteSites", "favoriteSites.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)
        ->groupBy("sites.siteId");
        if($usrLatitude !== "" && $usrLongitude !== "" && $maxKmsDistance !== ""){
            $this->having("distance <",  $maxKmsDistance);
        }
        $myFavoritesSites = $this->findAll();

        return count($myFavoritesSites);
    }

    public function getSiteAvgQualification($siteId){
        $siteData = $this->select("AVG(ratings.rating) avgQualification")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->find($siteId);
        // return round($siteData->avgQualification, 1);
        return $siteData->avgQualification == 0 ? 0 : round($siteData->avgQualification, 1);
    }

    public function getSiteMyQualification($uid, $siteId){
        $siteData = $this->select("ratings.rating as myQualification")
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->where([
            "ratings.uid" => $uid,
            "ratings.siteId" => $siteId
        ])->first();

        return ($siteData) ? round($siteData->myQualification, 1) : null;
        // return ($siteData) ? round($siteData["myQualification"], 2) : 0;
    }

    public function getIsMyFavoriteSite($uid, $siteId){
        $userFavoriteSite = $this
        ->join("favoriteSites", "favoriteSites.siteId =sites.siteId", "LEFT OUTER")
        ->where([
            "favoriteSites.uid" => $uid, 
            "favoriteSites.siteId" => $siteId
        ])->first();
        return $userFavoriteSite ? true : false;
    }

    public function getUserVisitedSitesOnlySitesId($uid){
        $querySelect = "ratings.siteId as siteId";
        $queryWhere = "ratings.uid = '" . $uid . "'";

        $userVisitedSites = $this->select($querySelect)
        ->join("ratings", "ratings.siteId=sites.siteId", "LEFT OUTER")
        ->where($queryWhere)->groupBy("sites.siteId")->findAll();

        return $userVisitedSites;
    }
}