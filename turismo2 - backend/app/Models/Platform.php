<?php

namespace App\Models;

use CodeIgniter\Model;

class Platform extends Model
{
    // Data Base
    protected $table = 'platform';
    protected $primaryKey = 'platformId';

    // Allowed Fields
    protected $allowedFields = [
        'name', 'shortname',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';

    // Validation Rules
    protected $skipValidation     = false;
    public $validationRules = [
        'name' => 'required|regex_match[/(\p{L})(.+)/iu]',
        'shortname' => 'required|regex_match[/(\p{L})(.+)/iu]'
    ];

    public $validationMessages = [
        'name' => [
            'regex_match' => 'El nombre tiene caracteres no permitidos'
        ],
        'shortname' => [
            'regex_match' => 'El nombre corto tiene caracteres no permitidos'
        ],
    ];

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }
}