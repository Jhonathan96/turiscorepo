<?php

namespace App\Models;

use CodeIgniter\Model;

class Country extends Model
{
    // Data Base
    protected $table = 'country';
    protected $primaryKey = 'countryId';

    // Allowed Fields
    protected $allowedFields = [
        'name', 'alpha2Code', 'alpha3Code', 'callingCode', 'capital', 'region', 'subregion', 'longitude', 'latitude', 'flag', 'callingCodeId',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $statusFields = [
        'status', 'created_at', 'updated_at', 'deleted_at'
    ];
    // protected $useSoftDeletes = true;

    // Return Data
    protected $returnType = 'object';
 

    public function __construct() {
        parent::__construct();
        array_unshift($this->allowedFields , $this->primaryKey);
        $this->allowedFields = array_merge($this->allowedFields, $this->statusFields);
    }

    // Foreign Keys
    public function state()
    {
        $response = $this->join("state", "state.stateId = country.stateId");
        return $response;
    }
}