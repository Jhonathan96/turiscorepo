<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Kubo - <?php echo NAME_PROJECT ?></title>
	<style>
		body {
			width: -webkit-fill-available;
			height: -webkit-fill-available;
		}
		.parent {
			display: flex;
    		justify-content: center;
			align-items: center;
			width: -webkit-fill-available;
			height: -webkit-fill-available;
		}
	</style>
</head>

<body>
	<div class="parent">
		<h1 class="child"><?php echo NAME_PROJECT ?></h1>
	</div>
</body>

</html>