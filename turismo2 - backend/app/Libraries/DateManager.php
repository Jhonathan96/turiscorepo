<?php
namespace App\Libraries;

class DateManager
{
    private $date;
    private $mappingMonth = [
        'Ene',
        'Feb',
        'Mar',
        'Abr',
        'May',
        'Jun',
        'Jul',
        'Ago',
        'Sep',
        'Oct',
        'Nov',
        'Dic'
    ];

    public function __construct($date = "")
    {
        $this->date = empty($date) ? date('Y-m-d H:i:s') : $date;
    }

    public function convertToTripodeFormat()
    {
        $month = $this->getMonthShortName((int)  date('m', strtotime($this->date)));
        $day = date('d', strtotime($this->date));
        $year = date('Y', strtotime($this->date));
        return $month . '/' . $day . '/' . $year;
    }

    public function convertToTripodeFormatWithHour()
    {
        $month = $this->getMonthShortName((int)  date('m', strtotime($this->date)));
        $day = date('d', strtotime($this->date));
        $year = date('Y', strtotime($this->date));
        $hour = date('g:i A', strtotime($this->date));

        return $month . '/' . $day . '/' . $year . " ". $hour;
    }

    private function getMonthShortName(int $month): string
    {
        $month--;
        return isset($this->mappingMonth[$month]) ? $this->mappingMonth[$month] : 'Indefinido';
    }

    public function getDiffBetweenDays($eventDate, $initialDate = "")
    {
        // $initialDate = empty($initialDate) ? strtotime(date('Y-m-d H:i:s')) : strtotime($initialDate);
        $initialDate = empty($initialDate) ? strtotime(date('Y-m-d G:i:s')) : strtotime($initialDate);
        // $eventDate = strtotime($eventDate. ' +1 day');
        $eventDate = strtotime($eventDate);
        if (date('Y-m-d G:i:s', $initialDate) >= date('Y-m-d G:i:s', $eventDate)) 
            return [
                'years'     => 0,
                'months'    => 0,
                'days'      => 0,
                'hours'     => 0,
                'minutes'   => 0,
                'seconds'   => 0
            ];

        $diff = abs($eventDate - $initialDate);  
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
        $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
        $hours = floor(($diff - $years * 365*60*60*24  - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
        $minutes = floor(($diff - $years * 365*60*60*24  - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
        $seconds = floor(($diff - $years * 365*60*60*24  - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));  
        return [
            'years'     => $years,
            'months'    => $months,
            'days'      => $days,
            'hours'     => $hours,
            'minutes'   => $minutes,
            'seconds'   => $seconds
        ];
    }


}
