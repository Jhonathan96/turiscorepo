<?php
namespace App\Libraries;

class SlugCreator
{
    private $slug;

    public function __construct($title)
    {
        $slug = str_replace(' ', '-', strtolower($title));
    }

    public function getSlug() :string
    {
        return $this->slug; 
    }

}
