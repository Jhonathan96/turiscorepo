<?php header('Content-Type: text/html; charset=utf-8'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title><?php echo NAME_PROJECT; ?></title>
    <style>
       @media screen{@font-face{font-family:Poppins;font-style:normal;font-weight:400;src:local("Poppins Regular"),local("Poppins-Regular"),url(https://fonts.gstatic.com/s/poppins/v9/pxiEyp8kv8JHgFVrJJfecnFHGPc.woff2) format("woff2")}*{align-items:center;margin:auto;text-align:center;font-family:Poppins,sans-serif}body{font-family:Poppins,sans-serif}.body{width:100%; font-size: 100%;}.header{background:-webkit-gradient(linear,left top,left bottom,from(#f63a49),to(#e5406c))!important;background:linear-gradient(180deg,#f63a49 0,#e5406c)!important;margin:auto;height:350px;border-radius:10px;color:#fff}.container{font-family:Poppins,sans-serif;color:#4a4a4b}.my-auto{margin:50px auto}.py-auto{padding-top:90px;padding-bottom:90px}h1{margin:10px auto}.footer{border-radius:50px;background:#000;color:#fff}.button{background-color:#009fff;-webkit-border-radius:14px;-moz-border-radius:14px;border-radius:14px;border:1px solid white;display:inline-block;cursor:pointer;color:#fff!important;font-family:Arial;font-size:28px;font-weight:700;padding:10px 76px;margin:30px auto;text-decoration:none;text-shadow:0 1px 0 #3acce1}} a { font-size: xx-small; }
    </style>
</head>

<body>
    <div class="body">
        <div class="header">
            <div class="py-auto">
                <img src="http://138.197.73.193/images/logo.png" alt="Kubo" srcset="kubo">
                <h1><?php echo utf8_decode($title ?? NAME_PROJECT); ?></h1>
                <h2><?php echo utf8_decode($subtitle ?? NAME_PROJECT); ?></h2>
            </div>
        </div>
        <div class="container">
            <div class="my-auto">
                <?php echo utf8_decode($description ?? NAME_PROJECT); ?>
                <br>
                <?php echo utf8_decode($content ?? NAME_PROJECT); ?>
            </div>
        </div>
        <div class="footer">
            <small><?php echo utf8_decode($copyright ?? NAME_PROJECT); ?></small>
        </div>
    </div>
</body>

</html>