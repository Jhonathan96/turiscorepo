#!/usr/bin/env node
// Load Libraries
var express = require("express");
var app = express();
var http = require("http").createServer(app);
var socketio = require("socket.io")(http);
var fs = require("fs");
const mysql = require("mysql");
var moment = require("moment-timezone");

// Const
// Host - Socket
const port = 4343;
const hostIP = "3.91.12.199"; // 3.91.12.199 <- Socket Url: dev.storent.co
// Database AWS
const RDS_HOSTNAME = "storent-dev.cwb5ajusgrsr.us-east-1.rds.amazonaws.com";
const RDS_USERNAME = "admin";
const RDS_PASSWORD = "storent2019*";
const RDS_PORT = 3306;
const RDS_DATABASE = "test";

// Debug "CONST"
let debug = null; // True: localhost, false = server, null = debug server
// Variables
let host = debug ? "127.0.0.1" : hostIP;
let routeFile = (debug ? "./public" : __dirname) + "/index.html";
// SuperDebugg
if (debug == null) {
  host = hostIP;
  routeFile = __dirname + "/index.html";
  debug = true;
}
// Set Const
// Host - Socket
app.set("port", process.env.PORT || port);
app.set("host", process.env.HOST || host);

// Database AWS
app.set("RDS_HOSTNAME", process.env.RDS_HOSTNAME || RDS_HOSTNAME);
app.set("RDS_USERNAME", process.env.RDS_USERNAME || RDS_USERNAME);
app.set("RDS_PASSWORD", process.env.RDS_PASSWORD || RDS_PASSWORD);
app.set("RDS_PORT", process.env.RDS_PORT || RDS_PORT);
app.set("RDS_DATABASE", process.env.RDS_DATABASE || RDS_DATABASE);

/**
 * Logic Connection - Express / Web
 */
// Init Connection (Get URL + PORT)
http.listen(app.get("port"), function() {
  console.log(
    "Express server listening on => " + app.get("host") + ":" + app.get("port")
  );
});

// Start and load Page with Socket (NODE STARTED)
app.get("/", function(req, res) {
  // Web Process (Read File and show content) __dirname + routeFile
  fs.readFile(routeFile, function(err, data) {
    if (err) {
      res.writeHead(500);
      return res.end("Error loading " + routeFile);
    }
    res.writeHead(200);
    res.end(data);
  });
});

/**
 *  DataBase
 */
const connection = mysql.createConnection({
  host: RDS_HOSTNAME,
  user: RDS_USERNAME,
  password: RDS_PASSWORD,
  port: RDS_PORT,
  database: RDS_DATABASE
});

connection.connect(function(err) {
  // in case of error
  if (err) {
    console.log(err.code);
    console.log(err.fatal);
    return;
  }
  console.log("Connected to database.");
});

/**
 * Logic Socket
 */
// Flags
var numUsers = 0;
// Config
const socket = socketio.sockets;
socket.setMaxListeners(0);
// Init
socket.on("connection", userSocket => {
  // Config Socket (User Id - Socket)
  let uid = null;
  let channel = null;
  var addedUser = false;
  let socketId = (uid = userSocket.id);
  if (debug) console.log("User " + socketId + " connected");

  // once a client has connected, we expect to get a ping from them saying what room they want to join
  userSocket.on("init", dataUser => {
    if (debug) console.log(dataUser);
    // Flag: only 1 user per session - socket
    if (addedUser) return;
    // Set Data
    validateDate(dataUser)
      .then(function(results) {
        if (debug) console.log(results);
        results = results[0];
        var tempData;
        getUsersChannel(results).then(function(res) {
          tempData = res[0];
          if (debug) console.log(tempData);
          // expected output: "foo"
          if (results.uid) {
            // Room id
            channel = results.channel;
            // uid
            uid = results.uid;
          }

          // Show messages
          const uidTag =
            results.uid === tempData.guest
              ? "guest"
              : results.uid === tempData.host
              ? "host"
              : "NAN";
          if (debug) console.log(tempData[uidTag]);

          getMessageByUnUID(tempData[uidTag]).then(function(resultArray) {
            resultArray.forEach(function(value) {
              if (debug) console.log(value);
              const dataMessage = {
                socketId,
                uid: value.uid,
                message: value.message,
                date: value.date
              };
              socket.in(channel).emit("newMessage", dataMessage);
              //
              removeMessageByUnUID(value.uid).then(function(resultDelete) {
                if (debug) console.log(resultDelete);
              });
            });
          });

          // we store the username in the socket session for this client
          ++numUsers;
          addedUser = true;

          // Join to the Channel
          joinChannel();
        });
      })
      .catch(function(err) {
        sendMessage(err);
        console.log(err);
      });
  });

  // when the client emits 'new message', this listens and executes
  userSocket.on("newMessage", message => {
    // we tell the client to execute 'new message'
    const nDate = new Date().toLocaleString('en-US', {
      timeZone: 'America/Bogota'
    });
    const date = moment().tz('America/Bogota').format("YYYY-MM-DD HH:m:s");
    socket.in(channel).emit("newMessage", {
      socketId,
      uid,
      message,
      date
    });
    if (debug) console.log(numUsers);
    if (numUsers < 2 && channel) {
      const sql =
        "INSERT INTO `chat` (`uid`, `reservationId`, `message`, `view`, `date`) VALUES ('" +
        uid +
        "', '" +
        channel +
        "', '" +
        message +
        "', '0', '" +
        date +
        "');";
        console.log(sql);
      connection.query(sql, (err, res) => {
        if (err) {
          throw err;
        }
        if (debug) console.log("1 chat inserted.");
      });
    }
  });

  //  // when the client emits 'typing', we broadcast it to others
  //  userSocket.on('typing', () => {
  //   socket.broadcast.emit('typing', {
  //     username: socket.username
  //   });
  // });

  // // when the client emits 'stop typing', we broadcast it to others
  // userSocket.on('stop typing', () => {
  //   socket.broadcast.emit('stop typing', {
  //     username: socket.username
  //   });
  // });

  // Exit the Channel (Exit the Room - Channel)
  userSocket.on("leaveChannel", () => {
    // Flag: only 1 user per session - socket
    if (!addedUser) return;
    // Leave Room
    leaveChannel();
    userLeft();
    resetVariables();
  });

  // Disconnect User
  userSocket.on("disconnect", reason => {
    userLeft();
  });

  /**
   * Functions
   **/
  function joinChannel() {
    // Leave existent ROOM
    if (userSocket.room) userSocket.leave(userSocket.room);
    // Set New Room
    userSocket.room = channel;
    // Join to the Room
    try {
      userSocket.join(channel);
      sendMessage("User " + uid + " join to room " + channel);
    } catch (e) {
      sendMessage("Error joinRoom service" + e);
    }
  }

  function leaveChannel() {
    let message = "";
    // Leave existent ROOM
    if (!userSocket.room) {
      message = "Not exist Channel";
    }
    // Join to the Room
    try {
      userSocket.leave(userSocket.room);
      message = "User " + uid + " leave the " + channel;
    } catch (e) {
      message = "Error leave service" + e;
    }
    sendMessage(message);
  }

  function userLeft() {
    if (addedUser) {
      addedUser = false;
      --numUsers;
      // TODO: Check
      // userSocket.leave(socketId);
      // echo globally that this client has left
      sendMessage("User " + uid + " disconnected");
      resetVariables();
    }
  }

  function resetVariables() {
    // Flags
    uid = null;
    channel = null;
    addedUser = false;
    socketId = userSocket.id;
  }

  function sendMessage(message) {
    if (debug) console.log(message);
    socket.in(channel).emit("message", message);
  }

  /**
   * SQLs - AWS
   */
  function validateDate(dataUser) {
    return new Promise(function(resolve, reject) {
      const sql =
        "SELECT user.uid, reservation.reservationId as channel FROM `user`, `reservation` WHERE user.`uid` = '" +
        dataUser.uid +
        "' and reservation.`reservationId` = '" +
        dataUser.channel +
        "'";
      connection.query(sql, function(err, result) {
        if (result.length > 0)
          resolve(Object.values(JSON.parse(JSON.stringify(result))));
        reject(
          new Error(
            "El usuario no existe o la reserva no coincide con el usuario en la base de datos."
          )
        );
      });
    });
  }
  function getUsersChannel(dataUser) {
    return new Promise(function(resolve, reject) {
      const sql =
        "select reservation.uid as guest, company.uid as host from reservation  INNER JOIN `warehouseProduct` ON warehouseProduct.warehouseProductId = reservation.warehouseProductId INNER JOIN `warehouse` ON warehouseProduct.warehouseId = warehouse.warehouseId INNER JOIN `company` ON company.companyId = warehouse.companyId where reservation.reservationId = '" +
        dataUser.channel +
        "'";
      connection.query(sql, function(err, result) {
        if (result.length > 0)
          resolve(Object.values(JSON.parse(JSON.stringify(result))));
        reject(
          new Error(
            "El usuario no existe o la reserva no coincide con el usuario en la base de datos."
          )
        );
      });
    });
  }
  function getMessageByUnUID(uid) {
    return new Promise(function(resolve, reject) {
      const sql =
        "select chatId, uid, reservationId as channel, message, date from chat where uid != '" +
        uid +
        "' order by date ASC";
      connection.query(sql, function(err, result) {
        if (result.length > 0)
          resolve(Object.values(JSON.parse(JSON.stringify(result))));
      });
    });
  }
  function removeMessageByUnUID(uid) {
    return new Promise(function(resolve, reject) {
      const sql = "DELETE FROM `chat` WHERE uid = '" + uid + "'";
      if (debug) console.log(sql);

      connection.query(sql, function(err, result) {
        if (debug) console.log(result);
      });
    });
  }
});

// process.exit(1);
// connection.end();
