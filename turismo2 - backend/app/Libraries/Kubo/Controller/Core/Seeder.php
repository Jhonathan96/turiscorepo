<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\Core;

use Exception;
// Add Library
require_once('Kubo.php');

/**
 * Seeders
 * @package Kubo
 */
class Seeder extends \Kubo\Base
{   

    public function __construct($context, $db, $tables) {
        $this->context = $context;
        self::cleanAndSet($db, $tables);
    }
    
    protected function cleanAndSet($db, $tables)
    {
        try {
            $db->query('SET FOREIGN_KEY_CHECKS=0;');
            // Remove Data
            foreach ($tables as $key => $value) {
                dump("TRUNCATE TABLE " . lcfirst($value));
                if (isset($value))
                    $db->query("TRUNCATE TABLE `" . lcfirst($value) . '`');
            }
            // Add Data
            foreach ($tables as $key => $value) {
                $this->context->call($value . 'Seeder');
            }
        } catch (Exception $ex) {
            exit((['message' => $ex->getMessage()]));
        }
    }
}
