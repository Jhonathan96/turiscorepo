<?php

if (!function_exists('ddd')) {
    /**
     * Display Dump and Die
     * Prints a debug report and exits.
     * @param array ...$vars
     */
    function ddd(...$vars)
    {
        if (isset($vars) && !empty($vars)) {
            dump($vars);
        }
        die();
        //Just exit with 200 OK with the above headers for OPTIONS method
        exit(0);
    }
}

if (!function_exists('checkIsEmpty')) {
    /**
     * Check if any item is empty
     * @param mixed $value 
     * @param mixed $checkNull 
     * @return bool 
     */
    function checkIsEmpty($value)
    {
        // Null
        $checkNull = is_null($value);
        if ($checkNull) {
            return $checkNull;
        }
        // Array
        if (is_array($value)) {
            return (!isset($value) || empty($value) || count($value) <= 0) || $value === null;
        }
        // String
        if (is_string($value)) {
            return (!isset($value) || trim($value) === '');
        }
        // Object and another
        return (!isset($value)) || empty($value);
    }
}
