<?php
namespace Kubo\Core;
class Routes
{
    private $baseUrls = [
        'Auth',  'Platform', 'Version',
        'Location'
    ];

    public function __construct($routes)
    {
        self::init($routes);
    }

    protected function init($routes)
    {

        if (file_exists(APPPATH . 'Libraries/Kubo/Controller/Core/Kubo.php')) {
            require_once APPPATH . 'Libraries/Kubo/Controller/Core/Kubo.php';
            \Kubo\Core\Kubo::setUrls($routes);
        }

        for ($i = DEPLOY_VERSION; $i > 0; $i--) {
            foreach ($this->baseUrls as $key => $value) {
                if (file_exists(APPPATH . "Libraries/Kubo/Controller/v{$i}/{$value}.php")) {
                    require_once APPPATH . "Libraries/Kubo/Controller/v{$i}/{$value}.php";
                    $class = "\Kubo\\v$i\\$value";
                    $class::setUrls($routes);
                }
            }
        }
    }
}
