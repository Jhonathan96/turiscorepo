<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\Core;

// Add Library
require_once('Base.php');
# Libraries
use App\Models\UserToken;
use Exception;

/**
 * Kubo - Tokenization
 * @package App\Libraries
 */

class Kubo extends \Kubo\Base
{
    // Base Variables
    protected $uri;
    private $jwt;
    private $timeToken = TIME_ACCESS_TOKEN; // 24 Hours
    private $timeExp   = TIME_AUTH_TOKEN;   // 4 Hours
    // Exceptions URL (Access or Auth)
    private $specificElement = false;
    // If exist in array has ACCESS if NOT exist in array is AUTH
    private $exceptions = URLS_EXCEPTION;


    public function __construct()
    {
        parent::init();
        // Init Actions
        self::init();
    }

    // Set URLs Base
    public static function setUrls($routes)
    {
        // Base elements
        $routes->group('v' . DEPLOY_VERSION, ['namespace' => 'Kubo\Core'], function ($routes) {

            // Register App
            $routes->group(strtolower(SHORT_NAME_PROJECT), function ($routes) {
                // Get Token
                $routes->get('generate-access-token', 'Kubo::generateAccessToken');
                $routes->get('refresh-access-token', 'Kubo::generateAccessToken');
            });
        });
    }

    protected function init()
    {
        self::initVariables();
        self::checkToken();
    }

    protected function initVariables()
    {
        // JWT
        $this->jwt          = new Jwt();

        // Time
        $this->timeToken = date('Y-m-d H:i:s', strtotime('+' . $this->timeToken . ' hours'));
        $this->timeExp   = time() + ($this->timeExp * 60 * 60); // 4 hours; 60 mins; 60 secs

        // Init Variables
        $this->uri = current_url(true);

        // Folder Files
        if (WRITEPATH !== null) {
            $this->pathFolderFiles = WRITEPATH;
        }
    }

    /**
     * Generate Access Token
     * @return mixed
     */
    public function generateAccessToken($response = true)
    {
        $secret      = SECRET_API . '/' . $this->timeExp;
        $ssl         = openssl_encrypt($secret, 'AES-128-CBC', KEY_TOKEN_API, OPENSSL_RAW_DATA, IV);
        $accessToken = (base64_encode($ssl));
        $output      = [
            'accessToken' => $accessToken,
        ];

        if ($response) {
            return self::sendResponse($output);
        } else {
            return $accessToken;
        }
    }

    protected function decodeAccessToken($value, $data = false)
    {
        $decode                  = base64_decode($value);
        $decrypted               = openssl_decrypt($decode, 'AES-128-CBC', KEY_TOKEN_API, OPENSSL_RAW_DATA, IV);
        $accessTokenDecode       = utf8_encode(trim($decrypted));
        $vectorAccessTokenDecode = explode('/', $accessTokenDecode);
        // ddd(openssl_decrypt($decode, 'AES-128-CBC', KEY_TOKEN_API, OPENSSL_RAW_DATA, IV),$decode, 'AES-128-CBC', KEY_TOKEN_API, OPENSSL_RAW_DATA, IV);

        $decodeValue = '';
        $size = count($vectorAccessTokenDecode) - 1;
        for ($i = 0; $i < $size; $i++) {
            $decodeValue .= $vectorAccessTokenDecode[$i];
            if ($i < $size - 1) {
                $decodeValue .= '/';
            }
        }
        $vectorAccessTokenDecode[1] = (int) $vectorAccessTokenDecode[1];
        //  Check If the Key is the Same
        if (SECRET_API !== $decodeValue) {
            return false;
        }
        // Return Data
        if ($data) {
            return $vectorAccessTokenDecode;
        }
        // Check Time
        $restTimestamp = time() - $vectorAccessTokenDecode[1];
        if (($restTimestamp > 1000000)  && ($vectorAccessTokenDecode[1] <= $this->timeExp)) {
            return true;
        } else {
            return false;
        }
    }


    protected function checkToken()
    {
        self::setHeaderRequest();
        self::checkHeaderRequest();
    }

    protected function checkHeaderRequest()
    {
        $isFoundMainSegment = false;
        $isFound            = false;
        $mainSegmentURL     = $this->uri->getSegment(2); // 2
        $segmentURL         = $this->uri->getSegment(3); // 3
        foreach ($this->exceptions as $value) {
            if (!is_array($value)) {
                $isFoundMainSegment = $isFoundMainSegment || (stripos($mainSegmentURL, $value) !== false);
                if ($isFoundMainSegment) {
                    break;
                }
            } else {
                // Exceptions URL Second Segments auth/generate-auth-token..............
                foreach ($value as $item) {
                    $isFound = $isFound || (stripos($segmentURL, $item) !== false);
                    if ($isFound) {
                        break;
                    }
                }
            }
        }

        // Specific Elements
        if ($this->specificElement) {
            // 3 / 4
            $segmentURLTemp = isset($this->uri->getSegments()[3]) ? $this->uri->getSegment(4) : null;
            if (isset($segmentURLTemp)) {
                if ($segmentURL == 'valid-user' && isset($segmentURLTemp) && $segmentURLTemp !== null) {
                    $isFound = false;
                    return;
                }
            }
        }
        // ddd($segmentURL, URLS_SUPEREXCEPTIONS, $isFound, $isFoundMainSegment);
        if (!in_array($segmentURL, URLS_SUPEREXCEPTIONS)) {
            if ($isFound || $isFoundMainSegment) {
                // Check Access Token
                self::checkDataToken('X-' . SHORT_NAME_PROJECT . '-Access-Token');
            } else {
                self::checkDataToken('X-' . SHORT_NAME_PROJECT . '-Auth-Token');
            }
        }
    }

    protected function checkDataToken($headerName)
    {
        $header = self::getHeader($headerName);
        if (checkIsEmpty($header)) {
            self::showErrorHeader('Debe reiniciar la aplicación, pues no existe un token de acceso valido.');
        }

        if (strstr(strtolower($headerName), 'access')) {
            self::checkAccessToken($header);
        }

        if (strstr(strtolower($headerName), 'auth')) {
            self::checkAuthToken($header);
        }
    }

    protected function checkAccessToken($header)
    {
        // Check if Token is valid (Format)
        $validationAccess = self::decodeAccessToken($header);
        if (checkIsEmpty($validationAccess) && $validationAccess !== true) {
            self::showErrorHeader('Invalid Access Token');
        }
    }
    protected function checkAuthToken($header)
    {
        //Validate Format Token
        $validationApi = $this->jwt->jwt_api_val($header);
        if (checkIsEmpty($validationApi) && $validationApi !== true) {
            self::showErrorHeader('Invalid Token, Not exist');
        }
        // Check the header in the DB
        $userTokens         = new UserToken();
        $checkUserWithToken = $userTokens->where(['authToken' => $header])->first();
        // dump($checkUserWithToken);
        if (checkIsEmpty($checkUserWithToken)) {
            self::showErrorHeader('Invalid Auth Token (User)');
        }

        if ($checkUserWithToken->authToken !== $header) {
            self::showErrorHeader('Invalid Token, the header is not the same with the requested');
        }
        // Check Date of token
        // $actuallyDate = date('Y-m-d H:i:s');
        // if (strtotime($checkUserWithToken->maxDate) <= strtotime($actuallyDate)) {
        //     self::showErrorHeader('Invalid Token, the token is caducade');
        // }
    }

    protected function getHeader($headerName)
    {
        $headers = getallheaders();
        $headerCheck = isset($headers[$headerName]) ? $headers[$headerName] : null;
        $headerName = ucwords(strtolower($headerName), '-');
        $headerCheckLower = isset($headers[$headerName]) ? $headers[$headerName] : null;

        // ddd($headerName, $headers, $headerCheck, $headerName, $headerCheckLower);

        if (!checkIsEmpty($headerCheck)) {
            return $headerCheck;
        } else if (!checkIsEmpty($headerCheckLower)) {
            return $headerCheckLower;
        } else {
            parent::showErrorHeader(['message' => 'Invalid Header', 'headers' => $headers]);
        }
    }

    protected function setHeaderRequest()
    {
        // Clear the old headers
        // header_remove();
        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // You can decide if the origin in $_SERVER['HTTP_ORIGIN'] is something you want to allow, or as we do here, just allow all
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        } else {
            //No HTTP_ORIGIN set, so we allow any. You can disallow if needed here
            header('Access-Control-Allow-Origin: *');
        }

        // treat this as json
        header('Content-Type: application/json;charset=utf-8');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 600');    // cache for 10 minutes

        if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT'); //Make sure you remove those you do not want to support
            }
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            } else {
                $headers = 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Headers';
                $tokens  = [
                    'X-' . SHORT_NAME_PROJECT . '-Auth-Token',
                    'X-' . SHORT_NAME_PROJECT . '-Access-Token',
                ];
                foreach ($tokens as $key => $value) {
                    $headers .= ", $value, " . strtolower($value) . ', ' . ucwords(strtolower($value), '-');
                }
                header("Access-Control-Allow-Headers: $headers");
            }
            //Just exit with 200 OK with the above headers for OPTIONS method
            exit(0);
        }
    }

    public function generateAuthToken($accessToken, $user, $complement = [])
    {
        if (checkIsEmpty($user)) {
            return (parent::sendResponse('The user is empty'));
        }

        // Add Complements to the Token
        // if (checkIsEmpty($complement)) {
        //     return (parent::sendResponseError('Error with the parameters - Login (version, platform)'));
        // }

        // Remove Private Data
        $user = parent::removeDirtySingleData($user);
        unset($user->phone);
        // Get Exp
        $accessToken = self::decodeAccessToken($accessToken, true);
        $uid = $user->uid;
        $token = [
            'actualTime'  => time(), // Actual Time
            'expireAccess' => $accessToken[1],
            'expireAuth'  => $this->timeExp, // Elapsed time
            'maxDate' => $this->timeToken,
            'data' => [ // 
                'access' => $accessToken[0],
                'uid' => $uid,
                // Set User
                'user' => [
                    'expireAccess' => $accessToken[1],
                    'actualTime'  => time(), // Actual Time
                    'expireAuth'  => $this->timeExp, // Elapsed time
                    'maxDate' => $this->timeToken,
                    // 'platformId' => $complement['platformId'],
                    // 'version' => $complement['version']
                ]
            ]
        ];
        $token['data']['user'] = array_merge($token['data']['user'], (array) ($user));
        $token  = $this->jwt->encode($token, SECRET_API);

        try {
            $userTokens = new UserToken();
            $tokenData  = $userTokens->select("authToken")->where(['uid' => $uid])->first();
            if (isset($tokenData)) {
                $token = $tokenData->authToken;
            }else{
                $newData = [
                    'uid'       => $uid,
                    'authToken' => $token,
                    // 'maxDate'   => $this->timeToken,
                    // 'platformId' => $complement['platformId'],
                    // 'version'   => $complement['version'],
                    // 'pushToken' => null,
                ];

                // if (isset($complement['pushToken']) && !empty($complement['pushToken'])) {
                //     $newData['pushToken'] = $complement['pushToken'];
                // }

                $userTokens = new UserToken();
                $userTokens->insert($newData);
            }
        } catch (Exception $ex) {
            return (parent::sendResponseError(['message' => $ex->getMessage()]));
        }
        return $token;
    }

    protected function getHeaderAuthToken()
    {
        return $this->request->getHeader('X-' . SHORT_NAME_PROJECT . '-Auth-Token')->getValue();
    }
    protected function getHeaderAccessToken()
    {
        return $this->request->getHeader('X-' . SHORT_NAME_PROJECT . '-Access-Token')->getValue();
    }

    protected function decodeAuthToken($authToken)
    {
        if (!isset($authToken)) {
            // JWT
            parent::showErrorHeader('Debe iniciar sesión antes de realizar cualquier acción');
        }
        $data      = $this->jwt->decode($authToken, SECRET_API, true);
        $jwtDecode = json_decode(json_encode($data), true);
        return $jwtDecode;
    }

    protected function getDataUser()
    {
        // Get Token
        $authToken  = self::getHeaderAuthToken();
        $contentJWT = self::decodeAuthToken($authToken);
        // Get Data of User Token
        $uid = self::forceArryToJSON($contentJWT['data']['user']);
        return $uid;
    }
}

class Jwt
{

    /**
     * Decodes a JWT string into a PHP object.
     *
     * @param string      $jwt    The JWT
     * @param string|null $key    The secret key
     * @param bool        $verify Don't skip verification process
     *
     * @return object      The JWT's payload as a PHP object
     * @throws UnexpectedValueException Provided JWT was invalid
     * @throws DomainException          Algorithm was not provided
     *
     * @uses jsonDecode
     * @uses urlsafeB64Decode
     */

    public static function decode($jwt, $key = null, $verify = true)
    {

        $tmp = true;

        $tks = explode('.', $jwt);
        if (count($tks) != 3) {
            $tmp = false;
            //echo 'Wrong number of segments';
        }

        list($headb64, $bodyb64, $cryptob64) = $tks;
        if (null === ($header = JWT::jsonDecode(JWT::urlsafeB64Decode($headb64)))) {
            $tmp = false;
            //echo 'Invalid segment encoding';
        }
        if (null === $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64))) {
            throw new \UnexpectedValueException('Invalid segment encoding');
        }
        $sig = JWT::urlsafeB64Decode($cryptob64);
        if ($verify) {
            if (empty($header->alg)) {
                $tmp = false;
                //echo 'Empty algorithm';
            }
            if ($sig != JWT::sign("$headb64.$bodyb64", $key, $header->alg)) {
                $tmp = false;
                //echo 'Signature verification failed';
            }
        }

        if ($tmp) {
            return $payload;
        } else {
            return "invalido";
        }
    }
    /**
     * Converts and signs a PHP object or array into a JWT string.
     *
     * @param object|array $payload PHP object or array
     * @param string       $key     The secret key
     * @param string       $algo    The signing algorithm. Supported
     *                              algorithms are 'HS256', 'HS384' and 'HS512'
     *
     * @return string      A signed JWT
     * @uses jsonEncode
     * @uses urlsafeB64Encode
     */
    public static function encode($payload, $key, $algo = 'HS256')
    {

        $header = array('typ' => 'JWT', 'alg' => $algo);
        $segments = array();
        $segments[] = JWT::urlsafeB64Encode(JWT::jsonEncode($header));
        $segments[] = JWT::urlsafeB64Encode(JWT::jsonEncode($payload));
        $signing_input = implode('.', $segments);
        $signature = JWT::sign($signing_input, $key, $algo);
        $segments[] = JWT::urlsafeB64Encode($signature);
        return implode('.', $segments);
    }
    /**
     * Sign a string with a given key and algorithm.
     *
     * @param string $msg    The message to sign
     * @param string $key    The secret key
     * @param string $method The signing algorithm. Supported
     *                       algorithms are 'HS256', 'HS384' and 'HS512'
     *
     * @return string          An encrypted message
     * @throws DomainException Unsupported algorithm was specified
     */
    public static function sign($msg, $key, $method = 'HS256')
    {

        $methods = array(
            'HS256' => 'sha256',
            'HS384' => 'sha384',
            'HS512' => 'sha512',
        );
        if (empty($methods[$method])) {
            throw new \DomainException('Algorithm not supported');
        }
        return hash_hmac($methods[$method], $msg, $key, true);
    }
    /**
     * Decode a JSON string into a PHP object.
     *
     * @param string $input JSON string
     *
     * @return object          Object representation of JSON string
     * @throws DomainException Provided string was invalid JSON
     */
    public static function jsonDecode($input)
    {
        $obj = json_decode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            JWT::_handleJsonError($errno);
        } else if ($obj === null && $input !== 'null') {
            throw new \DomainException('Null result with non-null input');
        }
        return $obj;
    }
    /**
     * Encode a PHP object into a JSON string.
     *
     * @param object|array $input A PHP object or array
     *
     * @return string          JSON representation of the PHP object or array
     * @throws DomainException Provided object could not be encoded to valid JSON
     */
    public static function jsonEncode($input)
    {
        $json = json_encode($input);
        if (function_exists('json_last_error') && $errno = json_last_error()) {
            JWT::_handleJsonError($errno);
        } else if ($json === 'null' && $input !== null) {
            throw new \DomainException('Null result with non-null input');
        }
        return $json;
    }
    /**
     * Decode a string with URL-safe Base64.
     *
     * @param string $input A Base64 encoded string
     *
     * @return string A decoded string
     */
    public static function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }
    /**
     * Encode a string with URL-safe Base64.
     *
     * @param string $input The string you want encoded
     *
     * @return string The base64 encode of what you passed in
     */
    public static function urlsafeB64Encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }
    /**
     * Helper method to create a JSON error.
     *
     * @param int $errno An error number from json_last_error()
     *
     * @return void
     */
    private static function _handleJsonError($errno)
    {
        $messages = array(
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_CTRL_CHAR => 'Unexpected control character found',
            JSON_ERROR_SYNTAX => 'Syntax error, malformed JSON',
        );
        throw new \DomainException(
            isset($messages[$errno])
                ? $messages[$errno]
                : 'Unknown JSON error: ' . $errno
        );
    }

    public function decode_access_token($code)
    {

        $decrypted = openssl_decrypt(base64_decode($code), 'AES-128-CBC', KEY_TOKEN_API, OPENSSL_RAW_DATA, IV);
        /*d($decrypted);*/
        $access_token_decode = utf8_encode(trim($decrypted));
        /*d($access_token_decode);*/
        $vector_access_token_decode = explode("/", $access_token_decode);
        /*d($vector_access_token_decode);*/
        $resta_timestamp = time() - $vector_access_token_decode[1];

        if (SECRET_API == $vector_access_token_decode[0]) {

            if ($resta_timestamp < 1000000) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    protected function hex2bin($hexdata)
    {

        $bindata = '';
        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }
        return $bindata;
    }

    public function jwt_api_val($auth_token)
    {

        $data = $this->decode($auth_token, SECRET_API, true);
        $info_jwt = json_decode(json_encode($data), true);

        if (is_array($info_jwt)) {
            //token valido
            return true;
        } else {
            //token no valido
            return false;
        }
    }
}
