<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\Core;
// namespace App\Libraries\Kubo\Controller\Core;

// Add Library
require_once('Kubo.php');

/**
 * Migrations
 * @package Kubo
 */
class Migration extends \Kubo\Base
{
    protected $context;
    private $primaryId;
    private $tableName;
    private $fields;
    private $primaryIdException;
    private $foreignKeys = [];

    public function __construct($context, $tableName, $fields, $foreignKeys = [], $primaryIdException = null)
    {
        $this->context = $context;
        $this->tableName = $tableName;
        $this->fields = $fields;
        $this->foreignKeys = $foreignKeys;
        $this->primaryIdException = $primaryIdException;
    }

    public function getTableName()
    {
        return $this->tableName;
    }

    public function getId()
    {
        # Primary ID
        $this->primaryId = $this->tableName . 'Id';
        $id = [
            $this->primaryId          => [
                'type'           => 'INT',
                'constraint'     => 10,
                'unsigned'       => true,
                'auto_increment' => true,
            ]
        ];
        if (!checkIsEmpty($this->primaryIdException)) {
            $id = $this->primaryIdException;
        }
        return $id;
    }

    public function getSingleId()
    {
        # Primary ID
        if (isset($this->primaryIdException)) {
            $id = (array_keys($this->primaryIdException)[0]);
        } else {
            $id = $this->tableName . 'Id';
        }
        $this->primaryId = $id;
        return $id;
    }

    public function getFields()
    {
        # Primary ID
        $id = $this->getId();
        // Fields User
        $data = [];
        $status = [
            'status'     => [
                'type'       => "ENUM('PENDING','ACTIVE','INACTIVE')",
                'null'       => true,
                'default'    => 'ACTIVE',
            ],
            'updated_at' => [
                'type' => 'TIMESTAMP',
            ],
            'created_at' => [
                'type' => 'TIMESTAMP',
            ],
            'deleted_at' => [
                'type' => 'TIMESTAMP',
            ]
        ];
        $makeForeignKeys = self::makeForeignKeys();
        // Company Files Table
        $data = array_merge($id, $this->fields, $makeForeignKeys, $status);
        return $data;
    }

    public function makeForeignKeys()
    {
        // Foreign Keys - Unsign
        $foreignKeys = [];
        if (!checkIsEmpty($this->foreignKeys)) {
            foreach ($this->foreignKeys as $key => $value) {
                $element = $value['foreignKey'];
                $foreignKeys[$element] = [
                    'type' => $value['type'] ?? 'INT',
                    'constraint' => $value['constraint'] ?? 100,
                ];
                if ($foreignKeys[$element]['type'] !== 'VARCHAR') {
                    $foreignKeys[$element]['unsigned'] = $value['unsigned'] ?? true;
                }
            }
        }
        return $foreignKeys;
    }

    public function getAddForeignKeys()
    {
        // Foreign Keys - Unsign
        if (!checkIsEmpty($this->foreignKeys)) {
            $foreignKeys = [];
            foreach ($this->foreignKeys as $key => $value) {
                $foreignKeys[] = self::addForeignKey($this->tableName, $value['foreignKey'], $value['references']);
            }
            return $foreignKeys;
        }
    }
    public function getDropForeignKey()
    {
        // Foreign Keys - Unsign
        if (!checkIsEmpty($this->foreignKeys)) {
            $foreignKeys = array();
            foreach ($this->foreignKeys as $key => $value) {
                $foreignKeys[] = (self::dropForeignKey($this->tableName, $value['foreignKey']));
            }
            return $foreignKeys;
        }
    }

    public function up($forge)
    {
        // $forge = $this->context->forge;
        // Add Fields
        $forge->addField(self::getFields());

        // Primary Key
        $forge->addKey(self::getSingleId(), true);

        // Make DB
        $forge->createTable(self::getTableName(), true, [
            'CHARSET' => 'utf8',
            'COLLATE' => 'utf8_unicode_ci',
        ]);
        // Foreign Keys - Unsign
        if (!checkIsEmpty($this->foreignKeys)) {
            $forge->addColumn(self::getTableName(), self::getAddForeignKeys());
        }
    }


    public function down($forge)
    {
        // $forge = $this->context->forge;
        // Foreign Keys - Unsign
        if (!checkIsEmpty($this->foreignKeys)) {
            $forge->addColumn(self::getTableName(), self::getDropForeignKey());
        }
        // Drop Table
        $forge->dropTable(self::getTableName());
    }


    /**
     * @param string $table      Table name
     * @param string $foreignKey Collumn name having the Foreign Key
     * @param string $references Table and column reference. Ex: users(id)
     * @param string $onDelete   RESTRICT, NO ACTION, CASCADE, SET NULL, SET DEFAULT
     * @param string $onUpdate   RESTRICT, NO ACTION, CASCADE, SET NULL, SET DEFAULT
     *
     * @return string SQL command
     */
    function addForeignKey($table, $foreignKey, $references, $onDelete = 'RESTRICT', $onUpdate = 'RESTRICT')
    {
        $table = strtolower($table);
        $references = explode('(', str_replace(')', '', str_replace('`', '', $references)));
        return "CONSTRAINT `{$table}_{$foreignKey}_fk` FOREIGN KEY (`{$foreignKey}`) REFERENCES `{$references[0]}`(`{$references[1]}`) ON DELETE {$onDelete} ON UPDATE {$onUpdate}";
    }

    /**
     * @param string $table      Table name
     * @param string $foreignKey Collumn name having the Foreign Key
     *
     * @return string SQL command
     */
    function dropForeignKey($table, $foreignKey)
    {
        $table = strtolower($table);
        return "ALTER TABLE `{$table}` DROP FOREIGN KEY `{$table}_{$foreignKey}_fk`";
    }

    /**
     * @param string $triggerName Trigger name
     * @param string $table       Table name
     * @param string $statement   Command to run
     * @param string $time        BEFORE or AFTER
     * @param string $event       INSERT, UPDATE or DELETE
     * @param string $type        FOR EACH ROW [FOLLOWS|PRECEDES]
     *
     * @return string SQL Command
     */
    function addTrigger($triggerName, $table, $statement, $time = 'BEFORE', $event = 'INSERT', $type = 'FOR EACH ROW')
    {
        return 'DELIMITER ;;' . PHP_EOL . "CREATE TRIGGER `{$triggerName}` {$time} {$event} ON `{$table}` {$type}" . PHP_EOL . 'BEGIN' . PHP_EOL . $statement . PHP_EOL . 'END;' . PHP_EOL . 'DELIMITER ;;';
    }
    /**
     * @param string $trigger_name Trigger name
     *
     * @return string SQL Command
     */
    function dropTrigger($trigger_name)
    {
        return "DROP TRIGGER `{$trigger_name}`;";
    }
}
