<?php
// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo;
# Libraries
use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use Config\Services;
use Exception;
/**
 * Config
 */
require_once 'Complements.php';
ini_set('memory_limit', (1024 * 3) . 'M');
set_time_limit(0);
# Develop
// Develop Version
define('VERSION', 1);
// The project is in Debug?
define('DEBUG', false);
// Status of Project: development testing production
define('DEPLOY', 'development');
// Actual version of the project v1, v2, v3....
define('DEPLOY_VERSION', 1);
# Project
// Name of the project
define('NAME_PROJECT', 'Tripode');
// Short Name and (X-<SH>-Access-Token or X-<SN>-Auth-Token)
define('SHORT_NAME_PROJECT', 'TP');
// Time
define('TIMEZONE', 'America/Bogota');
// Token Data
define('TIME_ACCESS_TOKEN', 4);
define('TIME_AUTH_TOKEN', 24);
// Reponse
define('CODE_RESPONSE', 100); // 100 - 299
define('CODE_RESPONSE_ERROR', 300); // 300 - 499
// Urls Actions
define('URLS_EXCEPTION', [
    '',
    // SHORT_NAME_PROJECT,
    // strtolower(SHORT_NAME_PROJECT),
    //'location', // Single Exception
    'platform', // Single Exception
    'auth' => [ // Exception  Only items inside and NOT the general element (auth)
        'generate-auth-token',
        'recovery',
        'valid-email',
        'valid-code',
        'change-password',
        'signup',
        'login',
        'loginFacebook',
        'addPhoneToAccount',
        'sendVerificationAccount',
        'verifyAccount',
    ],
]);
define('URLS_SUPEREXCEPTIONS', [
    'generate-access-token',
    'refresh-access-token',
    'valid-user',
    'index',
    // 'getSubcategoriesOrganizedByCategory',
]);
class Base extends Controller
{
    use ResponseTrait;
    protected function init()
    {
        self::defineEnvironments();
    }
    protected function defineEnvironments()
    {
        # Secrets
        // Keys of the Project
        define('IV_PROJECT', substr(md5(strrev(strlen(NAME_PROJECT)), true), 0, 16));
        define('KEY_TOKEN_API_PROJECT', substr(base64_encode(strrev(NAME_PROJECT)), 0, 16));
        define('SECRET_API_PROJECT', substr(sha1(strrev(SHORT_NAME_PROJECT . NAME_PROJECT . SHORT_NAME_PROJECT), true), 0, 16));
        // Constants Auth TOKENS
        $iv = strrev(IV_PROJECT . SHORT_NAME_PROJECT);
        // ddd(IV_PROJECT . SHORT_NAME_PROJECT);
        define('IV', substr($iv, 0, 16));
        $keyTokenApi = base64_encode(KEY_TOKEN_API_PROJECT . strrev(NAME_PROJECT));
        define('KEY_TOKEN_API', $keyTokenApi);
        $secretApi = base64_encode(SECRET_API_PROJECT . strrev(NAME_PROJECT));
        define('SECRET_API', $secretApi);
        // Times
        if (!date_default_timezone_set(TIMEZONE)) {
            self::showErrorHeader("The system doesn't know WTFUTC.  Maybe try updating tzinfo with your package manager?");
        }
        // Show Data
        // ddd(IV_PROJECT,IV);
    }
    /** Functions Base **/
    public function showErrorHeader($message)
    {
        // clear the old headers
        // header_remove();
        // treat this as json
        header('Content-Type: application/json;charset=utf-8');
        http_response_code(400);
        echo (json_encode(['code' => CODE_RESPONSE_ERROR, 'message' => $message], JSON_PRETTY_PRINT));
        exit();
    }
    public function sendResponse($result, $code = 0, string $message = '')
    {
        if ($code === 0) {
            $code = CODE_RESPONSE;
        }
        $response = [
            'code' => $code,
        ];
        $data = self::checkResponse($result);
        $data['message'] = $message;
        $response = array_merge($response, $data);
        return self::jsonResponse($response, $code, $message);
    }
    public function checkResponse($result)
    {
        $response = [];
        if (is_string($result)) {
            $response['message'] = $result;
        } else {
            if (is_array($result) && isset($result['message'])) {
                $response['message'] = $result['message'];
                unset($result['message']);
            }
            if (is_array($result) && isset($result['data'])) {
                $response['data'] = $result['data'];
                unset($result['data']);
            }else{
                $response['data'] = $result;
            }
        }
        return $response;
    }
    public function sendResponseError($error, $code = 0)
    {
        if ($code === 0) {
            $code = CODE_RESPONSE_ERROR;
        }
        $response = [
            'code' => $code,
        ];
        $response = array_merge($response, self::checkResponse($error));
        return self::jsonResponse($response, $code);
    }
    public function jsonResponse($data = null, $httpStatus)
    {
        // treat this as jso
        header('Content-Type: application/json;charset=utf-8');
        // set the header to make sure cache is forced
        header('Cache-Control: no-cache, private');
        header('Expires: Mon, 8 Jul 1997 22:00:00 GMT');
        // Format And Remove Dirty Data
        if (isset($data) && isset($data['data'])) {
            //$data['data'] = self::formatData($data['data']);
        }
        if ($httpStatus >= CODE_RESPONSE && $httpStatus < CODE_RESPONSE_ERROR) {
            $status = 200;
            http_response_code($status);
            $this->response->setStatusCode($status)->setBody($data);
            return $this->respond($data, $status); // Helper Codeigniter
        } else {
            $status = 400;
            http_response_code($status);
            $this->response->setStatusCode($status)->setBody($data);
            $this->fail($data, $status); // Helper Codeigniter
        }

        return json_encode($data, JSON_PRETTY_PRINT);
    }
    public function formatData($data)
    {
        if (!checkIsEmpty($data)) {
            $count = 0;
            foreach ($data as $key => $value) {
                $dataBase = $data;
                // TODO: CHECK WHEN IS ARRAY
                if (is_object($value)) {
                    $value = self::formatData($value);
                    if (is_object($data)) {
                        $data->$key = $value;
                    } else {
                        $data[$key] = $value;
                    }
                } else if(is_array($value)){
                    $value = self::formatData($value);
                        $data[$key] = $value;
                } else {
                    // Structure Object
                    self::structureData($dataBase, $key, $value, $count);
                }
                $count++;
            }
            // Remove elements Exception
            $data = self::removeDirtySingleData($data);
        }
        return $data;
    }
    public function structureData($dataBase, $key, $value, $count)
    {
        if (preg_match('/\b(\w*Id\w*)\b/', $key, $matches, PREG_OFFSET_CAPTURE)) {
            $dataBase->$key = (int) $value;
            // if ($count > 0 && $count < DEEPSEARCH) {
            //     $replaceId = str_replace("Id", "", $key);
            //     $nameClass = ucfirst($replaceId);
            //     // Get Object
            //     $class = "App\Models\\" . $nameClass;
            //     $instance = new $class();
            //     $data = (self::getObject($value, $instance));
            //     unset($dataBase->$key);
            //     $dataBase->$replaceId = self::formatData($data);
            // }
        }
    }
    public function checkIsEmpty($value)
    {
        // Null
        $checkNull = is_null($value);
        if ($checkNull) {
            return $checkNull;
        }
        // Array
        if (is_array($value)) {
            return (!isset($value) || empty($value) || count($value) <= 0) || $value === null;
        }
        // String
        if (is_string($value)) {
            return (!isset($value) || trim($value) === '');
        }
        return (!isset($value)) || empty($value);
    }
    public function removeDirtyData($data)
    {
        // Remove Dirty Data
        foreach ($data as $key => $value) {
            $data[$key] = self::removeDirtySingleData($value);
        }
        return $data;
    }
    public function removeDirtySingleData($data)
    {
        // Remove Dirty Data
        if (is_array($data)) {
            unset($data['status']);
            unset($data['created_at']);
            unset($data['updated_at']);
            unset($data['deleted_at']);
        } else {
            if (isset($data->status)) {
                unset($data->status);
            }
            if (isset($data->created_at)) {
                unset($data->created_at);
            }
            if (isset($data->updated_at)) {
                unset($data->updated_at);
            }
            if (isset($data->deleted_at)) {
                unset($data->deleted_at);
            }
        }
        return $data;
    }
    public function forceArryToJSON($array)
    {
        return json_decode(json_encode($array, JSON_FORCE_OBJECT));
    }
    /** Request **/
    public function getRequest($isJSON = true)
    {
        $request = null;
        if ($isJSON) {
            $request = json_decode(file_get_contents("php://input"), true);
        } else {
            $request =  $this->request->getPost();
        }
        if (checkIsEmpty($request)) {
            self::showErrorHeader('Empty Data');
        }
        return $request;
    }
    // Check Results
    public function checkValidation($model, $request = null, $isJSON = true)
    {
        $validation = Services::validation();
        // Validate Data
        $validation->setRules($model->validationRules, $model->validationMessages);
        // Check Form Data & Execute Value
        try {
            if ($isJSON && isset($request)) {
                $validation->run($request);
            } else {
                $validation->withRequest($this->request)->run();
            }
        } catch (Exception $th) {
            //throw $th;
            ddd($th);
        }
        // Show Errors
        $string = '';
        $errors = $validation->getErrors();
        if (!empty($errors)) {
            foreach ($errors as $key => $value) {
                $string .= $value . ' ';
            }
        }
        return $string;
    }
    public function setValidations($model, $validationRules = null, $validationMessages = null)
    {
        if (isset($validationRules)) {
            $model->validationRules = array_merge($model->validationRules, $validationRules);
        }
        if (isset($validationMessages)) {
            $model->validationMessages = array_merge($model->validationMessages, $validationMessages);
        }
        return $model;
    }
    public function getObject($id, $model = null)
    {
        $tempModel = $this->model;
        if (!checkIsEmpty($model)) {
            $tempModel = $model;
        }
        # Remove Hidden Files
        $object = $tempModel->find($id);
        if ($object === null) {
            $id = [$tempModel->primaryKey => $id];
            $object = $tempModel->where($id)->find();
        }
        if (!checkIsEmpty($tempModel->hiddenFields)) {
            foreach ($object as $key => $value) {
                if (in_array($key, $tempModel->hiddenFields)) {
                    unset($object->$key);
                }
            }
        }
        if(checkIsEmpty($object)){
            self::showErrorHeader('Empty Data');
        }
        return $object;
    }
    /** Upload Files */
    public function checkFiles()
    {
        // Check files to Upload
        if (!checkIsEmpty($_FILES)) {
            // Get all files in the request
            $files = $this->request->getFiles();
            if (!checkIsEmpty($files)) {
                return $files;
            }
        }
    }
    public function uploadFile($pathFolderFiles, $listFiles = [])
    {
        $files = self::checkFiles();
        if (checkIsEmpty($files)) {
            return ['error' => 'Debe ingresar los archivos solicitados'];
        }
        $dataTemp = [];
        // Check list elements
        $error = [];
        if (!checkIsEmpty($listFiles)) {
            $arrayNames = array_keys($files);
            $listFilesKeys = array_map(function ($item) {
                return $item['key'];
            }, $listFiles);
            $diff = array_diff($listFilesKeys, $arrayNames);
            if (!checkIsEmpty($diff)) {
                $error = 'Faltan los siguientes archivos: ';
                //Anonymous function
                $count = 0;
                foreach ($listFiles as $key => $value) {
                    # code...
                    foreach ($diff as $keyDiff => $valueDiff) {
                        if ($value['key'] === $valueDiff) {
                            $error .=  $value['name'];
                            if (count($listFiles) > 0 && $count++ < count($diff) - 1) {
                                $error .= ', ';
                            }
                        }
                    }
                }
                $error = ['error' => $error];
            }
        }
        if (!checkIsEmpty($error)) {
            return $error;
        }
        foreach ($files as $key => $file) {
            if ($file->getSize() <= 0) {
                return ['error' => "Falta el archivo: {$key}. "];
            }
            if ($file->isValid() && !$file->hasMoved()) {
                $nameFile = uniqid($key) . '_' . time() . '.' . $file->getClientExtension();
                // Add to DB
                $dataTemp[$key] = $pathFolderFiles . '/' . $nameFile;
                // ddd($pathFolderFiles . ', ' . $nameFile);
                $file->move($pathFolderFiles, $nameFile);
                // echo $pathFile;
            }
        }
        return $dataTemp;
    }
    public function disableForeignKeyChecks()
    {
        $forge = \Config\Database::forge(config('Database')->defaultGroup);
        $db = $forge->getConnection();
        $db->disableForeignKeyChecks();
    }
    public function enableForeignKeyChecks()
    {
        $forge = \Config\Database::forge(config('Database')->defaultGroup);
        $db = $forge->getConnection();
        $db->enableForeignKeyChecks();
    }
}
