<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\v1;

use App\Controllers\BaseController;
use App\Models\Platform as PlatformModel;
use CodeIgniter\Database\Exceptions\DataException;
use CodeIgniter\Exceptions\ModelException;
use CodeIgniter\Database\Exceptions\DatabaseException;
use CodeIgniter\HTTP\Exceptions\HTTPException;
use CodeIgniter\Validation\Exceptions\ValidationException;
use InvalidArgumentException;

/**
 * Platform Service
 * @package Kubo
 */
class Platform extends BaseController
{

    // Model
    protected $model;

    public function __construct()
    {
        parent::__construct();
        // Init Variables
        $this->model = new PlatformModel();
    }

    // Set URLs Base
    public static function setUrls($routes)
    {
        // Base elements
        $routes->group('v' . DEPLOY_VERSION, ['namespace' => 'Kubo\v1'], function ($routes) {
            $routes->group(strtolower(SHORT_NAME_PROJECT), function ($routes) {
                $routes->resource('platform', ['controller' => 'Platform']);
            });
        });
    }

    /**
     * Display a listing of the resource.
     * @return string|false|mixed 
     * @throws HTTPException 
     */
    public function index()
    {
        $allData = $this->model->where(['status !=' => 'INACTIVE'])->findAll();
        if (checkIsEmpty($allData)) {
            return self::sendResponseError('La lista de elementos esta vacia');
        }
        return self::sendResponse($allData);
    }

    /**
     * Show the form for creating a new resource
     * @return string|false|mixed 
     * @throws HTTPException 
     * @throws DataException 
     * @throws ModelException 
     * @throws InvalidArgumentException 
     * @throws DatabaseException 
     */
    public function create()
    {
        // Create Model
        $request = self::getRequest();

        // Check Data
        $validation = self::checkValidation($this->model, $request);

        if (!checkIsEmpty($validation)) {
            return self::sendResponseError($validation);
        }

        // Make Object
        $id = $this->model->insert($request);
        return self::sendResponse(self::getObject($id));
    }

    /**
     * Display the specified resource.
     * @param int|null $id 
     * @return string|false|mixed 
     * @throws HTTPException 
     */
    public function show(int $id = null)
    {
        return self::sendResponse(self::getObject($id));
    }

    /**
     * Update the specified resource in storage.
     * @param int|null $id 
     * @return string|false|mixed 
     * @throws HTTPException 
     * @throws DataException 
     * @throws ModelException 
     * @throws InvalidArgumentException 
     * @throws DatabaseException 
     */
    public function update(int $id = null)
    {
        // Create Model
        $request = self::getRequest();

        // Check Data
        $validation = self::checkValidation($this->model, $request);

        if (!checkIsEmpty($validation)) {
            return self::sendResponseError($validation);
        }
        $this->model->update($id, $request);
        return self::sendResponse(self::getObject($id));
    }

    /**
     * Remove the specified resource from storage.
     * @param int|null $id 
     * @return string|false|mixed 
     * @throws ModelException 
     * @throws InvalidArgumentException 
     * @throws DatabaseException 
     * @throws DataException 
     * @throws HTTPException 
     * @throws ValidationException 
     */
    public function delete(int $id = null)
    {
        $data = $this->model->asArray()->find($id);
        if (checkIsEmpty($data)) {
            return self::sendResponseError("No se encuentra el elemento con id {$id}");
        }
        $model = $this->model->where(['status' => 'INACTIVE'])->find($id);
        if (isset($model)) {
            return self::sendResponseError("Ya se eliminio el elemento con id {$id}");
        }

        // Delete Object
        $this->model->update($id, ['status' => 'INACTIVE']);
        return self::sendResponse('Elemento eliminado');
    }
}
