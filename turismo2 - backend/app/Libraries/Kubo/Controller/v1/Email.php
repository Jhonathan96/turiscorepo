<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\v1;

use Exception;
use PHPMailer\PHPMailer\PHPMailer;

/**
 * Email Kubo
 * @package Kubo\v1
 */
class Email
{
    private $mail;

    // temp data
    private $tempConfig = [
        'protocol' => 'smtp',
        'smtp_host' => 'smtp.mailtrap.io',
        'smtp_port' => 2525,
        'smtp_user' => '2d0801dcdb3d6d',
        'smtp_pass' => '2da2def2a4fdab',
        'crlf' => "\r\n",
        'newline' => "\r\n"
    ];
    private $dataEmail    = [
        'protocol'  => 'smtp',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465, //587 OFFICE;
        // 'smtp_user' => 'davila@kubo.co',
        // 'smtp_pass' => 'accesoKubo',
        'smtp_user' => 'cesarpea15@gmail.com',
        'smtp_pass' => '@488Gtb0',
        'mailtype'  => 'html',
        'charset'   => 'UTF-8',
        'protocol'  => 'smtp',
        'crlf'      => "\r\n",
        'newline'   => "\r\n",
    ];


    private $pathEmail = APPPATH . 'Libraries/Kubo/emails/';

    public function __construct()
    {
        // parent::__construct();
        self::configEmail();
    }

    public function configEmail()
    {
        try {
            $mail = new PHPMailer(true);
            $config = DEBUG ? $this->tempConfig : $this->dataEmail;
            // $config = $this->dataEmail;
            //Server settings
            // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      
            // Enable verbose debug output
            $mail->isSMTP();
            // Set the SMTP server to send through
            $mail->Host       = $config['smtp_host'];
            // Enable SMTP authentication
            $mail->SMTPAuth   = true;
            // SMTP username
            $mail->Username   = $config['smtp_user'];
            // SMTP password
            $mail->Password   = $config['smtp_pass'];
            // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
            // TCP port to connect to
            $mail->Port       = $config['smtp_port'];
            //Recipients
            $mail->setFrom('no-replay@kubo.co', NAME_PROJECT . ' - Kubo');
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->AltBody = 'Kubo.co 2020';
            $this->mail    = $mail;
        } catch (Exception $e) {
            exit("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
        }
    }

    public function send($to, $subject, $message)
    {
        //ddd($to, $subject, $message);
        if(!isset($this->mail)){
            exit('No init email');
        }
        try {
            $this->mail->clearAllRecipients();
            $this->mail->clearAttachments();   //Remove all attachements
            //Recipients
            $mail          = clone $this->mail;
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->addAddress($to); // Name is optional
            $mail->send();
        } catch (Exception $e) {
            exit("Message could not be sent. Mailer Error: {$e}");
        }
    }

    public function sendMail()
    {
        if(!isset($this->mail)){
            exit('No init email');
        }
        // Data
        $data = $this->request->getPost();
        try {
            //Server settings
            header('Content-Type: text/html; charset=utf-8');
            $this->mail->Subject = $data['subject'];
            $this->mail->Body    = ($data['message']);
            $this->mail->AltBody = 'Kubo.co 2020';
            $this->mail->send();
            exit('Message has been sent');
        } catch (Exception $e) {
            exit("Message could not be sent. Mailer Error: {$this->mail->ErrorInfo}");
        }
    }


    public function makeRecoveryEmail($code)
    {
        $message = '';
        // Add Variables Content
        $title = 'Pagamos por contenido';
        $description = 'Hola, el siguiente es tu código de confirmación';
        $content = '<h1 class="code">' . wordwrap($code, 1, " ", true) . '</h1>';
        $contact = '
        <p>
            En caso de no haber solicitado un cambio de contraseña comunícate con nosotros o haz caso omiso a este correo
        </p>';
        // Add Contact
        $content .= $contact;
        // Get Element Base
        try {
            ob_end_clean();
            ob_start();
            if (!@require_once($this->pathEmail . '/template.php')) {
                ob_end_clean();
            }
            $message = ob_get_contents();
            ob_end_clean();
        } catch (Exception $ex) {
            exit(['message' => $ex->getMessage()]);
        }
        return $message;
    }

    public function makeRegisterUser($email, $user)
    {
        $message = '';
        // Add Variables Content
        $title = '¡Estás a un paso de ser parte de ' . NAME_PROJECT . '!';
        $description = 'Te haz registrado como Usuario en ' . NAME_PROJECT . '';
        $content = '<h1><b class="title">Usuario:</b> ' . $email . '</h1>';
        $content .= '<h1><b class="title">Nombre:</b> ' . $user . '</h1>';
        $contact = '
        <p>
            En caso de preguntas no dudes en contactarte con nosotros al correo contacto@tripode.co
        </p>';
        // Add Contact
        $content .= $contact;
        // Get Element Base
        try {
            ob_end_clean();
            ob_start();
            if (!@require_once($this->pathEmail . '/template.php')) {
                ob_end_clean();
            }
            $message = ob_get_contents();
            ob_end_clean();
        } catch (Exception $ex) {
            exit(['message' => $ex->getMessage()]);
        }
        return $message;
    }

    public function makeVerification($description, $email, $user)
    {
        $message = '';
        // Add Variables Content
        $title = '¡Bienvendio a ' . NAME_PROJECT . '!';
        $content = '<h1><b class="title">Usuario:</b> ' . $email . '</h1>';
        $content .= '<h1><b class="title">Nombre:</b> ' . $user . '</h1>';
        $contact = '
        <p>
            En caso de preguntas no dudes en contactarte con nosotros
        </p>';
        // Add Contact
        $content .= $contact;
        // Get Element Base
        try {
            ob_end_clean();
            ob_start();
            if (!@require_once($this->pathEmail . '/verify.php')) {
                ob_end_clean();
            }
            $message = ob_get_contents();
            ob_end_clean();
        } catch (Exception $ex) {
            exit(['message' => $ex->getMessage()]);
        }
        return $message;
    }

    public function makeValidateUser($code)
    {
        $message = '';
        // Add Variables Content
        $title = 'Valida tu cuenta de ' . NAME_PROJECT . '';
        $subtitle = 'Verifica tu cuenta';
        $description = 'Antes de empezar con' . NAME_PROJECT . ' es importante validar tu cuenta Haciendo clic abajo:';
        $content = '<h1><a href="' . base_url() . '/v1/auth/valid-user/' . $code . '" class="button">Has click Aquí</a></h1>';
        $copyright = 'Kubo 2020';
        $contact = '
        <p>
            O accede al siguiente link<br/>
            <a href="' . base_url() . '/v1/auth/valid-user/' . $code . '">' . base_url() . '/v1/auth/valid-user/' . $code . '</a>
        </p>
        <p>
            En caso de preguntas no dudes en contactarte con nosotros
        </p>';
        // Add Contact
        $content .= $contact;
        // Get Element Base
        try {
            ob_end_clean();
            ob_start();
            if (!@require_once($this->pathEmail . '/template.php')) {
                ob_end_clean();
            }
            $message = ob_get_contents();
            ob_end_clean();
        } catch (Exception $ex) {
            exit(['message' => $ex->getMessage()]);
        }
        return $message;
    }

    public function makeResetPassword($newPass)
    {
        $message = '';
        // Add Variables Content
        $title = 'Restablecer contraseña de ' . NAME_PROJECT . '';
        $subtitle = 'Nueva contraseña';
        $description = 'Debido a tu solicitud hemos creado una nueva contraseña para tu cuenta de ' . NAME_PROJECT;
        $content = '<h4> Nueva contraseña '. $newPass . ' </h1>';
        $copyright = 'Tripode 2020';
        try {
            ob_end_clean();
            ob_start();
            if (!@require_once($this->pathEmail . '/template.php')) {
                ob_end_clean();
            }
            $message = ob_get_contents();
            ob_end_clean();
        } catch (Exception $ex) {
            exit(['message' => $ex->getMessage()]);
        }
        return $message;
    }

    public function makeContactMessage($fullName, $userEmail, $country, $state, $city, $userMessage)
    {
        $message = '';
        // Add Variables Content
        $title = 'Contáctenos';
        $content = '<h1 class="code">' . $title . '</h1><br>'.'
        <p>
            Nombre: '. $fullName .'
        </p><br>'.'
        <p>
            Correo electrónico: '. $userEmail .'
        </p><br>'.'
        <p>
            País: '. $country .'
        </p><br>';

        $textState='';
        if($state!=''){
            $textState = '
            <p>
                Departamento o Estado: '. $state .'
            </p><br>';
        }
        $textCity='';
        if($city!=''){
            $textCity = '
            <p>
                Ciudad o localidad: '. $city .'
            </p><br>';
        }

        $textMessage = '
        <p>
            Mensaje: '. $userMessage .'
        </p>';
        // Add user state
        $content .= $textState;
        // Add user city
        $content .= $textCity;
        // Add user Message
        $content .= $textMessage;
        // Get Element Base
        try {
            ob_end_clean();
            ob_start();
            if (!@require_once($this->pathEmail . '/template.php')) {
                ob_end_clean();
            }
            $message = ob_get_contents();
            ob_end_clean();
        } catch (Exception $ex) {
            exit(['message' => $ex->getMessage()]);
        }
        return $message;
    }


}
