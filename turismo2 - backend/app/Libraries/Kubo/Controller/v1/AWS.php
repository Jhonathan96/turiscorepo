<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\v1;

use Exception;
use Aws\S3\S3Client;
use Aws\Sns\SnsClient;
use Aws\Exception\AwsException;

class AWS
{
    # Credentials
    // Credentials SNS
    private $region   = 'us-east-1';
    private $key    = '';
    private $secret = '';
    // private $key    = 'AKIAJULTGOJK7DMR35GA';
    // private $secret = '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';

    // SMS
    // private $SMSType = 'Transactional';
    private $SMSType = 'Promotional';
    private $sns;

    public function __construct()
    {
        // SNS
        $params    = [
            'credentials' => [
                'key'    => $this->key,
                'secret' => $this->secret,
            ],
            'region'      => $this->region, // < your aws from SNS Topic region
            'version'     => 'latest',
        ];
        $this->sns = new SnsClient($params);
        self::setSMS();
    }

    public function putVideoToBucket($filePath, $newFileName){
        // $bucket = 'arn:aws:s3:us-east-1:579808242871:accesspoint/tripodecdn';
        $bucket = 'tripodevideos';

        try {
            //options AWS S3
            $s3Options = [
                'version' => 'latest',
                'region' => $this->region,
                'credentials' =>[
                    'key' => 'AKIAISKTPGT4ZOXX527A',
                    'secret' => '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'
                ]
            ];
            //Create a S3Client
            $s3Client = new S3Client($s3Options);
            // $existBuck = $s3Client->createBucket(array('Bucket' => 'tripodevideos'));
            // var_dump($existBuck);

            $result = $s3Client->putObject([
                'Bucket' => $bucket,
                'Key' => $newFileName,
                'SourceFile' => $filePath,
                'ACL' => 'public-read'
            ]);
            return $result->get('ObjectURL');

        } catch (AwsException $e) {
            return $e->getMessage() . "\n";
        }
    }

    public function test()
    {
        $phone = '+573142364808';
        // $phone = '+573112709555';
        $message = 'This is a test TRIPODE';
        self::checkIfNumberExist($phone);
        self::sendSMSAWS($phone, $message);
        // self::getSMS(true);
        // self::listPhones();
    }

    public function SendCodeActivation($phone, $message)
    {
        try{
            self::checkIfNumberExist($phone);
            self::sendSMSAWS($phone, $message);
            return true;
        }catch (Exception $e) {
            return false;
        }
    }

    /**
     * Check If Number Exist
     * @param string $phone 
     * @return exit 
     */
    public function checkIfNumberExist($phone = '+573142364808')
    {
        try {
            $result = $this->sns->checkIfPhoneNumberIsOptedOut([
                'phoneNumber' => $phone,
            ]);
            // (dump($result));
        } catch (AwsException $e) {
            // output error message if fails
            exit($e->getMessage());
            // var_dump($e->getMessage());
        }
    }

    /**
     * Send SMS by AWS
     * @param string $phoneNumber 
     * @param string $message 
     * @return exit 
     */
    public function sendSMSAWS($phoneNumber = '+573142364808', $message = 'Test SMS')
    {
        try {

            $args   = [
                'SenderID'    => 'Kubo',
                'SMSType'     => $this->SMSType,
                'PhoneNumber' => $phoneNumber,
                'Message'     => $message,
            ];
            $result = $this->sns->publish($args);
            // var_dump($result);
        } catch (AwsException $e) {
            // output error message if fails
            exit($e->getMessage());
            // var_dump($e->getMessage());
        }
    }

    public function setSMS($showResult = false)
    {
        try {
            $result = $this->sns->SetSMSAttributes([
                'attributes' => [
                    'DefaultSMSType' => $this->SMSType,
                ],
            ]);
            // Show Result
            if ($showResult) {
                var_dump($result);
            }
        } catch (AwsException $e) {
            // output error message if fails
            exit($e->getMessage());
        }
    }

    public function getSMS($showResult = false)
    {
        try {
            $result = $this->sns->getSMSAttributes([
                'attributes' => [
                    'DefaultSMSType',
                ],
            ]);
            if ($showResult) {
                (dump($result));
            }
        } catch (AwsException $e) {
            // output error message if fails
            exit($e->getMessage());
        }
    }

    public function listPhones()
    {
        try {
            $result = $this->sns->listPhoneNumbersOptedOut();
            (dump($result));
        } catch (AwsException $e) {
            // output error message if fails
            exit($e->getMessage());
        }
    }

    public function getSSHPath()
    {
        return realpath($this->sshFile);
    }
}
