<?php

namespace Kubo\v1;

use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoUploadException;

class Vimeovideo
{
    public function index()
	{
		// return view('welcome_message');
	}
	/**
	 * Instalar composer require vimeo/vimeo-api
	 */
	public function upload($file, $name)
	// public function upload($originalVideoFile, $name)
	{
		$params = ['privacy' => ['view' => 'disable']];
		//Datos reales del app de vimeo para subir el video.
		$lib = new Vimeo(
			'', //ClienteId
			'', //ClienteSecret
			'' //token
		);

		// $file = $this->request->getFile('videoFile');
		// $name = $file->getName();
		// $ext = $file->getClientExtension();
		try {
			// Upload the file and include the video title and description.
			$uri = $lib->upload($file, array(
				'name' => 'Vimeo API SDK test upload tripode',
				'description' => "This video was uploaded by tripode."
			));
			echo 'Esta respuesta ' . $uri;
		
			// Get the metadata response from the upload and log out the Vimeo.com url
			// $video_data = $lib->request($uri . '?fields=link');
			// echo '"' . $name . ' has been uploaded to ' . $video_data['body']['link'] . "\n";
		
			// Make an API call to edit the title and description of the video.
			// $lib->request($uri, array(
			// 	'name' => 'Vimeo API SDK test edit',
			// 	'description' => "This video was edited through the Vimeo API's PHP SDK.",
			// ), 'PATCH');
		
			// echo 'The title and description for ' . $uri . ' has been edited.' . "\n";
		
			// Make an API call to see if the video is finished transcoding.
			// $video_data = $lib->request($uri . '?fields=transcode.status');
			// echo 'The transcode status for ' . $uri . ' is: ' . $video_data['body']['transcode']['status'] . "\n";
		} catch (VimeoUploadException $e) {
			// We may have had an error. We can't resolve it here necessarily, so report it to the user.
			echo 'Error uploading ' . $name . "\n";
			echo 'Server reported: ' . $e->getMessage() . "\n";
		} 

		return '';
	}
}
