<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\v1;

use Exception;
use App\Models\Role;
use App\Models\Users;
use App\Models\UserSubcategories;
use App\Models\Subcategories;
use App\Models\Project;
use App\Models\UserToken;
use App\Models\PasswordReset;
use InvalidArgumentException;
use App\Controllers\BaseController;
use CodeIgniter\Exceptions\ModelException;
use CodeIgniter\HTTP\Exceptions\HTTPException;
use CodeIgniter\Database\Exceptions\DataException;
use CodeIgniter\Database\Exceptions\DatabaseException;
use CodeIgniter\Files\Exceptions\FileNotFoundException;
use CodeIgniter\Validation\Exceptions\ValidationException;

/**
 * Auth
 * @package App\Libraries
 */
class Auth extends BaseController
{

    // Model
    protected $model;
    protected $modelUserSubcategories;

    public function __construct()
    {
        parent::__construct();
        // Init Variables
        $this->model = new Users();
        $this->modelUserSubcategories = new UserSubcategories();
    }

    // Set URLs Base
    public static function setUrls($routes)
    {
        // Base elements
        $routes->group('v' . DEPLOY_VERSION, ['namespace' => 'Kubo\v1'], function ($routes) {

            // Auth Data
            $routes->group('auth', function ($routes) {
                # Onboarding
                $routes->add('signup', 'Auth::signupUser');
                // $routes->add('sendVerificationAccount', 'Auth::sendVerificationAccount');
                $routes->add('login', 'Auth::login');
                // Recovery
                $routes->add('recovery', 'Auth::recovery');
                $routes->add('valid-code', 'Auth::checkRecoveryCode');
                $routes->add('change-password', 'Auth::changePassword');
            });

            // User Data
            $routes->resource('user', [
                'only' => 'index',
                'controller' => 'Auth'
            ]);

            // User
            $routes->group('user', function ($routes) {
                $routes->put('/', 'Auth::update');
                $routes->delete('/', 'Auth::delete');
                $routes->post('logout', 'Auth::logout');
                $routes->get('refresh-auth-token', 'Auth::checkTokenAndGenerateAuthToken');
            });
        });
    }

    public function signupUser(){
        // Get Data
        $request = self::getRequest(false);

        $newUserEmail = $request['email'];
        $newUserPassword = $request['password'];
        // Check Inactive Data
        $user = self::checkActiveUser($request['email']);
        $checkIfExistInactiveEmail = (!checkIsEmpty($user) && is_string($user));
        $ruleEmail = ($checkIfExistInactiveEmail) ? '' : '|is_unique[users.email]';
        // Add Rules
        $validationRules = [
            'fullName' => 'required|regex_match[/(\p{L})(.+)/iu]',
            'email' => 'required' . $ruleEmail,
        ];
        $this->model = self::setValidations($this->model, $validationRules);

        // Check Data
        $validation = self::checkValidation($this->model, $request);
        if (!checkIsEmpty($validation)) {
            // return self::sendResponseError($validation);
            return self::sendResponse([], 150, $validation);
        }

        // Refactor Data
        $uid      = uniqid(sha1(md5(strrev(base64_encode(SHORT_NAME_PROJECT . '-' . $request['email'])))));
        $dataUser = [
            'uid'        => $uid,
            'tries'      => 0,
            'valid'      => 1,  // 0 false, 1 true
            'roleId'     => 2, // onlyUser
            'status'     => 'ACTIVE',
            'created_at' => date('Y-m-d G:i:s', time()),
            'deleted_at' => '0000-00-00 00:00:00',
            'updated_at' => '0000-00-00 00:00:00'
        ];
        $request['accountType'] == 'TC' ? $request['password'] = password_hash($request['password'], PASSWORD_BCRYPT) : $request['password'] = '';
        
        $request = array_merge($dataUser, $request);
        try {
            // Make Object
            $replaceUser = $this->model->replace($request);
            // Get User
            $user = self::checkAndGetSigned($newUserEmail, $newUserPassword);
            if (is_string($user)) {
                // return self::sendResponseError($user);
                return self::sendResponse([], 150, $user);
            }
            $authToken = self::getAuthToken($user, $request);
            // $authToken["user"]->image = 'http://drive.google.com/'.$authToken["user"]->image;
            // create user subcategories inactive
            $allSubcategories = (new Subcategories())->allSubcategories();
            foreach ($allSubcategories as $keySubCt => $valueSubCt) {
                (new UserSubcategories())->createInactiveUserSubcategory($user->uid, $valueSubCt->subcategoryId);
            }

            return self::sendResponse($authToken, 100, "Se ha creado su cuenta correctamente");
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    protected function checkAndGetSigned($email, $password)
    {
        // Refactor Data
        $user = self::checkActiveUser($email);
        // Validations
        if (!checkIsEmpty($user) && is_string($user)) {
            return $user;
        }
        // if (!password_verify($password, $user->password)) {
        //     return ('Contraseña incorrecta.');
        // }
        return $user;
    }

    /**
     * Login User
     * @return string|false|mixed
     * @throws HTTPException
     * @throws DatabaseException
     */
    public function login(){
        // Create Model
        $request = self::getRequest(false);
        // $validationRules = [
        //     'platformId' => 'required',
        //     'version'   => 'required',
        // ];
        // $this->model = self::setValidations($this->model, $validationRules);
        // Check Data
        $validation = self::checkValidation($this->model, $request);
        if (!checkIsEmpty($validation)) {
            // return self::sendResponseError($validation);
            return self::sendResponse([], 150, $validation);
        }
        $dataOrigin = $this->model->where([
            "email" => $request['email'],
            "accountType" => $request['accountType'],
        ])->first();

        if (checkIsEmpty($dataOrigin)) {
            // return self::sendResponseError("El tipo de cuenta o correo son incorrectos.");
            return self::sendResponse([], 150, "El tipo de cuenta o correo son incorrectos.");
        }

        // Get User
        $user = self::checkAndGetUser($request);
        if (is_string($user)) {
            // return self::sendResponseError($user);
            return self::sendResponse([], 150, $user);
        }

        // Add Tries
        // temporary deprecated
        $checkTVU = self::checkTriesAndValidUser($user);
        if (!checkIsEmpty($checkTVU)) {
            // return self::sendResponseError([], 199);
            return (self::sendResponse([], 150));
        }
        
        // Check And generate Auth Token
        $authToken = self::getAuthToken($user, $request);
        // $authToken["user"]->image = 'http://drive.google.com/'.$authToken["user"]->image;
        $userSubcategoriesActive = $this->modelUserSubcategories->getUserSubcategoriesIdsActive($user->uid);
        if (checkIsEmpty($userSubcategoriesActive)) {
            return self::sendResponse([], 140, 'Por favor seleccione subcategorias de su preferencia para la recomendación');
        }
        return (self::sendResponse($authToken));
    }

    protected function checkAndGetUser($request)
    {
        // Refactor Data
        $user = self::checkActiveUser($request['email']);
        // Validations
        if (!checkIsEmpty($user) && is_string($user)) {
            return $user;
        }
        if($request['accountType']=='TC'){
            if (!password_verify($request['password'], $user->password)) {
                // Password Incorrect
                return ('Contraseña incorrecta.');
            }
        }
        return $user;
    }

    protected function checkActiveUser($email, $allObject = false)
    {
        //TODO: Roles Joins
        $user = $this->model->where(['email' => $email, 'status' => 'ACTIVE'])->first();
        if (checkIsEmpty($user)) {
            return ('El correo electrónico NO existe en la base de datos.');
        }
        $roleId = $user->roleId;
        if($allObject){
            $user = self::getObject($user->uid, new Users());
        }
        $user->role =  self::getObject($roleId, new Role());
        return $user;
    }


    protected function checkTriesAndValidUser($user)
    {
        $uid   = $user->uid;
        $count = (int) $user->tries;
        $email = $user->email;
        // Validate User
        if (((bool) $user->valid) === false) {
            return ('Es necesario que actives tu cuenta. te enviamos un nuevo código a tú teléfono.');
        }
    }

    protected function getAuthToken($user, $request)
    {
        $uid = $user->uid;
        // Get Access Token
        $accessToken = self::getHeaderAccessToken();
        if (strlen($accessToken) <= 10) {
            // return self::sendResponseError('Invalid Auth Token', CODE_RESPONSE_ERROR);
            return self::sendResponse([], 150, 'Invalid Auth Token');
            exit();
        }
        $data = ['tries' => 0];
        try {
            $this->model->update($uid, $data);
        } catch (Exception $ex) {
            // return (self::sendResponseError($ex->getMessage()));
            return self::sendResponse([], 150, $ex->getMessage());
        }
        // Check Access Token and Set AuthToken
        $loginUser['user'] = $user = self::checkActiveUser($user->email, true);
        unset($user->roleId);
        $loginUser['authToken'] = self::generateAuthToken($accessToken, $user, $request);
        return $loginUser;
    }

    /**
     * Logout User (Remove Auth)
     * @return string|false|mixed|void
     * @throws HTTPException
     */
    public function logout()
    {
        // Get UID
        $dataUser = self::getDataUser();
        // Check Token
        if (checkIsEmpty($dataUser)) {
            // return (self::sendResponseError('Error with the Auth Token - You are already Logout.'));
            return self::sendResponse([], 150, 'Error with the Auth Token - You are already Logout.');
        }

        self::removeOAuth($dataUser);
        return (self::sendResponse('Logout Success'));
    }

    protected function removeOAuth($dataUser, $all = true)
    {
        // Get Variables
        $uid = $dataUser->uid;
        $dataUserAuth = [
            'uid'       => $uid
        ];
        if ($all) {
            $dataUserComplements = [
                'platformId' => $dataUser->platformId,
                'version'   => $dataUser->version
            ];
            $dataUserAuth = array_merge($dataUserAuth, $dataUserComplements);
        }
        // Remove OAuth
        $userTokens = new UserToken();
        $userTokens = $userTokens->where(['uid' => $uid]);
        if (!checkIsEmpty($userTokens)) {
            // Flag Remove
            $userTokens->where($dataUserAuth)->remove();
            $userTokens->where($dataUserAuth)->delete();
        }

        // Flag Remove Password Reset
        $passwordReset = new PasswordReset();
        $passwordReset->where(['uid' => $uid])->remove();
        $passwordReset->where(['uid' => $uid])->delete();

        // Flag Remove Project
        $project = new Project();
        $project->where(['uid' => $uid])->remove();
        $project->where(['uid' => $uid])->delete();
    }


    /**
     * Send Email - Recovery Password
     * @return string|false|mixed
     * @throws HTTPException
     */
    public function recovery()
    {
        // Get Model
        $request = self::getRequest(false);

        // Check Data
        $validation = self::checkValidation($this->model, $request);
        if (!checkIsEmpty($validation)) {
            // return self::sendResponseError($validation);
            return self::sendResponse([], 150, $validation);
        }
        // Get data
        $user = self::checkActiveUser($request['email']);
        if (!checkIsEmpty($user) && is_string($user)) {
            // return self::sendResponseError($user);
            return self::sendResponse([], 150, $user);
        }
        self::sendEmail($user->email, $user->uid, 'recovery');
        return ($this->sendResponse([], 100, 'Mensaje de verificación enviado'));
    }

    /**
     * Check Recovery Code
     * @return string|false|mixed
     * @throws HTTPException
     */
    public function checkRecoveryCode()
    {
        // Get Request and set new Model
        $request = self::getRequest(false);
        $this->model = new PasswordReset();

        $validationRules = [
            'code' => 'required|numeric',
        ];
        $this->model = self::setValidations($this->model, $validationRules);
        // Check Data
        $validation = self::checkValidation($this->model, $request);

        if (!checkIsEmpty($validation)) {
            // return self::sendResponseError($validation);
            return self::sendResponse([], 150, $validation);
        }

        $checkCode = self::checkCode($request['code'], $request['email']);
        if ((bool) $checkCode['check']) {
            return self::sendResponse($checkCode);
        } else {
            // return self::sendResponseError($checkCode);
            return self::sendResponse([], 150, $checkCode);
        }
    }

    protected function checkCode($code, $email)
    {
        $model = new PasswordReset();
        $userModel = new Users();
        
        $userData = $userModel->where(["email" => $email])->first();
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('El correo electrónico NO existe en la base de datos.');
            return self::sendResponse([], 150, 'El correo electrónico NO existe en la base de datos.');
        }

        $passwordCheck = $model->where(['code' => $code,'uid' => $userData->uid]);
        if (count($passwordCheck->find()) > 0) {
            return (['check' => true, 'message' => 'El código es valido', 'uid' => $userData->uid]);
        } else {
            return (['check' => false, 'message' => 'El código es invalido']);
        }
    }

    /**
     * Change Password
     * @return string|false|mixed
     * @throws HTTPException
     * @throws DatabaseException
     */
    public function changePassword()
    {
        // Get Model
        $request = self::getRequest(false);
        $this->model = new PasswordReset();

        $validationRules = [
            'code'     => 'required|numeric',
            //'password' => 'required|min_length[8]|regex_match[/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\W\_])[A-Za-z\d\W\_]{8,}$/]',
            'password' => 'required|min_length[8]'
        ];
        $this->model = self::setValidations($this->model, $validationRules);

        // Check Data
        $validation = self::checkValidation($this->model, $request);

        if (!checkIsEmpty($validation)) {
            // return self::sendResponseError($validation);
            return self::sendResponse([], 150, $validation);
        }

        $checkCode = self::checkCode($request['code'], $request['email']);
        if ((bool) $checkCode['check'] === false) {
            // return self::sendResponseError($checkCode);
            return self::sendResponse([], 150, $checkCode);
        }
        // Get UID by Code
        $uid      = $this->model->where(['code' => $request['code'], 'uid' => $checkCode['uid']])->first()->uid;
        // Check User
        $user     = new Users();
        if (count($user->where(['uid' => $uid])->find()) < 1) {
            // return (self::sendResponseError('User not found'));
            return self::sendResponse([], 150, 'User not found');
        }
        try {
            $user->update($uid, [
                'tries'    => 0,
                'password' => password_hash($request['password'], PASSWORD_BCRYPT),
            ]);
            $passwordReset = new PasswordReset();
            $passwordReset->where(['uid' => $uid])->remove();
            $passwordReset->where(['uid' => $uid])->delete();
            return ($this->sendResponse([], 100, 'La contraseña ha sido cambiada correctamente'));
        } catch (Exception $ex) {
            // return (self::sendResponseError($ex->getMessage()));
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    protected function sendEmail($email, $uid, $option)
    {
        // Class Email
        if (file_exists(APPPATH . 'Libraries/Kubo/Controller/v1/Email.php')) {
            require_once APPPATH . 'Libraries/Kubo/Controller/v1/Email.php';
        }
        $emailController = new Email();
        $subject = '';
        $messageValidate = '';
        $user = $this->model->select('fullName as name')->find($uid);
        switch ($option) {
            case 'verified':
                $subject =  'El usuario ya esta verificado anteriormente';
                return $emailController->makeVerification('El usuario ya esta verificado anteriormente', $email, $user->name);
                break;
            case 'register':
                $subject =  'Resgistro';
                $messageValidate = $emailController->makeRegisterUser($email, $user->name);
                break;
            case 'thanks':
                $subject =  'Gracias';
                return $emailController->makeVerification('Gracias por verficar su cuenta', $email, $user->name);
                break;
            case 'recovery':
                $subject =  'Recuperar tu cuenta';
                $code = self::makeAndSendRecoveryCode($uid);
                $messageValidate = $emailController->makeRecoveryEmail($code);
                break;
            case 'reset':
                $subject = 'Restablecer contraseña';
                $messageValidate = $emailController->makeResetPassword('4trtertet5645');
                break;
        }
        // Validate Email
        $subject         .= ' - ' . NAME_PROJECT;
        $emailController->send($email, $subject, $messageValidate);
        // Get Info
        return [
            'uid'     => $uid,
            'message' => 'Correo enviado',
        ];
    }

    protected function sendEmailContact($to, $fullName, $userEmail, $country, $state, $city, $userMessage)
    {
        // Class Email
        if (file_exists(APPPATH . 'Libraries/Kubo/Controller/v1/Email.php')) {
            require_once APPPATH . 'Libraries/Kubo/Controller/v1/Email.php';
        }
        $emailController = new Email();
        $subject = 'Nuevo mensaje';
        $messageValidate = $emailController->makeContactMessage($fullName, $userEmail, $country, $state, $city, $userMessage);
        // Validate Email
        $subject         .= ' - ' . NAME_PROJECT;
        $emailController->send($to, $subject, $messageValidate);
        // Get Info
        return [
            'data'     => 'send',
            'message' => 'Correo enviado',
        ];
    }

    private function makeAndSendRecoveryCode($uid)
    {
        $code    = rand(1000, 9999);
        // Add to DB
        $passwordReset = new PasswordReset();
        $passwordCheck = $passwordReset->where(['uid' => $uid]);
        if (count($passwordCheck->find()) > 0) {
            // Flag Remove
            $passwordReset->where(['uid' => $uid])->remove();
            $passwordReset->where(['uid' => $uid])->delete();
        }
        try {
            $data = [
                'uid'    => $uid,
                'code'   => $code,
                'status' => 1,
            ];
            $passwordReset->insert($data);
            return $code;
        } catch (Exception $ex) {
            // return (self::sendResponseError($ex->getMessage()));
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /* Crud */
    /**
     * Get User Info by Auth Token
     * @return string|false|mixed
     * @throws HTTPException
     */
    public function index()
    {
        // Get User
        $dataUser = self::getDataUser();
        $uid = $dataUser->uid;
        // Get data
        $userTemp = self::checkActiveUser($dataUser->email);
        if (!checkIsEmpty($userTemp) && is_string($userTemp)) {
            // return self::sendResponseError($userTemp);
            return self::sendResponse([], 150, $userTemp);
        }

        // role
        unset($dataUser);
        unset($userTemp);
        return self::sendResponse(self::getObject($uid));
    }

    /**
     * Update the specified user in the DB.
     * @return string|false|mixed
     * @throws HTTPException
     * @throws DataException
     * @throws ModelException
     * @throws InvalidArgumentException
     * @throws DatabaseException
     */
    public function update()
    {

        // Get User
        $dataUser = self::getDataUser();
        $uid = $dataUser->uid;
        // Get data
        $userTemp = self::checkActiveUser($dataUser->email);
        if (!checkIsEmpty($userTemp) && is_string($userTemp)) {
            // return self::sendResponseError($userTemp);
            return self::sendResponse([], 150, $userTemp);
        }
        // Create Model
        $request = self::getRequest(false);
        // Check Data
        $validation = self::checkValidation($this->model, $request);

        if (!checkIsEmpty($validation)) {
            // return self::sendResponseError($validation);
            return self::sendResponse([], 150, $validation);
        }
        // Hash Password
        $request['password'] = password_hash($request['password'], PASSWORD_BCRYPT);
        $this->model->update($uid, $request);
        return self::sendResponse(['message' => 'El usuario ha sido actualizado correctamente', 'user' => self::getObject($uid)]);
    }

    /**
     * Remove the specified resource from storage.
     */
    /**
     * Remove/Delete an User specific
     * @return string|false|mixed
     * @throws ModelException
     * @throws InvalidArgumentException
     * @throws DatabaseException
     * @throws DataException
     * @throws HTTPException
     * @throws ValidationException
     */
    public function delete()
    {
        $uid = self::getDataUser()->uid;
        $data = $this->model->find($uid);
        if (checkIsEmpty($data)) {
            // return self::sendResponseError("No se encuentra el usuario con uid {$uid}");
            return self::sendResponse([], 150, "No se encuentra el usuario con uid {$uid}");
        }
        self::removeOAuth($data, false);
        $model = $this->model->where(['uid' => $uid, 'status' => 'INACTIVE'])->find();
        if (!checkIsEmpty($model)) {
            // return self::sendResponseError("Ya se eliminio el elemento con uid {$uid}");
            return self::sendResponse([], 150, "Ya se eliminio el elemento con uid {$uid}");
        }
        // Delete Object
        $this->model->update($uid, ['status' => 'INACTIVE', 'deleted_at' =>  date('Y-m-d h:i:s', time())]);
        return self::sendResponse('Elemento eliminado');
    }

    public function checkTokenAndGenerateAuthToken()
    {
        // Get User
        $dataUser = self::getDataUser();
        // Get data
        $user = self::checkActiveUser($dataUser->email);
        if (!checkIsEmpty($user) && is_string($user)) {
            // return self::sendResponseError($user);
            return self::sendResponse([], 150, $user);
        }
        $accessToken = self::generateAccessToken(false);
        $token       = self::generateAuthToken($accessToken, $user, [
            'platformId' => $dataUser->platformId,
            'version' => $dataUser->version
        ]);
        return (self::sendResponse(['authToken' => $token]));
    }
}
