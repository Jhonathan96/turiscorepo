<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\v1;

use Kreait\Firebase\Exception\DatabaseException;
use Kreait\Firebase\Exception\Messaging\InvalidArgument;
use Kreait\Firebase\Exception\InvalidArgumentException;
use Kreait\Firebase\Exception\RuntimeException;
use Kreait\Firebase\Exception\LogicException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Messaging\CloudMessage;

/**
 * Firebase
 * @package App\Libraries
 */
class Firebase
{
    private $firebase;
    private $messaging;
    # Credentials
    // JSON
    private $firebaseKeyJson =  APPPATH . "/Libraries/Kubo/cert/firebase.json"; // APPPATH . '/Libraries/Kubo/cert/firebase.json'
    // Database
    private $firebaseDatabaseUri = '';
    // Keys
    private $firebaseServerKey = '';

    public function __construct()
    {
        try {
            $serviceAccount = ServiceAccount::fromJsonFile(self::getFirebaseKeyJson());
            $this->firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri($this->firebaseDatabaseUri)
                ->create();
            $this->messaging = self::getFirebase()->getMessaging();
        } catch (\Exception $ex) {
            exit($ex->getMessage());
        }
    }

    public function testing()
    {
        $pushToken = '';
        $arrayPushTokens = [$pushToken,$pushToken,$pushToken];
        // Multiple Push
        self::sentToAll('Nueva notificación', 'Test - Multiple',$arrayPushTokens);
        // Single Push
        self::sendDeviceNotification('Nueva notificación', 'Test - Simple',$pushToken);
        // Key Push
        self::sendNotificationCurl($pushToken, 'Nueva notificación', 'Test - CURL KEY');
    }

    /**
     * Send Message to Channel
     * @param mixed $message 
     * @param string $chanel 
     * @return void 
     * @throws RuntimeException 
     * @throws LogicException 
     * @throws InvalidArgumentException 
     * @throws DatabaseException 
     */
    public function sendMessagesToChanel($message, $chanel = 'notifications')
    {
        $database = self::getFirebase()->getDatabase();
        $newPost = $database->getReference($chanel)
            ->push($message);
    }


    /**
     * Send Device Notification (Single)
     * @param mixed $title 
     * @param mixed $body 
     * @param mixed $token 
     * @param array $data 
     * @return void 
     * @throws InvalidArgument 
     * @throws InvalidArgumentException 
     */
    public function sendDeviceNotification($title, $body, $token, $data = [])
    {
        $message = self::structureMessage($title, $body, $token, $data);
        self::getMessaging()->send($message);
    }

    protected function structureMessage($title, $body, $token = '', $data = [])
    {
        $structure = [
            'notification' => [
                // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#notification
                'title' => $title,
                'body' => $body,
                // 'image' => 'http://lorempixel.com/400/200/',
            ],
            'data' => $data,
            'android' => [
                // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#androidconfig
                'ttl' => '3600s',
                'priority' => 'high',
                'notification' => [
                    'title' => $title,
                    'body' => $body,
                    'icon' => 'stock_ticker_update',
                    'color' => '#f45342',
                ],
            ],
            'apns' => [
                // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#apnsconfig
                'headers' => [
                    'apns-priority' => '10',
                ],
                'payload' => [
                    'aps' => [
                        'alert' => [
                            'title' => $title,
                            'body' => $body,
                        ],
                        'badge' => 42,
                    ],
                ],
            ],
            'webpush' => [
                // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#webpushconfig
                'notification' => [
                    'title' => $title,
                    'body' => $body,
                    // 'icon' => 'https://my-server/icon.png',
                ],
            ],
            'fcm_options' => [
                // https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages#fcmoptions
                'analytics_label' => 'some-analytics-label'
            ]

        ];

        if (isset($token)) {
            $token = [
                'token' => $token
            ];
            $structure = array_merge($structure,  $token);
        }
        $message = CloudMessage::fromArray($structure);
        return $message;
    }

    /**
     * Send to specifics Devices Tokens (ALL Devices)
     * @param mixed $title 
     * @param mixed $body 
     * @param mixed $deviceTokens 
     * @return void 
     * @throws InvalidArgument 
     * @throws InvalidArgumentException 
     */
    public function sentToAll($title, $body, $deviceTokens)
    {
        /** @var Kreait\Firebase\Messaging\MulticastSendReport $report **/
        $report = self::getMessaging()->sendMulticast(self::structureMessage($title, $body), $deviceTokens);

        // echo 'Successful sends: ' . $report->successes()->count() . PHP_EOL;
        // echo 'Failed sends: ' . $report->failures()->count() . PHP_EOL;

        // if ($report->hasFailures()) {
        //     foreach ($report->failures()->getItems() as $failure) {
        //         echo $failure->error()->getMessage() . PHP_EOL;
        //     }
        // }
    }

    protected function getFirebase()
    {
        return $this->firebase;
    }
    protected function getMessaging()
    {
        return $this->messaging;
    }

    protected function getFirebaseKeyJson()
    {
        return realpath($this->firebaseKeyJson);
    }

    /** Special Methods **/
    /**
     * Send push notification FIREBASE by Firebase Key
     * @param mixed $pushToken Push Token User
     * @param mixed $title Title main message
     * @param mixed $body Text message
     * @param array $data (Optional) Send const ACTIONS to mobile/desk
     * @param mixed $firebaseServerKey Firebase key
     * 
     * @author Miguel Ángel <mtorres@kubo.co>
     * 
     * @return (true|string)[]|(false|string)[] 
     */

    public function sendNotificationCurl($pushToken, $title, $body, $data = [])
    {
        $pathProcess = 'https://fcm.googleapis.com/fcm/send';
        $fields = [
            'to' => $pushToken,
            'notification' => [
                'title' => $title,
                'body' => $body,
                "subtitle" => "Kubo S.A.S.",
                'content_available' => true,
                'priority' => 'high',
                'sound' => 'default',
                'icon' => 'ic_push'
            ],
            "apns" => [
                "alert" => $title,
                "badge" => 9,
                "sound" => "bingbong.aiff"
            ]
        ];

        if (isset($data) && !empty($data)) {
            // Send personal Data
            $fields['data'] = $data;
        }

        $headers = array(
            'Authorization:key=' . $this->firebaseServerKey,
            'Content-Type:application/json'
        );

        // clear the old headers
        header_remove();
        // Specify the type of data
        header('Content-Type: application/json');
        // Curl Process
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $pathProcess);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($curl);
        curl_close($curl);

        // Show Result 
        $response = [];
        $dataTemp = json_decode($result, true);
        if ($dataTemp['success']) {
            $response =  [
                'success' => true,
                'message' => 'Notification Send'
            ];
        } else {
            $response =  [
                'success' => false,
                'message' => 'The message could not be sent'
            ];
        }
        return $response;
    }
}
