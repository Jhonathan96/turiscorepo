<?php

// namespace App\Libraries\Kubo;
// namespace App\Libraries\Kubo\Controller\Core;
namespace Kubo\v1;

use App\Models\City;
use App\Models\State;
use App\Models\Country;
use App\Controllers\BaseController;

/**
 * Location
 * @package App\Libraries
 */
class Location extends BaseController
{
    // Flag
    protected $model;

    // Set URLs Base
    public static function setUrls($routes)
    {
        // Base elements
        $routes->group('v' . DEPLOY_VERSION, ['namespace' => 'Kubo\v1'], function ($routes) {
            // Location
            $routes->group('location', function ($routes) {
                $routes->get('countries', 'Location::getCountries');
                $routes->get('country/(:num)', 'Location::getStatesOfCountry');

                $routes->get('states', 'Location::getStates');
                $routes->get('state-cities', 'Location::getStatesWithCities');
                $routes->get('state/(:num)', 'Location::getCitiesOfState');

                $routes->get('cities', 'Location::getCities');
                $routes->get('cities/(:num)', 'Location::getCitiesFrom');
                $routes->get('cities-state', 'Location::getCitiesWithState');
                // Search City
                $routes->get('search-city/(:any)', 'Location::searchCity');
            });
        });
    }

    public function getCountries()
    {
        // Create Model 
        $this->model = new Country();
        // first get Colombia
        $response = $this->model
            ->where('name', 'Colombia')
            ->where(['status !=' => 'INACTIVE'])
            ->findAll();

        $response = array_merge(
            $response,
            $this->model
                ->where('name !=', 'Colombia')
                ->where(['status !=' => 'INACTIVE'])
                ->orderBy('name', 'ASC')
                ->findAll()
        );
        // $data = file_get_contents(APPPATH . '/Libraries/Kubo/complements/countries.json');
        // $data = json_decode($data, true);
        // $callingCode = file_get_contents(APPPATH . '/Libraries/Kubo/complements/callingCodes.json');
        // $callingCode = json_decode($callingCode, true);
        if (checkIsEmpty($response)) {
            return self::sendResponseError('La lista de paises esta vacia');
        }
        return self::sendResponse($response);
    }

    public function getStatesOfCountry()
    {
        $countryId = ($this->uri->getSegment(4));
        $this->model     = new State();
        $response = $this->model
            ->where('name', 'Bogotá D.C.')
            ->where('status', 'ACTIVE')
            ->where('countryId', $countryId)
            ->findAll();

        $response  = array_merge(
            $response,
            $this->model->where('countryId', $countryId)->orderBy('name', 'ASC')->findAll()
        );
        if (checkIsEmpty($response)) {
            return self::sendResponseError('La lista de estados del país seleccionado esta vacia');
        }
        return self::sendResponse($response);
    }

    public function getStates()
    {
        $data = file_get_contents(APPPATH . '/Libraries/Kubo/complements/states.json');
        $response = json_decode($data, true);
        // $this->model     = new State();
        // $response    = $this->model->orderBy('name', 'ASC')->findAll();
        if (checkIsEmpty($response)) {
            return self::sendResponseError('La lista de estados esta vacia');
        }
        return self::sendResponse($response);
    }

    public function getCities()
    {
        // $this->model = new City();
        // $response = $this->model->orderBy('name', 'ASC')->findAll();
        $data = file_get_contents(APPPATH . '/Libraries/Kubo/complements/cities.json');
        $response = json_decode($data, true);
        if (checkIsEmpty($response)) {
            return self::sendResponseError('La lista de ciudades esta vacia');
        }
        return self::sendResponse($response);
    }

    public function getCitiesWithState()
    {
        $department = new State();
        $cities    = new City();
        $response  = $cities->orderBy('name', 'ASC')->findAll();
        foreach ($response as $key => $value) {
            $state                   = $department->find($value->stateId);
            $response[$key]->state = $state;
            unset($response[$key]->stateId);
            // Remove Dirty Data
        }
        return (self::sendResponse($response));
    }

    public function getStatesWithCities()
    {
        $department = new State();
        $city      = new City();
        $response  = $department->findAll();
        $data = [];
        $count = 0;
        foreach ($response as $key => $value) {
            $data[$count] = $value;
            $cities = $city->where('stateId', $value->stateId)->orderBy('name', 'ASC')->findAll();
            $cities     = self::removeDirtyData($cities);
            $data[$count]->cities = $cities;
            $count++;
        }
        return (self::sendResponse($data));
    }


    public function getCitiesOfState()
    {
        $stateId = ($this->uri->getSegment(4));
        $this->model         = new City();
        $response       = $this->model->where('stateId', $stateId)->orderBy('name', 'ASC')->findAll();
        return (self::sendResponse($response));
    }

    public function searchCity()
    {
        $nameCity = ($this->uri->getSegment(4));
        $this->model         = new City();
        $response       = $this->model->like('name', $nameCity)->orderBy('name', 'ASC')->findAll();
        return (self::sendResponse($response));
    }

    // Get Specific Cities
    public function getCitiesFrom()
    {
        // Init
        $response = [];
        $state    = new State();
        $city     = new City();
        // ID
        $countryId = ($this->uri->getSegment(4));
        // States
        $states = $state->where('countryId', $countryId)->orderBy('name', 'ASC')->findAll();

        foreach ($states as $key => $value) {
            // $cities = $city->where('stateId', $value['stateId'])->orderBy('name', 'ASC')->findAll();
            // foreach ($cities as $keyCities => $value) {
            //     if ($countryId === '3686110') {
            //         switch ($value['cityId']) {
            //             case '3687926':
            //             case '9609540':
            //             case '3674954':
            //             case '3689152':
            //             case '3687247':
            //             case '3666610':
            //                 array_push($response, $value);
            //                 break;
            //         }
            //     } else {
                    array_push($response, $value);
            //     }
            // }
        }
        // Order
        $sortArray = [];
        foreach ($response as $person) {
            foreach ($person as $key => $value) {
                if (!isset($sortArray[$key])) {
                    $sortArray[$key] = [];
                }
                $sortArray[$key][] = $value;
            }
        }
        $orderby = 'name'; //change this to whatever key you want from the array
        array_multisort($sortArray[$orderby], SORT_ASC, $response);
        return (self::sendResponse($response));
    }
}
