<?php
namespace App\Libraries;

class CollaborativeFilter{

    public function similarityDistance($matrixSites, $uid, $uidOtherPerson){
        $similar = array();
        $sum = 0;
    
        foreach($matrixSites[$uid] as $key => $value){ // recorriendo las calificaciones de sitios del usuario objetivo
            // se forma un arreglo para ver cuantos Id existen del sitio por parte del otro usuario
            // si existe la llave (id del sitio) en el otro usuario, asigna 1 para ese key (id)
            if(array_key_exists($key, $matrixSites[$uidOtherPerson])){
                $similar[$key] = 1;
            }
        }
        // si no existe alguna de las llaves para el otro usuario (es decir que el array este vacio), 
        // devuelve 0 en la similitud
        if(count($similar) == 0){
            return 0;
        }
        $countItemsToDivide = 0; // contador para dividir las distancias y sacar promedio
        foreach($matrixSites[$uid] as $key => $value){// recorriendo las calificaciones de sitios del usuario objetivo
            // si existe la llave (id de sitio) en las calificaciones del otro usuario calcula la similitud
            if(array_key_exists($key, $matrixSites[$uidOtherPerson])){
                // se resta la calificacion del usuario con la del otro usuario para la misma llave (id de sitio)
                $subtraction = ($value - $matrixSites[$uidOtherPerson][$key]);
                // hace sumatoria del calculo de distancia euclidiana.
                // esta sumaria se hace colocando exponencial de 2, para tener valores positivos y raiz cuadrada
                $sum =$sum + sqrt(pow($subtraction, 2));
                $countItemsToDivide = $countItemsToDivide+1; // aumento al contador para dividir y posteriormente obtener promedio
            }
        }
        // se obtiene el promedio total de la distancia dividiendo entre el número
        // o cantidad de sitios (items calificados)
        $similarityAvg = ($sum)/$countItemsToDivide;
        // para la similitud ahora se obtiene valores entre 0 y 1
        // 1 es una similitud más proxima y 0 es que la similitud no existe
        $similarity = 1/(1 + $similarityAvg);
        return  $similarity;
    }

    public function getRecommendations($matrixSimilarity, $matrixSites, $uid){
        $totalSites = array();
        $qualificationsQuantities = array();
        $ranks = array();
        $similarity = 0;
        
        // echo '<br><br><strong>::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        // <h1>SIMILITUD DE USUARIOS</h1>
        // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::</strong><br><br>';
        foreach($matrixSites as $uidOtherPerson => $values){
            
            ($uidOtherPerson !== $uid) ? $similarity = $this->similarityDistance($matrixSimilarity, $uid, $uidOtherPerson) : $similarity = 0;
            if($similarity >= 0.8 && count($matrixSites[$uidOtherPerson])>0){
                // echo 'USER '. $uidOtherPerson.' ::=>:: '. $similarity."\n";
                //recorriendo las calificaciones de los sitios de cada persona
                foreach($matrixSites[$uidOtherPerson] as $siteId => $qualification){

                    // condicional, entra a realizar la sumatoria
                    // Si el id del sitio no esta dentro de las calificaciones del usuario objetivo
                    if(!array_key_exists($siteId, $matrixSites[$uid])){
                        // si no esta el id de sitio en el arreglo de totalSites, 
                        // se inicializa la sumatoria de calificaciones para ese sitio en 0
                        if(!array_key_exists($siteId, $totalSites)) {
                            $totalSites[$siteId] = 0;
                        }
                        // realiza la sumatoria de las calificaciones multiplicandola 
                        // por la similitud del usuario (usuario segun similitud)
                        $totalSites[$siteId] += $matrixSites[$uidOtherPerson][$siteId] * $similarity;
                        
                        // si no esta el id del sitio en el arreglo de contador para posteriormente dividir
                        // y obtener primedio, se inicializa el contador para ese sitio en 0
                        if(!array_key_exists($siteId, $qualificationsQuantities)) {
                            $qualificationsQuantities[$siteId] = 0;
                        }
                        // se realiza el aumento del contador para el sitio
                        $qualificationsQuantities[$siteId] += 1;
                    }
                }
            }
        }
        foreach($totalSites as $siteId => $qualification){
            // se calcula la calificacion promedio de acuerdo a la sumatoria de todas las calificaciones
            // y se divide en la cantidad de calificaciones
            $ranks[$siteId] = $qualification / $qualificationsQuantities[$siteId];
        }
        array_multisort($ranks, SORT_DESC); //ordenando los sitios por calificacion de forma descendente
        return $ranks;
    }
}

?>