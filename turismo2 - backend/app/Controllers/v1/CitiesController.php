<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Cities;
use App\Controllers\BaseController;

class CitiesController extends BaseController
{
    private $modelCities;

    public function __construct()
    {
        parent::__construct();
        $this->modelCities = new Cities();
    }

    /**
    * @api {POST} cities/getAllCities/ getAllCities
    * @apiVersion 1.0.0
    * @apiName getAllCities
    * @apiGroup cities
    * @apiDescription get all cities with total rows
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    * @apiParam {String} status status rows
    * @apiParam {String} cityName filter city name
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getAllCities(){

        $request = self::getRequest(false);

        $limit = $request['limit'];
        $offset = $request['offset'];
        $statusReq = $request['status'];
        $cityNameReq = $request['cityName'];
        
        $statusReq !== '' && $statusReq !== null ? $status = $statusReq : $status = 'ACTIVE';
        $cityNameReq !== '' && $cityNameReq !== null ? $cityNameLike = $cityNameReq : $cityNameLike = '';

        $cities = $this->modelCities
        ->where(["cities.status" => $status])
        ->like('cities.cityName', $cityNameLike, 'both')
        ->orderBy('cityName','ASC')
        ->findAll($limit, $offset);

        if (checkIsEmpty($cities)) {
            // return self::sendResponseError('No hay ciudades.');
            return self::sendResponse([], 150, 'No hay ciudades.');
        }

        $totalRows = $this->modelCities
        ->where(["cities.status" => $status])
        ->like('cities.cityName', $cityNameLike, 'both')
        ->findAll();

        return $this->sendResponse(
            ["cities" => $cities, "total" => count($totalRows)],
            100,
            'Operación exitosa'
        );
    }

}