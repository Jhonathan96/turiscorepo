<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Users;
use App\Models\UserFriendShip;
use App\Controllers\BaseController;

class UserFriendshipController extends BaseController
{
    private $modelUserFriendship;
    private $modelUsers;

    public function __construct()
    {
        parent::__construct();
        $this->modelUserFriendship = new UserFriendShip();
        $this->modelUsers = new Users();
    }

    /**
    * @api {POST} userFriendship/sendFriendRequest/ sendFriendRequest
    * @apiVersion 1.0.0
    * @apiName sendFriendRequest
    * @apiGroup userFriendship
    * @apiDescription send friend request
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {String} otherUid with status per user
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function sendFriendRequest(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];
        $otherUid = $request['otherUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario que envía la solicitud esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario que envía la solicitud esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('El usuario que envía la solicitud no existe');
            return self::sendResponse([], 150, 'El usuario que envía la solicitud no existe');
        }

        if (checkIsEmpty($otherUid)) {
            // return self::sendResponseError('El identificador del usuario que recibe la solicitud esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario que recibe la solicitud esta vacío');
        }
        $toUserData = $this->modelUsers->getUserDataByUid($otherUid);
        if (checkIsEmpty($toUserData)) {
            // return self::sendResponseError('El usuario que recibe la solicitud no existe');
            return self::sendResponse([], 150, 'El usuario que recibe la solicitud no existe');
        }
        if ($myUid===$otherUid) {
            // return self::sendResponseError('los dos identificadores son el mismo');
            return self::sendResponse([], 150, 'los dos identificadores son el mismo');
        }

        try{

            $userRelationFriendship = $this->modelUserFriendship->getRelationFriendship($myUid, $otherUid);
            if ($userRelationFriendship!==null) {
                switch ($userRelationFriendship->friendshipStatus) {
                    case 'BLOCKED':
                        // return self::sendResponseError('No puede enviar la solicitud porque existe un bloqueo');
                        return self::sendResponse([], 150, 'No puede enviar la solicitud porque existe un bloqueo');
                        break;
                    case 'REQUEST':
                        // return self::sendResponseError('No puede enviar la solicitud porque existe una solicitud actualmente');
                        return self::sendResponse([], 150, 'No puede enviar la solicitud porque existe una solicitud actualmente');
                        break;
                    case 'FRIEND':
                        // return self::sendResponseError('No puede enviar la solicitud porque ya tienen amistad');
                        return self::sendResponse([], 150, 'No puede enviar la solicitud porque ya tienen amistad');
                        break;
                }
            }

            $sendRequestFriendship = $this->modelUserFriendship->createRelationFriendship($myUid, $otherUid, "REQUEST");

            return $this->sendResponse(
                [
                    "userFriendshipId" => $sendRequestFriendship.""
                ],
                100, 
                'Solicitud de amistad enviada'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/cancelFriendRequest/ cancelFriendRequest
    * @apiVersion 1.0.0
    * @apiName cancelFriendRequest
    * @apiGroup userFriendship
    * @apiDescription cancel friend request
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {String} otherUid with status per user
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function cancelFriendRequest(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];
        $otherUid = $request['otherUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario que envió la cancelación de amistad esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario que envió la cancelación de amistad esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('El usuario que envió la cancelación de amistad no existe');
            return self::sendResponse([], 150, 'El usuario que envió la cancelación de amistad no existe');
        }

        if (checkIsEmpty($otherUid)) {
            // return self::sendResponseError('El identificador del usuario a cancelar la solicitud esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario a cancelar la solicitud esta vacío');
        }
        $toUserData = $this->modelUsers->getUserDataByUid($otherUid);
        if (checkIsEmpty($toUserData)) {
            // return self::sendResponseError('El usuario a cancelar la solicitud no existe');
            return self::sendResponse([], 150, 'El usuario a cancelar la solicitud no existe');
        }
        if ($myUid===$otherUid) {
            // return self::sendResponseError('los dos identificadores son el mismo');
            return self::sendResponse([], 150, 'los dos identificadores son el mismo');
        }

        try{

            $userRelationFriendship = $this->modelUserFriendship->getRelationFriendship($myUid, $otherUid);
            if($userRelationFriendship===null){
                // return self::sendResponseError('No puede cancelar porque no existe una solicitud de amistad');
                return self::sendResponse([], 150, 'No puede cancelar porque no existe una solicitud de amistad');
            }
            switch ($userRelationFriendship->friendshipStatus) {
                case 'BLOCKED':
                    // return self::sendResponseError('No puede cancelar porque no existe una solicitud de amistad y existe un bloqueo');
                    return self::sendResponse([], 150, 'No puede cancelar porque no existe una solicitud de amistad y existe un bloqueo');
                    break;
                case 'REQUEST':
                    $this->modelUserFriendship->deleteRelationFriendship($userRelationFriendship->userFriendshipId);
                    return $this->sendResponse(
                        [
                            "userFriendshipId" => $userRelationFriendship->userFriendshipId
                        ],
                        100, 
                        'Solicitud de amistad cancelada'
                    );
                    break;
                case 'FRIEND':
                    // return self::sendResponseError('No puede cancelar la solicitud porque ya tienes una relación de amistad');
                    return self::sendResponse([], 150, 'No puede cancelar la solicitud porque ya tienes una relación de amistad');
                    break;
            }

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/acceptFriendRequest/ acceptFriendRequest
    * @apiVersion 1.0.0
    * @apiName acceptFriendRequest
    * @apiGroup userFriendship
    * @apiDescription accept friend request
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {String} otherUid with status per user
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function acceptFriendRequest(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];
        $otherUid = $request['otherUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario que aceptará la solicitud de amistad esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario que aceptará la solicitud de amistad esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('El usuario que aceptará la solicitud de amistad no existe');
            return self::sendResponse([], 150, 'El usuario que aceptará la solicitud de amistad no existe');
        }

        if (checkIsEmpty($otherUid)) {
            // return self::sendResponseError('El identificador del usuario a aceptar la solicitud esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario a aceptar la solicitud esta vacío');
        }
        $toUserData = $this->modelUsers->getUserDataByUid($otherUid);
        if (checkIsEmpty($toUserData)) {
            // return self::sendResponseError('El usuario a aceptar la solicitud no existe');
            return self::sendResponse([], 150, 'El usuario a aceptar la solicitud no existe');
        }
        if ($myUid===$otherUid) {
            // return self::sendResponseError('los dos identificadores son el mismo');
            return self::sendResponse([], 150, 'los dos identificadores son el mismo');
        }

        try{
            $userRelationFriendship = $this->modelUserFriendship->getRelationFriendshipStatusSentBy($otherUid, $myUid, "REQUEST");
            if($userRelationFriendship===null){
                // return self::sendResponseError('No existe una solicitud de amistad');
                return self::sendResponse([], 150, 'No existe una solicitud de amistad');
            }

            $this->modelUserFriendship->changeStatusToRelationFriendship($userRelationFriendship->userFriendshipId, "FRIEND");
            return $this->sendResponse(
                [
                    "userFriendshipId" => $userRelationFriendship->userFriendshipId
                ],
                100, 
                'Solicitud de amistad aceptada'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/cancelFriendship/ cancelFriendship
    * @apiVersion 1.0.0
    * @apiName cancelFriendship
    * @apiGroup userFriendship
    * @apiDescription cancel friendship
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {String} otherUid with status per user
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function cancelFriendship(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];
        $otherUid = $request['otherUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario que eliminará la amistad esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario que eliminará la amistad esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('El usuario que eliminará la amistad no existe');
            return self::sendResponse([], 150, 'El usuario que eliminará la amistad no existe');
        }

        if (checkIsEmpty($otherUid)) {
            // return self::sendResponseError('El identificador del usuario a eliminar de la lista de amigos esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario a eliminar de la lista de amigos esta vacío');
        }
        $toUserData = $this->modelUsers->getUserDataByUid($otherUid);
        if (checkIsEmpty($toUserData)) {
            // return self::sendResponseError('El usuario a eliminar de la lista de amigos no existe');
            return self::sendResponse([], 150, 'El usuario a eliminar de la lista de amigos no existe');
        }
        if ($myUid===$otherUid) {
            // return self::sendResponseError('los dos identificadores son el mismo');
            return self::sendResponse([], 150, 'los dos identificadores son el mismo');
        }

        try{

            $userRelationFriendship = $this->modelUserFriendship->getRelationFriendship($myUid, $otherUid);
            if($userRelationFriendship===null){
                // return self::sendResponseError('No puede eliminar a esta persona de su lista de amigos porque no tiene relación con esta persona');
                return self::sendResponse([], 150, 'No puede eliminar a esta persona de su lista de amigos porque no tiene relación con esta persona');
            }
            switch ($userRelationFriendship->friendshipStatus) {
                case 'BLOCKED':
                    // return self::sendResponseError('No puede eliminar a esta persona de su lista de amigos porque se encuentra bloqueado');
                    return self::sendResponse([], 150, 'No puede eliminar a esta persona de su lista de amigos porque se encuentra bloqueado');
                    break;
                case 'REQUEST':
                    // return self::sendResponseError('No puede eliminar a esta persona de su lista de amigos porque existe una solicitud de amistad');
                    return self::sendResponse([], 150, 'No puede eliminar a esta persona de su lista de amigos porque existe una solicitud de amistad');
                    break;
                case 'FRIEND':
                    $this->modelUserFriendship->deleteRelationFriendship($userRelationFriendship->userFriendshipId);
                    return $this->sendResponse(
                        [
                            "userFriendshipId" => $userRelationFriendship->userFriendshipId
                        ],
                        100, 
                        'eliminado de su lista de amigos'
                    );
                    break;
            }

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/blockUser/ blockUser
    * @apiVersion 1.0.0
    * @apiName blockUser
    * @apiGroup userFriendship
    * @apiDescription block user
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {String} otherUid with status per user
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function blockUser(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];
        $otherUid = $request['otherUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario que bloqueará esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario que bloqueará esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('El usuario que bloqueará no existe');
            return self::sendResponse([], 150, 'El usuario que bloqueará no existe');
        }

        if (checkIsEmpty($otherUid)) {
            // return self::sendResponseError('El identificador del usuario a bloquear esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario a bloquear esta vacío');
        }
        $toUserData = $this->modelUsers->getUserDataByUid($otherUid);
        if (checkIsEmpty($toUserData)) {
            // return self::sendResponseError('El usuario a bloquear no existe');
            return self::sendResponse([], 150, 'El usuario a bloquear no existe');
        }
        if ($myUid===$otherUid) {
            // return self::sendResponseError('los dos identificadores son el mismo');
            return self::sendResponse([], 150, 'los dos identificadores son el mismo');
        }

        try{

            $userRelationFriendship = $this->modelUserFriendship->getRelationFriendship($myUid, $otherUid);
            if($userRelationFriendship){
                switch ($userRelationFriendship->friendshipStatus) {
                    case 'BLOCKED':
                        // return self::sendResponseError('Ya existe un bloqueo');
                        return self::sendResponse([], 150, 'Ya existe un bloqueo');
                        break;
                    default:
                        $blokedUserFriendshipId = $this->modelUserFriendship->blockUserWithVerification($myUid, $otherUid, $userRelationFriendship->userFriendshipId);
                        break;
                }
            }else{
                $blokedUserFriendshipId = $this->modelUserFriendship->createRelationFriendship($myUid, $otherUid, "BLOCKED");
            }

            return $this->sendResponse(
                [
                    "userFriendshipId" => $blokedUserFriendshipId.""
                ],
                100, 
                'usuario bloqueado'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/unblockUser/ unblockUser
    * @apiVersion 1.0.0
    * @apiName unblockUser
    * @apiGroup userFriendship
    * @apiDescription unblock user
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {String} otherUid with status per user
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function unblockUser(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];
        $otherUid = $request['otherUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario que desbloqueará esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario que desbloqueará esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('El usuario que desbloqueará no existe');
            return self::sendResponse([], 150, 'El usuario que desbloqueará no existe');
        }

        if (checkIsEmpty($otherUid)) {
            // return self::sendResponseError('El identificador del usuario a desbloquear esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario a desbloquear esta vacío');
        }
        $toUserData = $this->modelUsers->getUserDataByUid($otherUid);
        if (checkIsEmpty($toUserData)) {
            // return self::sendResponseError('El usuario a desbloquear no existe');
            return self::sendResponse([], 150, 'El usuario a desbloquear no existe');
        }
        if ($myUid===$otherUid) {
            // return self::sendResponseError('los dos identificadores son el mismo');
            return self::sendResponse([], 150, 'los dos identificadores son el mismo');
        }

        try{
            $userRelationFriendship = $this->modelUserFriendship->getRelationFriendshipStatusSentBy($myUid, $otherUid, "BLOCKED");
            if ($userRelationFriendship === null) {
                // return self::sendResponseError('No puede desbloquear a este usuario porque no existe ningun bloqueo');
                return self::sendResponse([], 150, 'No puede desbloquear a este usuario porque no existe ningun bloqueo');
            }
            $this->modelUserFriendship->deleteRelationFriendship($userRelationFriendship->userFriendshipId);
            return $this->sendResponse(
                [
                    "userFriendshipId" => $userRelationFriendship->userFriendshipId.""
                ],
                100, 
                'usuario desbloqueado'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/countFriendRequest/ countFriendRequest
    * @apiVersion 1.0.0
    * @apiName countFriendRequest
    * @apiGroup userFriendship
    * @apiDescription count friend request
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function countFriendRequest(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
            return self::sendResponse([], 150, 'Este usuario no existe');
        }

        try{
            $countFriendRequest = $this->modelUserFriendship->countFriendRequest($myUid);
            return $this->sendResponse(
                [
                    "countFriendRequest" => $countFriendRequest
                ],
                100, 
                'Operación exitosa'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/friendRequestList/ friendRequestList
    * @apiVersion 1.0.0
    * @apiName friendRequestList
    * @apiGroup userFriendship
    * @apiDescription friend request list
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function friendRequestList(){
        $request = self::getRequest(false);

        $myUid = $request["myUid"];
		$limit = $request["limit"];
		$offset = $request["offset"];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
            return self::sendResponse([], 150, 'Este usuario no existe');
        }

        try{
            $friendRequestList = [];
            $allFriendRequest = $this->modelUserFriendship->getAllFriendRequest($myUid, $limit, $offset);
            if (checkIsEmpty($allFriendRequest)) {
                // return self::sendResponseError('La lista de solicitudes esta vacía');
                return self::sendResponse([], 150, 'La lista de solicitudes esta vacía');
            }
            foreach ($allFriendRequest as $friendRequest) {
                    array_push($friendRequestList, $this->modelUsers->getFullUserDataByUid($myUid, $friendRequest->senderUid));
            }
            $totalFriendRequest = $this->modelUserFriendship->countFriendRequest($myUid);
            return $this->sendResponse(
                [
                    "friendRequestList" => $friendRequestList,
                    "total" => $totalFriendRequest
                ],
                100, 
                'Operación exitosa'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/friendsList/ friendsList
    * @apiVersion 1.0.0
    * @apiName friendsList
    * @apiGroup userFriendship
    * @apiDescription friends list
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function friendsList(){
        $request = self::getRequest(false);

        $myUid = $request["myUid"];
		$limit = $request["limit"];
		$offset = $request["offset"];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
            return self::sendResponse([], 150, 'Este usuario no existe');
        }

        try{
            $friends = [];
            $allFriendRelationship = $this->modelUserFriendship->getAllFriendRelationship($myUid, $limit, $offset);
            if (checkIsEmpty($allFriendRelationship)) {
                // return self::sendResponseError('La lista de amigos esta vacía');
                return self::sendResponse([], 150, 'La lista de amigos esta vacía');
            }
            foreach ($allFriendRelationship as $friendRelationship) {
                if ($friendRelationship->senderUid === $myUid) {
                    array_push($friends, $this->modelUsers->getFullUserDataByUid($myUid, $friendRelationship->addresseeUid));
                }else{
                    array_push($friends, $this->modelUsers->getFullUserDataByUid($myUid, $friendRelationship->senderUid));
                }
            }
            $totalFriends = $this->modelUserFriendship->countAllFriends($myUid);
            return $this->sendResponse(
                [
                    "friends" => $friends,
                    "total" => $totalFriends
                ],
                100, 
                'Operación exitosa'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/countAllFriends/ countAllFriends
    * @apiVersion 1.0.0
    * @apiName countAllFriends
    * @apiGroup userFriendship
    * @apiDescription count all friends
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function countAllFriends(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
            return self::sendResponse([], 150, 'Este usuario no existe');
        }

        try{
            $countAllFriends = $this->modelUserFriendship->countAllFriends($myUid);
            return $this->sendResponse(
                [
                    "countAllFriends" => $countAllFriends
                ],
                100, 
                'Operación exitosa'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} userFriendship/blockedUsersList/ blockedUsersList
    * @apiVersion 1.0.0
    * @apiName blockedUsersList
    * @apiGroup userFriendship
    * @apiDescription blocked List
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function blockedUsersList(){
        $request = self::getRequest(false);

        $myUid = $request["myUid"];
		$limit = $request["limit"];
		$offset = $request["offset"];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
            return self::sendResponse([], 150, 'Este usuario no existe');
        }

        try{
            $blockedUsers = [];
            $allBlockedRelationship = $this->modelUserFriendship->getAllBlockedRelationship($myUid, $limit, $offset);
            if (checkIsEmpty($allBlockedRelationship)) {
                // return self::sendResponseError('La lista de bloqueados esta vacía');
                return self::sendResponse([], 150, 'La lista de bloqueados esta vacía');
            }
            foreach ($allBlockedRelationship as $blockedRelationship) {
                    array_push($blockedUsers, $this->modelUsers->getFullUserDataByUid($myUid, $blockedRelationship->addresseeUid));
            }
            $totalBlocked = $this->modelUserFriendship->countAllBlocked($myUid);
            return $this->sendResponse(
                [
                    "blockedUsers" => $blockedUsers,
                    "total" => $totalBlocked
                ],
                100, 
                'Operación exitosa'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

}