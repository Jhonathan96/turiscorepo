<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Users;
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\UserSubcategories;
use App\Controllers\BaseController;

class UserSubcategoriesController extends BaseController
{
    private $modelUsers;
    private $modelCategories;
    private $modelSubcategories;
    private $modelUserSubcategories;

    public function __construct()
    {
        parent::__construct();
        $this->modelUsers = new Users();
        $this->modelCategories = new Categories();
        $this->modelSubcategories = new Subcategories();
        $this->modelUserSubcategories = new UserSubcategories();
    }

    /**
    * @api {POST} userSubcategories/userSubcategoriesRegistry/ userSubcategoriesRegistry
    * @apiVersion 1.0.0
    * @apiName userSubcategoriesRegistry
    * @apiGroup userSubcategories
    * @apiDescription user subcategories registry and user subcategories update
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {Object[]} categories with status per user
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function userSubcategoriesRegistry(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        // $subcategoriesReq = json_decode($request['subcategories']);
        $subcategoriesReq = explode(",", $request['subcategories']);

        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        try{

            $userSubcategories = $this->modelUserSubcategories->getAllUserSubcategoryByUidAndSubcategory($uid);

            foreach ($userSubcategories as $keyObjUserSubcategory => $valueObjUserSubcategory) {
                $this->modelUserSubcategories->updateUserSubcategoryStatusByUidAndSubcategoryId(
                    $uid,
                    $valueObjUserSubcategory->subcategoryId,
                    "INACTIVE"
                );
            }

            foreach ($subcategoriesReq as $objectSubCategory) {
                $verifyRow = $this->modelUserSubcategories->verifyUserSubcategoryWithUid2(
                    $uid,
                    $objectSubCategory // $objectSubCategory->subcategoryId
                );
                if ($verifyRow) {
                    $this->modelUserSubcategories->updateUserSubcategoryStatusByUidAndSubcategoryId(
                        $uid,
                        $objectSubCategory, // $objectSubCategory->subcategoryId,
                        "ACTIVE"
                    );
                }
            }

            return $this->sendResponse(
                ["subcategories" => $subcategoriesReq],
                100, 
                'Operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }        
    }

    /**
    * @api {POST} userSubcategories/getUserSubcategoriesByUid/ getUserSubcategoriesByUid
    * @apiVersion 1.0.0
    * @apiName getUserSubcategoriesByUid
    * @apiGroup userSubcategories
    * @apiDescription get user subcategories by user identifier
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getUserSubcategoriesByUid(){

        $request = self::getRequest(false);
        
        $uid = $request['uid'];
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        $allSubcategories = $this->modelSubcategories->allSubcategories();
        if (checkIsEmpty($allSubcategories)) {
            // return self::sendResponseError('No hay subcategorias.');
            return self::sendResponse([], 150, 'No hay subcategorias.');
        }

        try{
            foreach ($allSubcategories as $keySubCt => $valueSubCt) {
                $toVerify = $this->modelUserSubcategories->getUserSubcategoryByUidAndSubcategory($uid, $valueSubCt->subcategoryId);
                if (checkIsEmpty($toVerify)) {
                    $this->modelUserSubcategories->createInactiveUserSubcategory($uid, $valueSubCt->subcategoryId);
                }
            }

            $activeCategories = $this->modelCategories->getAllCategoriesByStatus("ACTIVE");
            if (checkIsEmpty($activeCategories)) {
                // return self::sendResponseError('No hay Categorias disponibles.');
                return self::sendResponse([], 150, 'No hay Categorias disponibles.');
            }
            foreach ($activeCategories as $keyCtg => $valueCtg) {
                $activeCategories[$keyCtg]->subcategories = $this->modelUserSubcategories->getUserSubcategoryByUidAndCategoryId($uid, $valueCtg->categoryId);
            }

            return $this->sendResponse(
                ["categories" => $activeCategories],
                100,
                'Operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

}