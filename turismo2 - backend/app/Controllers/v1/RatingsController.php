<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Users;
use App\Models\Sites;
use App\Models\Ratings;
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\UserSubcategories;
use App\Controllers\BaseController;

class RatingsController extends BaseController
{
    private $modelUsers;
    private $modelSites;
    private $modelRatings;
    private $modelCategories;
    private $modelSubcategories;
    private $modelUserSubcategories;

    public function __construct()
    {
        parent::__construct();
        $this->modelUsers = new Users();
        $this->modelSites = new Sites();
        $this->modelRatings = new Ratings();
        $this->modelCategories = new Categories();
        $this->modelSubcategories = new Subcategories();
        $this->modelUserSubcategories = new UserSubcategories();
    }

    /**
    * @api {POST} ratings/createSiteRating/ createSiteRating
    * @apiVersion 1.0.0
    * @apiName createSiteRating
    * @apiGroup ratings
    * @apiDescription create site rating
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {Number} rating rating by user
    * @apiParam {String} ratingComment rating comment by user
    * @apiParam {String} uid user identifier
    * @apiParam {Number} siteId site identifier
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function createSiteRating(){

        $request = self::getRequest(false);

        $rating = $request['rating'];
        $ratingComment = $request['ratingComment'];
        $uid = $request['uid'];
        $siteId = $request['siteId'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        if (checkIsEmpty($siteId)) {
            // return self::sendResponseError('el identificador del sitio esta vacío.');
            return self::sendResponse([], 150, 'el identificador del sitio esta vacío.');
        }
        $siteData = $this->modelSites->getSiteById($siteId);
        if (checkIsEmpty($siteData)) {
            // return self::sendResponseError('Este sitio no existe.');
            return self::sendResponse([], 150, 'Este sitio no existe.');
        }
        if (checkIsEmpty($rating)) {
            // return self::sendResponseError('la calificación del sitio esta vacía.');
            return self::sendResponse([], 150, 'la calificación del sitio esta vacía.');
        }
        if ($rating < 1 || $rating > 5) {
            // return self::sendResponseError('la calificación debe ser entre 1 y 5');
            return self::sendResponse([], 150, 'la calificación debe ser entre 1 y 5');
        }

        try{
            $ratingSiteData = $this->modelRatings->getDataSiteRatingByUser($uid, $siteId);

            if ($ratingSiteData) {
                $update = $this->modelRatings->updateSiteRating($ratingSiteData[0]->ratingId, $rating, $ratingComment, $uid, $siteId);
                return $this->sendResponse(
                    ["update" => $update],
                    100,
                    'Actualización de la calificación exitosa'
                );
            }else{
                $create = $this->modelRatings->createSiteRating($rating, $ratingComment, $uid, $siteId);
                return $this->sendResponse(
                    ["create" => $create],
                    100, 
                    'Calificación exitosa'
                );
            }

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }        
    }

    /**
    * @api {POST} ratings/getAllSiteRatings/ getAllSiteRatings
    * @apiVersion 1.0.0
    * @apiName getAllSiteRatings
    * @apiGroup ratings
    * @apiDescription get all site ratings
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    * @apiParam {Number} siteId site identifier
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getAllSiteRatings(){

        $request = self::getRequest(false);

        $myUid = $request['myUid'];
        $siteId = $request['siteId'];
        $limit = $request['limit'];
        $offset = $request['offset'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        if (checkIsEmpty($siteId)) {
            // return self::sendResponseError('el identificador del sitio esta vacío.');
            return self::sendResponse([], 150, 'el identificador del sitio esta vacío.');
        }
        $siteData = $this->modelSites->getSiteById($siteId);
        if (checkIsEmpty($siteData)) {
            // return self::sendResponseError('Este sitio no existe.');
            return self::sendResponse([], 150, 'Este sitio no existe.');
        }

        try{
            $siteRatings = $this->modelRatings->getAllSiteRatings($myUid, $siteId, $limit, $offset);
            if (checkIsEmpty($siteRatings)) {
                // return self::sendResponseError('No hay calificaciones de este sitio');
                return self::sendResponse([], 150, 'No hay calificaciones de este sitio');
            }
            $totalSiteRatings = $this->modelRatings->countAllSiteRatings($myUid, $siteId);

            return $this->sendResponse(
                [
                    "siteRatings" => $siteRatings,
                    "total" => $totalSiteRatings
                ],
                100, 
                'Operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }        
    }

    /**
    * @api {POST} ratings/getSiteRatingByUser/ getSiteRatingByUser
    * @apiVersion 1.0.0
    * @apiName getSiteRatingByUser
    * @apiGroup ratings
    * @apiDescription get site rating by user
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {Number} siteId site identifier
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getSiteRatingByUser(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        $siteId = $request['siteId'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        if (checkIsEmpty($siteId)) {
            // return self::sendResponseError('el identificador del sitio esta vacío.');
            return self::sendResponse([], 150, 'el identificador del sitio esta vacío.');
        }
        $siteData = $this->modelSites->getSiteById($siteId);
        if (checkIsEmpty($siteData)) {
            // return self::sendResponseError('Este sitio no existe.');
            return self::sendResponse([], 150, 'Este sitio no existe.');
        }

        try{
            $siteRatings = $this->modelRatings->getSiteRatingByUser($uid, $siteId);
            if (checkIsEmpty($siteRatings)) {
                // return self::sendResponseError('No existe la calificación de este sitio');
                return self::sendResponse([], 150, 'No existe la calificación de este sitio');
            }

            return $this->sendResponse(
                [
                    "siteRatings" => $siteRatings
                ],
                100, 
                'Operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }        
    }

}