<?php 
namespace App\Controllers\v1;

use Exception;
use App\Models\Users as User;
use App\Models\UserSubcategories;
use App\Models\Role;
use Kubo\v1\Auth;
use App\Controllers\BaseController;

class UserController extends BaseController
{
	private $modelUsers;
	private $modelUserSubcategories;
	private $modelRole;
	
	public function __construct() 
    {
		parent::__construct();
		$this->modelUsers = new User();
		$this->modelUserSubcategories = new UserSubcategories();
		$this->modelRole = new Role();
	}

	/**
    * @api {POST} user/getRecommendedUsers/ getRecommendedUsers
    * @apiVersion 1.0.0
    * @apiName getRecommendedUsers
    * @apiGroup user
    * @apiDescription get recommended users
	*
	* @apiHeader {String} X-TP-Auth-Token Header access token.
    * @apiParam {String} myUid user identifier
	* @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
	*
    * @apiSuccess {Number} code 200: success ok <br> 102: error in service
    */
	public function getRecommendedUsers(){
		$request = self::getRequest(false);

		$myUid = $request["myUid"];
		$limit = $request["limit"];
		$offset = $request["offset"];

		// validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
			return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
			return self::sendResponse([], 150, 'Este usuario no existe');
        }
		try {
			$whereSubcategories = "";
			$userSubcategoriesActive = $this->modelUserSubcategories->getUserSubcategoriesIdsActive($myUid);

			foreach ($userSubcategoriesActive as $userSubcategoryActive) {
				$whereSubcategories == "" 
				? $whereSubcategories .= " AND (userSubcategories.subcategoryId=".$userSubcategoryActive->subcategoryId 
				: $whereSubcategories .= " OR userSubcategories.subcategoryId=".$userSubcategoryActive->subcategoryId;
            }
			if($whereSubcategories!=""){
                $whereSubcategories .= ")";
            }

			$usersWithSubcategories = $this->modelUsers->getRecommendedUsers($myUid, $limit, $offset, $whereSubcategories);
			if (checkIsEmpty($usersWithSubcategories)) {
                // return self::sendResponseError('La lista de usuarios recomendados esta vacía');
				return self::sendResponse([], 150, 'La lista de usuarios recomendados esta vacía');
            }
			$allUsers = [];
			foreach ($usersWithSubcategories as $userWithSubcategories) {
				array_push($allUsers, $this->modelUsers->getFullUserDataByUid($myUid, $userWithSubcategories->uid));
			}
			$total = $this->modelUsers->countRecommendedUsers($myUid, $whereSubcategories);

			return $this->sendResponse(
                [
                    "users" => $allUsers,
                    "total" => $total
                ],
                100, 
                'operación exitosa'
            );

		} catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
			return self::sendResponse([], 150, $ex->getMessage());
        }
	}

	/**
    * @api {POST} user/getRecentUsers/ getRecentUsers
    * @apiVersion 1.0.0
    * @apiName getRecentUsers
    * @apiGroup user
    * @apiDescription get recent users
	*
	* @apiHeader {String} X-TP-Auth-Token Header access token.
    * @apiParam {String} myUid user identifier
	* @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
	*
    * @apiSuccess {Number} code 200: success ok <br> 102: error in service
    */
	public function getRecentUsers(){
		$request = self::getRequest(false);

		$myUid = $request["myUid"];
		$limit = $request["limit"];
		$offset = $request["offset"];

		// validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
			return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
			return self::sendResponse([], 150, 'Este usuario no existe');
        }
		try {

			$recentUsers = $this->modelUsers->getRecentUsers($myUid, $limit, $offset);
			if (checkIsEmpty($recentUsers)) {
                // return self::sendResponseError('La lista de usuarios esta vacía');
				return self::sendResponse([], 150, 'La lista de usuarios esta vacía');
            }
			$allUsers = [];
			foreach ($recentUsers as $recentUser) {
				array_push($allUsers, $this->modelUsers->getFullUserDataByUid($myUid, $recentUser->uid));
			}
			$total = $this->modelUsers->countUsersExceptMe($myUid);

			return $this->sendResponse(
                [
                    "users" => $allUsers,
                    "total" => $total
                ],
                100, 
                'operación exitosa'
            );

		} catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
			return self::sendResponse([], 150, $ex->getMessage());
        }
	}

	/**
    * @api {POST} user/updateProfile/ updateProfile
    * @apiVersion 1.0.0
    * @apiName updateProfile
    * @apiGroup user
    * @apiDescription update profile data
	*
	* @apiHeader {String} X-TP-Auth-Token Header access token.
    * @apiParam {String} uid user identifier
	* @apiParam {String} fullName
	* @apiParam {String} oldPassword encrypt on sha1
	* @apiParam {String} newPassword encrypt on sha1
    * @apiParam {String} description
	*
    * @apiSuccess {Number} code 200: success ok <br> 102: error in service
    */
	public function updateProfile(){
		$request = self::getRequest(false);

		$uid = $request["uid"];
		$fullName = $request["fullName"];
		$oldPassword = $request["oldPassword"];
		$newPassword = $request["newPassword"];
		$description = $request["description"];
		 
		$requestToSave = [
			'fullName' 		=> $fullName,
			'description'	=> $description
		];
		if (!checkIsEmpty($oldPassword) && !checkIsEmpty($newPassword))
		{
			$user = $this->modelUsers->find($request['uid']);
			if (password_verify($oldPassword, $user->password)) 
				$requestToSave['password'] = password_hash($newPassword, PASSWORD_BCRYPT);
			else 
				// return self::sendResponseError('La contraseña actual no coincide.');
				return self::sendResponse([], 150, 'La contraseña actual no coincide.');
		}
		// TO UPLOAD IMAGE (DISABLED)
		// $imgFile = $this->request->getFile('image');
		// $fileExtention = $imgFile->guessExtension();
		// $imgName = "";
        // if (isset($imgFile) && !empty($imgFile) && $fileExtention!=='' && 
		// ($fileExtention==='png' || $fileExtention==='jpg' || $fileExtention==='jpeg')
		// ){
        //     $imgName = uniqid() .'.'. $imgFile->guessExtension();
        //     if (!$this->uploadFileImgUser($imgFile, 'imagesUsers/', $imgName))
		// 		return self::sendResponse([], 150, 'Error al subir la imagen de perfil');
		// 	// $imgFullname= BASE_URL_IMGS_USER.'turismo2/public/imagesUsers/'.$imgName;
		// 	$imgFullname= 'turismo2/public/imagesUsers/'.$imgName;
        // }else{
        //     $imgFullname = $user->image;
        // }
		// $requestToSave['image'] = $imgFullname;

		$newUserData = $this->modelUsers->update($uid, $requestToSave);
		if ($newUserData) 
		{
			$user = $this->modelUsers->select("uid, fullName, email, accountType, tries, valid, roleId, image, description")
			->find($uid);
			$user->role = $this->modelRole->select("roleId, name, status, updated_at, created_at, deleted_at")
			->where(["roleId" => $user->roleId])->first();
			unset($user->roleId);
		}
		else
			$user = [];

		return !empty($user) ? $this->sendResponse($user, 100, 'Operacion exitosa') :
			$this->sendResponse([], 102, 'Error al actualizar los datos');

	}

	public function uploadFileImgUser($file, string $path, string $newFileName) :bool
    {
        if ($file->isValid() && !$file->hasMoved())
        {
            $file->move($path, $newFileName);
            return true;
        }
        return false;
    }

	// public function updatePhoto()
	// {
	// 	$request = self::getRequest(false);
	// 	$file = $this->request->getFile('photo');
	// 	$file->move('./public/profileImage', $file->getName());
	// 	$user = $this->modelUsers->updateImageName($request['uid'], $file->getName());
	// 	return $this->sendResponse($this->modelUsers->getBasicDataUser($request['uid']), 100);
	// }

	//  /**
	//  * @api {POST}  User/resetPassword/ resetPassword
	//  * @apiVersion 1.0.0
	//  * @apiName recovery
	//  * @apiGroup Auth
	//  * @apiDescription Service to reset password
	//  * @apiHeader {String} X-TP-Access-Token Header Access token.
	//  * @apiParam {String} email user email
	//  * @apiSuccess {Number} code 100: service ok  <br> != 100 error in service
	//  */
	// public function resetPassword()
	// {
	// 	$request = self::getRequest(false);

	// }

	/**
    * @api {POST} user/getUserData/ getUserData
    * @apiVersion 1.0.0
    * @apiName getUserData
    * @apiGroup user
    * @apiDescription get user data
	*
	* @apiHeader {String} X-TP-Auth-Token Header access token.
    * @apiParam {String} myUid user identifier
	* @apiParam {String} userId other user identifier
	*
    * @apiSuccess {Number} code 200: success ok <br> 102: error in service
    */
	public function getUserData(){
		$request = self::getRequest(false);

		$myUid = $request["myUid"];
		$userId = $request["userId"];

		// validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
			return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
			return self::sendResponse([], 150, 'Este usuario no existe');
        }
		if (checkIsEmpty($userId)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
			return self::sendResponse([], 150, 'El identificador del usuario a consultar esta vacío');
        }
        $fromOtherUserData = $this->modelUsers->getUserDataByUid($userId);
        if (checkIsEmpty($fromOtherUserData)) {
            // return self::sendResponseError('Este usuario no existe');
			return self::sendResponse([], 150, 'El usuario a consultar no existe');
        }
		try {
			$userData = $this->modelUsers->getFullUserDataByUid($myUid, $userId);

			return $this->sendResponse(
                [
                    "userData" => $userData
                ],
                100, 
                'operación exitosa'
            );

		} catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
			return self::sendResponse([], 150, $ex->getMessage());
        }
	}
}
