<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Users;
use App\Models\Sites;
use App\Models\FavoriteSites;
use App\Controllers\BaseController;

class FavoriteSitesController extends BaseController
{
    private $modelFavoriteSites;
    private $modelUsers;
    private $modelSites;

    public function __construct()
    {
        parent::__construct();
        $this->modelFavoriteSites = new FavoriteSites();
        $this->modelUsers = new Users();
        $this->modelSites = new Sites();
    }

    /**
    * @api {POST} favoriteSites/createOrDeleteFavoritesSite/ createOrDeleteFavoritesSite
    * @apiVersion 1.0.0
    * @apiName createOrDeleteFavoritesSite
    * @apiGroup favoriteSites
    * @apiDescription create or delete favorites site
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {Number} siteId site identifier
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function createOrDeleteFavoritesSite(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        $siteId = $request['siteId'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        if (checkIsEmpty($siteId)) {
            // return self::sendResponseError('el identificador del sitio esta vacío.');
            return self::sendResponse([], 150, 'El identificador del sitio esta vacío.');
        }
        $siteData = $this->modelSites->getSiteById($siteId);
        if (checkIsEmpty($siteData)) {
            // return self::sendResponseError('Este sitio no existe.');
            return self::sendResponse([], 150, 'Este sitio no existe.');
        }

        try{
            $favoriteSiteData = $this->modelFavoriteSites->getSiteFavoriteByUser($uid, $siteId);

            if ($favoriteSiteData) {
                $this->modelFavoriteSites->deleteSiteFavoriteByUser($favoriteSiteData->favoriteSiteId);
                $response = intval($favoriteSiteData->favoriteSiteId);
                $msj = "eliminado";
            }else{
                $response = $this->modelFavoriteSites->createSiteFavoriteByUser($uid, $siteId);
                $msj = "agregado";
            }
            return $this->sendResponse(
                ["row" => $response],
                100,
                'sitio ' . $msj . ' de favoritos'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }        
    }

    /**
    * @api {POST} favoriteSites/countUserFavoriteSites/ countUserFavoriteSites
    * @apiVersion 1.0.0
    * @apiName countUserFavoriteSites
    * @apiGroup favoriteSites
    * @apiDescription count user favorite sites
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function countUserFavoriteSites(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
            return self::sendResponse([], 150, 'Este usuario no existe');
        }

        try{
            $totalMyFavoritesSites = $this->modelSites->countMyFavoritesSites($myUid, "", "", "", "");
            return $this->sendResponse(
                [
                    "countUserFavoriteSites" => $totalMyFavoritesSites
                ],
                100, 
                'Operación exitosa'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

}