<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Users;
use App\Models\Feedback;

use App\Controllers\BaseController;

class FeedbackController extends BaseController
{
    private $modelUsers;
    private $modelFeedback;

    public function __construct()
    {
        parent::__construct();
        $this->modelUsers = new Users();
        $this->modelFeedback = new Feedback();
    }

    /**
    * @api {POST} feedback/createFeedback/ createFeedback
    * @apiVersion 1.0.0
    * @apiName createFeedback
    * @apiGroup feedback
    * @apiDescription create feedback - application rating
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {Number} rating rating by user
    * @apiParam {String} comment rating comment by user
    * @apiParam {String} uid user identifier
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function createFeedback(){

        $request = self::getRequest(false);

        $rating = $request['rating'];
        $comment = $request['comment'];
        $uid = $request['uid'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }
        if (checkIsEmpty($rating)) {
            // return self::sendResponseError('la calificación del sitio esta vacía.');
            return self::sendResponse([], 150, 'La calificación del sitio esta vacía.');
        }
        if ($rating < 1 || $rating > 5) {
            // return self::sendResponseError('la calificación debe ser entre 1 y 5');
            return self::sendResponse([], 150, 'La calificación debe ser entre 1 y 5');
        }
        if (checkIsEmpty($comment)) {
            return self::sendResponse([], 150, 'Debe enviar un comentario');
        }
        if (strlen($comment)>200) {
            return self::sendResponse([], 150, 'El comentario no puede superar los 200 caracteres');
        }

        if (strlen($comment)<4) {
            return self::sendResponse([], 150, 'El comentario debe tener al menos 4 caracteres');
        }

        try{
            $feedbackData = $this->modelFeedback->getFeedbackByUser($uid);

            if ($feedbackData) {
                $update = $this->modelFeedback->updateFeedback($feedbackData[0]->feedbackId, $rating, $comment, $uid);
                return $this->sendResponse(
                    ["update" => $update],
                    100,
                    'Actualización de la calificación exitosa'
                );
            }else{
                $create = $this->modelFeedback->createFeedback($rating, $comment, $uid);
                return $this->sendResponse(
                    ["create" => $create],
                    100, 
                    'Calificación exitosa'
                );
            }

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }        
    }

}