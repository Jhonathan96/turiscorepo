<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Categories;
use App\Models\Subcategories;
use App\Controllers\BaseController;

class SubcategoriesController extends BaseController
{
    private $modelCategories;
    private $modelSubcategories;

    public function __construct()
    {
        parent::__construct();
        $this->modelCategories = new Categories();
        $this->modelSubcategories = new Subcategories();
    }

    /**
    * @api {POST} subcategories/getAllSubcategories/ getAllSubcategories
    * @apiVersion 1.0.0
    * @apiName getAllSubcategories
    * @apiGroup subcategories
    * @apiDescription get all subcategories with total rows
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    * @apiParam {String} status status rows
    * @apiParam {String} categoryName filter categoryName
    * @apiParam {String} subcategoryName filter subcategoryName
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getAllSubcategories(){

        $request = self::getRequest(false);

        $limit = $request['limit'];
        $offset = $request['offset'];
        $statusReq = $request['status'];
        $categoryNameReq = $request['categoryName'];
        $subcategoryNameReq = $request['subcategoryName'];
        
        $statusReq !== '' && $statusReq !== null ? $status = $statusReq : $status = 'ACTIVE';
        $categoryNameReq !== '' && $categoryNameReq !== null ? $categoryNameLike = $categoryNameReq : $categoryNameLike = '';
        $subcategoryNameReq !== '' && $subcategoryNameReq !== null ? $subcategoryNameLike = $subcategoryNameReq : $subcategoryNameLike = '';

        $subcategories = $this->modelSubcategories
        ->where(["subcategories.status" => $status])
        ->join("categories","categories.categoryId=subcategories.categoryId","LEFT OUTER")
        ->like('categories.categoryName', $categoryNameLike, 'both')
        ->like('subcategories.subcategoryName', $subcategoryNameLike, 'both')
        ->orderBy('subcategoryName','ASC')
        ->findAll($limit, $offset);

        if (checkIsEmpty($subcategories)) {
            // return self::sendResponseError('No hay subcategorias.');
            return self::sendResponse([], 150, 'No hay subcategorias.');
        }

        $totalRows = $this->modelSubcategories
        ->where(["subcategories.status" => $status])
        ->join("categories","categories.categoryId=subcategories.categoryId","LEFT OUTER")
        ->like('categories.categoryName', $categoryNameLike, 'both')
        ->like('subcategories.subcategoryName', $subcategoryNameLike, 'both')
        ->findAll();

        return $this->sendResponse(
            ["subcategories" => $subcategories, "total" => count($totalRows)],
            100, 
            'Operación exitosa'
        );
    }

    /**
    * @api {POST} subcategories/getSubcategoriesOrganizedByCategory/ getSubcategoriesOrganizedByCategory
    * @apiVersion 1.0.0
    * @apiName getSubcategoriesOrganizedByCategory
    * @apiGroup subcategories
    * @apiDescription get subcategories organized by category
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} categoryName filter categoryName
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getSubcategoriesOrganizedByCategory(){

        $request = self::getRequest(false);
        $categoryNameReq = $request['categoryName'];
        
        $categoryNameReq !== '' && $categoryNameReq !== null ? $categoryNameLike = $categoryNameReq : $categoryNameLike = '';

        $categories = $this->modelCategories->getAllCategoriesFilterByName($categoryNameLike);
        if (checkIsEmpty($categories)) {
            return self::sendResponse([], 150, 'No hay subcategorias.');
        }
        foreach ($categories as $keyCategory => $valueCategory) {
            $categories[$keyCategory]->subcategories = $this->modelSubcategories->subcategoriesByCategoryId($valueCategory->categoryId);
        }

        return $this->sendResponse(
            ["categories" => $categories],
            100, 
            'Operación exitosa'
        );
    }

    /**
    * @api {POST} subcategories/getSubcategoriesByCategoryId/ getSubcategoriesByCategoryId
    * @apiVersion 1.0.0
    * @apiName getSubcategoriesByCategoryId
    * @apiGroup subcategories
    * @apiDescription get subcategories by categoryId with total rows
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    * @apiParam {String} status status rows
    * @apiParam {String} categoryName filter categoryName
    * @apiParam {String} subcategoryName filter subcategoryName
    * @apiParam {Number} categoryId category identifier
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getSubcategoriesByCategoryId(){

        $request = self::getRequest(false);

        $limit = $request['limit'];
        $offset = $request['offset'];
        $statusReq = $request['status'];
        $categoryNameReq = $request['categoryName'];
        $subcategoryNameReq = $request['subcategoryName'];
        $categoryIdReq = $request['categoryId'];
        
        $statusReq !== '' && $statusReq !== null ? $status = $statusReq : $status = 'ACTIVE';
        $categoryNameReq !== '' && $categoryNameReq !== null ? $categoryNameLike = $categoryNameReq : $categoryNameLike = '';
        $subcategoryNameReq !== '' && $subcategoryNameReq !== null ? $subcategoryNameLike = $subcategoryNameReq : $subcategoryNameLike = '';

        if (checkIsEmpty($categoryIdReq) || $categoryIdReq === 0 || $categoryIdReq === '0') {
            // return self::sendResponseError('Debes seleccionar una categoria.');
            return self::sendResponse([], 150, 'Debes seleccionar una categoria.');
        }

        $subcategories = $this->modelSubcategories
        ->where([
            "subcategories.status" => $status,
            "subcategories.categoryId" => $categoryIdReq
        ])
        ->join("categories","categories.categoryId=subcategories.categoryId","LEFT OUTER")
        ->like('categories.categoryName', $categoryNameLike, 'both')
        ->like('subcategories.subcategoryName', $subcategoryNameLike, 'both')
        ->orderBy('subcategoryName','ASC')
        ->findAll($limit, $offset);

        if (checkIsEmpty($subcategories)) {
            // return self::sendResponseError('No hay subcategorias.');
            return self::sendResponse([], 150, 'No hay subcategorias.');
        }

        $totalRows = $this->modelSubcategories
        ->where([
            "subcategories.status" => $status,
            "subcategories.categoryId" => $categoryIdReq
        ])
        ->join("categories","categories.categoryId=subcategories.categoryId","LEFT OUTER")
        ->like('categories.categoryName', $categoryNameLike, 'both')
        ->like('subcategories.subcategoryName', $subcategoryNameLike, 'both')
        ->findAll();

        return $this->sendResponse(
            ["subcategories" => $subcategories, "total" => count($totalRows)],
            100, 
            'Operación exitosa'
        );
    }

}