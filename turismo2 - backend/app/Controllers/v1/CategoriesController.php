<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Categories;
use App\Controllers\BaseController;

class CategoriesController extends BaseController
{
    private $modelCategories;

    public function __construct()
    {
        parent::__construct();
        $this->modelCategories = new Categories();
    }

    /**
    * @api {POST} categories/getAllCategories/ getAllCategories
    * @apiVersion 1.0.0
    * @apiName getAllCategories
    * @apiGroup categories
    * @apiDescription get all categories with total rows
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    * @apiParam {String} status status rows
    * @apiParam {String} categoryName filter categoryName
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getAllCategories(){

        $request = self::getRequest(false);

        $limit = $request['limit'];
        $offset = $request['offset'];
        $statusReq = $request['status'];
        $categoryNameReq = $request['categoryName'];

        $statusReq !== '' && $statusReq !== null ? $status = $statusReq : $status = 'ACTIVE';
        $categoryNameReq !== '' && $categoryNameReq !== null ? $categoryNameLike = $categoryNameReq : $categoryNameLike = '';

        $categories = $this->modelCategories
        ->where(["status" => $status])
        ->like('categoryName', $categoryNameLike, 'both')
        ->orderBy('categoryName','ASC')
        ->findAll($limit, $offset);

        if (checkIsEmpty($categories)) {
            // return self::sendResponseError('No hay categorias.');
            return self::sendResponse([], 150, 'No hay categorias.');
        }

        $totalRows = $this->modelCategories
        ->where(["status" => $status])
        ->like('categoryName', $categoryNameLike, 'both')
        ->findAll();

        return $this->sendResponse(
            ["categories" => $categories, "total" => count($totalRows)], 
            100,
            'Operación exitosa'
        );
    }

    // /**
    // * @api {POST} ads/setAdVisualization/ setAdVisualization
    // * @apiVersion 1.0.0
    // * @apiName setAdVisualization
    // * @apiGroup ads
    // * @apiDescription set ad visualization
    // *
    // * @apiHeader {String} X-TP-Access-Token Header access token.
    // *
    // * @apiParam {Number} adsId ad identifier
    // * @apiParam {Number} adsPerVideoId adsPerVideo identifier
    // * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    // */
    // public function setAdVisualization(){
    //     $request = self::getRequest(false);
    //     $adsId = $request['adsId'];
    //     $adsPerVideoId = $request['adsPerVideoId'];

    //     if(checkIsEmpty($this->modelAdsPerVideo->find($adsPerVideoId))){
    //         return self::sendResponseError('El identificador de anuncio por video no existe');
    //     }

    //     if(checkIsEmpty($this->modelAds->find($adsId))){
    //         return self::sendResponseError('El anuncio no existe');
    //     }

    //     try{

    //         $this->modelAdViews->setAdVisualization($adsId);
    //         $this->modelAdsPerVideo->addCountRotation($adsPerVideoId);
    //         return $this->sendResponse([], 100, 'Operación exitosa');

    //     } catch (Exception $ex) {
    //         return self::sendResponseError($ex->getMessage());
    //     }
    // }

    // /**
    // * @api {POST} ads/setAdClick/ setAdClick
    // * @apiVersion 1.0.0
    // * @apiName setAdClick
    // * @apiGroup ads
    // * @apiDescription set ad click
    // *
    // * @apiHeader {String} X-TP-Access-Token Header access token.
    // *
    // * @apiParam {String} adsId ad identifier
    // * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    // */
    // public function setAdClick(){
    //     $request = self::getRequest(false);
    //     $adsId = $request['adsId'];

    //     if(checkIsEmpty($this->modelAds->find($adsId))){
    //         return self::sendResponseError('El anuncio no existe');
    //     }

    //     try{

    //         $this->modelAdClicks->setAdClick($adsId);
    //         return $this->sendResponse([], 100, 'Operación exitosa');

    //     } catch (Exception $ex) {
    //         return self::sendResponseError($ex->getMessage());
    //     }
    // }

}