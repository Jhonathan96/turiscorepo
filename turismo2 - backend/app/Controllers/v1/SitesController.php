<?php
namespace App\Controllers\v1;

use Exception;
use App\Models\Users;
use App\Models\Ratings;
use App\Models\Sites;
use App\Models\Cities;
use App\Models\UserSubcategories;
use App\Controllers\BaseController;
use App\Libraries\CollaborativeFilter;

class SitesController extends BaseController
{
    private $modelUsers;
    private $modelRatings;
    private $modelSites;
    private $modelCities;
    private $modelUserSubcategories;
    private $libraryCollaborativeFilter;

    public function __construct()
    {
        parent::__construct();
        $this->modelUsers = new Users();
        $this->modelRatings = new Ratings();
        $this->modelSites = new Sites();
        $this->modelCities = new Cities();
        $this->modelUserSubcategories = new UserSubcategories();
        $this->libraryCollaborativeFilter = new CollaborativeFilter();
    }

    /**
    * @api {POST} sites/getRecommendSites/ getRecommendSites
    * @apiVersion 1.0.0
    * @apiName getRecommendSites
    * @apiGroup sites
    * @apiDescription get recommend sites
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {String} maxKmsDistance maximum distance in kilometers
    * @apiParam {String} usrLatitude user lantitude
    * @apiParam {String} usrLongitude user longitude
    * @apiParam {String} cityId city identifier
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getRecommendSites(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        $maxKmsDistance = $request['maxKmsDistance'];
        $usrLatitude = $request['usrLatitude'];
        $usrLongitude = $request['usrLongitude'];
        $cityId = $request['cityId'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        try{
            $subcategoriesIds = [];
            $whereSubcategories = "";
        
            $subcategoriesRatings = $this->modelRatings->getSubcategoriesIdsByUserRatings($uid);
            $userSubcategoriesActive = $this->modelUserSubcategories->getUserSubcategoriesIdsActive($uid);

            foreach ($subcategoriesRatings as $subcategoryRating) {
                array_push($subcategoriesIds, $subcategoryRating->subcategoryId);
            }

            foreach ($userSubcategoriesActive as $userSubcategoryActive) {
                if (!in_array($userSubcategoryActive->subcategoryId, $subcategoriesIds)) {
                    array_push($subcategoriesIds, $userSubcategoryActive->subcategoryId);
                }
            }

            if (checkIsEmpty($subcategoriesIds)) {
                // return self::sendResponseError('Por favor seleccione subcategorias de su preferencia para la recomendación');
                return self::sendResponse([], 140, 'Por favor seleccione subcategorias de su preferencia para la recomendación');
            }

            $allUsersIdsObjects = $this->modelUsers->getAllUserIds();
            
            foreach ($subcategoriesIds as $subcategoryId) {
                $whereSubcategories == "" ? $whereSubcategories .= "(sites.subcategoryId=".$subcategoryId : $whereSubcategories .= " OR sites.subcategoryId=".$subcategoryId;
            }
            if($whereSubcategories!=""){
                $whereSubcategories .= ")";
            }

            if($maxKmsDistance && $usrLatitude && $usrLongitude){
                $sites = $this->getRecommendSitesWithLocation($uid, $maxKmsDistance, $usrLatitude, $usrLongitude, $allUsersIdsObjects, $whereSubcategories);
            }else{
                if (!checkIsEmpty($cityId)) {
                    $cityData = $this->modelCities->existCity($cityId);
                    if (checkIsEmpty($cityData)) {
                        // return self::sendResponseError('Esta ciudad no existe.');
                        return self::sendResponse([], 150, 'Esta ciudad no existe.');
                    }
                }
                $sites = $this->getRecommendSitesWithoutLocation($uid, $cityId, $allUsersIdsObjects, $whereSubcategories, $usrLatitude, $usrLongitude);
            }

            return $this->sendResponse(
                [
                    "sites" => $sites
                ],
                100, 
                'operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    private function getRecommendSitesWithoutLocation($uid, $cityId, $allUsersIdsObjects, $whereSubcategories, $usrLatitude, $usrLongitude){
        $goodRatings = [];
        $whereOtherSites = "";

        if(!checkIsEmpty($allUsersIdsObjects)){
            foreach ($allUsersIdsObjects as $userIdsObject) {
                $matrixSites[$userIdsObject->uid] = $this->modelRatings->getUserRatingsPerSubcategories($userIdsObject->uid, $whereSubcategories, $cityId, true);
                $matrixSimilarity[$userIdsObject->uid] = $this->modelRatings->getUserAllRatings($userIdsObject->uid, true);
            }

            $resultCollaborative = $this->libraryCollaborativeFilter->getRecommendations($matrixSimilarity, $matrixSites, $uid);

            foreach ($resultCollaborative as $keySiteId => $valueSiteRating) {
                if($valueSiteRating >= 4){
                // if($valueSiteRating >= 3){
                    $goodSiteData = $this->modelSites->getSiteAllDataById(str_replace("id", "", $keySiteId), $usrLatitude, $usrLongitude, round($valueSiteRating, 2));
                    $goodSiteData->myQualification = $this->modelRatings->getOnlySiteRatingByUser($uid, $goodSiteData->siteId);
                    $goodSiteData->isMyFavorite = $this->modelSites->getIsMyFavoriteSite($uid, $goodSiteData->siteId);
                    array_push($goodRatings, $goodSiteData);
                    $whereOtherSites == "" ? $whereOtherSites .= "(sites.siteId!=".str_replace("id", "", $keySiteId) : $whereOtherSites .= " AND sites.siteId!=".str_replace("id", "", $keySiteId);
                }
            }
        }
        $itemsSitesVisited = $this->modelSites->getUserVisitedSitesOnlySitesId($uid);

        foreach ($itemsSitesVisited as $objVisitedSite) {
            $whereOtherSites == "" ? $whereOtherSites .= "(sites.siteId!=".$objVisitedSite->siteId : $whereOtherSites .= " AND sites.siteId!=".$objVisitedSite->siteId;
        }
        if($whereOtherSites!=""){
            $whereOtherSites .= ")";
        }

        $otherSites = $this->modelSites->getOtherSitesWithSubcategories($whereOtherSites, $whereSubcategories, $cityId, $usrLatitude, $usrLongitude);
        foreach ($otherSites as $otherSite) {
            isset($otherSite->distance) ? $otherSite->distance = round($otherSite->distance, 1) : $otherSite->distance=null;
            // $otherSite->qualification = round($otherSite->qualification, 2);
            $otherSite->qualification == 0 ? $otherSite->qualification = 0 : $otherSite->qualification = round($otherSite->qualification,1);
            $otherSite->myQualification = $this->modelRatings->getOnlySiteRatingByUser($uid, $otherSite->siteId);
            $otherSite->isMyFavorite = $this->modelSites->getIsMyFavoriteSite($uid, $otherSite->siteId);
        }
        (checkIsEmpty($goodRatings)) ? $sitesRecommend = $otherSites : $sitesRecommend = array_merge($goodRatings, $otherSites);
        return $sitesRecommend;
    }

    private function getRecommendSitesWithLocation($uid, $maxKmsDistance, $usrLatitude, $usrLongitude, $allUsersIdsObjects, $whereSubcategories){
        $goodRatings = [];
        $whereOtherSites = "";

        if(!checkIsEmpty($allUsersIdsObjects)){
            foreach ($allUsersIdsObjects as $userIdsObject) {
                $matrixSites[$userIdsObject->uid] = $this->modelRatings->getUserRatingsPerSubcategoriesWithLocation($userIdsObject->uid, $whereSubcategories, $maxKmsDistance, $usrLatitude, $usrLongitude, true);
                $matrixSimilarity[$userIdsObject->uid] = $this->modelRatings->getUserAllRatings($userIdsObject->uid, true);
            }

            $resultCollaborative = $this->libraryCollaborativeFilter->getRecommendations($matrixSimilarity, $matrixSites, $uid);

            foreach ($resultCollaborative as $keySiteId => $valueSiteRating) {
                // if($valueSiteRating >= 4){
                if($valueSiteRating > 3){
                    $goodSiteData = $this->modelSites->getSiteAllDataById(str_replace("id", "", $keySiteId), $usrLatitude, $usrLongitude, round($valueSiteRating, 2));
                    $goodSiteData->myQualification = $this->modelRatings->getOnlySiteRatingByUser($uid, $goodSiteData->siteId);
                    $goodSiteData->isMyFavorite = $this->modelSites->getIsMyFavoriteSite($uid, $goodSiteData->siteId);
                    array_push($goodRatings, $goodSiteData);
                    $whereOtherSites == "" ? $whereOtherSites .= "(sites.siteId!=".str_replace("id", "", $keySiteId) : $whereOtherSites .= " AND sites.siteId!=".str_replace("id", "", $keySiteId);
                }
            }
        }
        $itemsSitesVisited = $this->modelSites->getUserVisitedSitesOnlySitesId($uid);

        foreach ($itemsSitesVisited as $objVisitedSite) {
            $whereOtherSites == "" ? $whereOtherSites .= "(sites.siteId!=".$objVisitedSite->siteId : $whereOtherSites .= " AND sites.siteId!=".$objVisitedSite->siteId;
        }
        if($whereOtherSites!=""){
            $whereOtherSites .= ")";
        }

        $otherSites = $this->modelSites->getOtherSitesWithSubcategoriesAndLocation($whereOtherSites, $whereSubcategories, $maxKmsDistance, $usrLatitude, $usrLongitude);
        foreach ($otherSites as $otherSite) {
            isset($otherSite->distance) ? $otherSite->distance = round($otherSite->distance, 1) : $otherSite->distance=null;
            // $otherSite->qualification = round($otherSite->qualification, 2);
            $otherSite->qualification == 0 ? $otherSite->qualification = 0 : $otherSite->qualification = round($otherSite->qualification,1);
            $otherSite->myQualification = $this->modelRatings->getOnlySiteRatingByUser($uid, $otherSite->siteId);
            $otherSite->isMyFavorite = $this->modelSites->getIsMyFavoriteSite($uid, $otherSite->siteId);
        }
        (checkIsEmpty($goodRatings)) ? $sitesRecommend = $otherSites : $sitesRecommend = array_merge($goodRatings, $otherSites);
        return $sitesRecommend;
    }

    /**
    * @api {POST} sites/getUserVisitedSites/ getUserVisitedSites
    * @apiVersion 1.0.0
    * @apiName getUserVisitedSites
    * @apiGroup sites
    * @apiDescription get visited sites
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {Number} cityId filter city identifier
    * @apiParam {String} maxKmsDistance maximum distance in kilometers
    * @apiParam {String} usrLatitude user lantitude
    * @apiParam {String} usrLongitude user longitude
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getUserVisitedSites(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        $cityId = $request['cityId'];
        $maxKmsDistance = $request['maxKmsDistance'];
        $usrLatitude = $request['usrLatitude'];
        $usrLongitude = $request['usrLongitude'];
        $limit = $request['limit'];
        $offset = $request['offset'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }
        if (!checkIsEmpty($cityId)) {
            $cityData = $this->modelCities->existCity($cityId);
            if (checkIsEmpty($cityData)) {
                // return self::sendResponseError('Esta ciudad no existe.');
                return self::sendResponse([], 150, 'Esta ciudad no existe.');
            }
        }

        try{

            $userVisitedSites = $this->modelSites->getUserVisitedSites($uid, $limit, $offset, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude);
            if (checkIsEmpty($userVisitedSites)) {
                // return self::sendResponseError('No hay sitios visitados.');
                return self::sendResponse([], 150, 'No hay sitios visitados.');
            }
            $totalUserVisitedSites = $this->modelSites->countUserVisitedSites($uid, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude);

            return $this->sendResponse(
                [
                    "sites" => $userVisitedSites,
                    "total" => $totalUserVisitedSites
                ],
                100, 
                'operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} sites/countUserVisitedSites/ countUserVisitedSites
    * @apiVersion 1.0.0
    * @apiName countUserVisitedSites
    * @apiGroup sites
    * @apiDescription count user visited sites
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} myUid user identifier
    *
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function countUserVisitedSites(){
        $request = self::getRequest(false);

        $myUid = $request['myUid'];

        // validations
        if (checkIsEmpty($myUid)) {
            // return self::sendResponseError('El identificador del usuario esta vacío');
            return self::sendResponse([], 150, 'El identificador del usuario esta vacío');
        }
        $fromUserData = $this->modelUsers->getUserDataByUid($myUid);
        if (checkIsEmpty($fromUserData)) {
            // return self::sendResponseError('Este usuario no existe');
            return self::sendResponse([], 150, 'Este usuario no existe');
        }

        try{
            $totalUserVisitedSites = $this->modelSites->countUserVisitedSites($myUid, "", "", "", "");
            return $this->sendResponse(
                [
                    "countUserVisitedSites" => $totalUserVisitedSites
                ],
                100, 
                'Operación exitosa'
            );
        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} sites/getRecentsSites/ getRecentsSites
    * @apiVersion 1.0.0
    * @apiName getRecentsSites
    * @apiGroup sites
    * @apiDescription get recents sites
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {Number} cityId filter city identifier
    * @apiParam {String} maxKmsDistance maximum distance in kilometers
    * @apiParam {String} usrLatitude user lantitude
    * @apiParam {String} usrLongitude user longitude
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getRecentsSites(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        $cityId = $request['cityId'];
        $maxKmsDistance = $request['maxKmsDistance'];
        $usrLatitude = $request['usrLatitude'];
        $usrLongitude = $request['usrLongitude'];
        $limit = $request['limit'];
        $offset = $request['offset'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        if (!checkIsEmpty($cityId)) {
            $cityData = $this->modelCities->existCity($cityId);
            if (checkIsEmpty($cityData)) {
                // return self::sendResponseError('Esta ciudad no existe.');
                return self::sendResponse([], 150, 'Esta ciudad no existe.');
            }
        }

        try{
            $recentsSites = $this->modelSites->getRecentsSites($uid, $limit, $offset, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude);
            if (checkIsEmpty($recentsSites)) {
                // return self::sendResponseError('No hay sitios para mostrar.');
                return self::sendResponse([], 150, 'No hay sitios para mostrar.');
            }
            $totalRecentSites = $this->modelSites->countRecentSites($cityId, $maxKmsDistance, $usrLatitude, $usrLongitude);

            return $this->sendResponse(
                [
                    "sites" => $recentsSites,
                    "total" => $totalRecentSites
                ],
                100, 
                'operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} sites/getMyFavoritesSites/ getMyFavoritesSites
    * @apiVersion 1.0.0
    * @apiName getMyFavoritesSites
    * @apiGroup sites
    * @apiDescription get my favorites sites
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {Number} cityId filter city identifier
    * @apiParam {String} maxKmsDistance maximum distance in kilometers
    * @apiParam {String} usrLatitude user lantitude
    * @apiParam {String} usrLongitude user longitude
    * @apiParam {Number} limit limit rows per page
    * @apiParam {Number} offset offset to rows on page
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getMyFavoritesSites(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        $cityId = $request['cityId'];
        $maxKmsDistance = $request['maxKmsDistance'];
        $usrLatitude = $request['usrLatitude'];
        $usrLongitude = $request['usrLongitude'];
        $limit = $request['limit'];
        $offset = $request['offset'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        if (!checkIsEmpty($cityId)) {
            $cityData = $this->modelCities->existCity($cityId);
            if (checkIsEmpty($cityData)) {
                // return self::sendResponseError('Esta ciudad no existe.');
                return self::sendResponse([], 150, 'Esta ciudad no existe.');
            }
        }

        try{
            $myFavoritesSites = $this->modelSites->getMyFavoritesSites($uid, $limit, $offset, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude);
            if (checkIsEmpty($myFavoritesSites)) {
                // return self::sendResponseError('No hay sitios para mostrar.');
                return self::sendResponse([], 150, 'No hay sitios para mostrar.');
            }
            $totalMyFavoritesSites = $this->modelSites->countMyFavoritesSites($uid, $cityId, $maxKmsDistance, $usrLatitude, $usrLongitude);

            return $this->sendResponse(
                [
                    "sites" => $myFavoritesSites,
                    "total" => $totalMyFavoritesSites
                ],
                100, 
                'operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }

    /**
    * @api {POST} sites/getSiteData/ getSiteData
    * @apiVersion 1.0.0
    * @apiName getSiteData
    * @apiGroup sites
    * @apiDescription get site data
    *
    * @apiHeader {String} X-TP-Auth-Token Header access token.
    *
    * @apiParam {String} uid user identifier
    * @apiParam {Number} siteId site identifier
    * @apiParam {String} usrLatitude user lantitude
    * @apiParam {String} usrLongitude user longitude
    * @apiSuccess {Number} code 200: success ok <br> != 100: error in service
    */
    public function getSiteData(){

        $request = self::getRequest(false);

        $uid = $request['uid'];
        $siteId = $request['siteId'];
        $usrLatitude = $request['usrLatitude'];
        $usrLongitude = $request['usrLongitude'];

        // validations
        if (checkIsEmpty($uid)) {
            // return self::sendResponseError('el identificador de usuario esta vacío.');
            return self::sendResponse([], 150, 'el identificador de usuario esta vacío.');
        }
        $userData = $this->modelUsers->getUserDataByUid($uid);
        if (checkIsEmpty($userData)) {
            // return self::sendResponseError('Este usuario no existe.');
            return self::sendResponse([], 150, 'Este usuario no existe.');
        }

        if (checkIsEmpty($siteId)) {
            // return self::sendResponseError('el identificador del sitio esta vacío.');
            return self::sendResponse([], 150, 'El identificador del sitio esta vacío.');
        }
        

        try{
            $siteData = $this->modelSites->getSiteAllDataById($siteId, $usrLatitude, $usrLongitude);
            if (checkIsEmpty($siteData)) {
                // return self::sendResponseError('Este sitio no existe.');
                return self::sendResponse([], 150, 'Este sitio no existe.');
            }
            $siteData->myQualification = $this->modelRatings->getOnlySiteRatingByUser($uid, $siteData->siteId);
            $siteData->qualification == 0 ? $siteData->qualification = 0 : $siteData->qualification = round($siteData->qualification,1);
            return $this->sendResponse(
                ["siteData" => $siteData],
                100, 
                'operación exitosa'
            );

        } catch (Exception $ex) {
            // return self::sendResponseError($ex->getMessage());
            return self::sendResponse([], 150, $ex->getMessage());
        }
    }
}