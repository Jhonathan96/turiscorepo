<?php

namespace Config;


// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
// Kubo URLS
if (file_exists(APPPATH . 'Libraries/Kubo/Controller/Core/Routes.php')) {
	require APPPATH . 'Libraries/Kubo/Controller/Core/Routes.php';
	new \Kubo\Core\Routes($routes);
}
# Another URLS

// Group user
$routes->group('v1/user', function($routes){
	$routes->add('updateProfile', 'v1/UserController::updateProfile');
	$routes->add('getRecommendedUsers', 'v1/UserController::getRecommendedUsers');
	$routes->add('getRecentUsers', 'v1/UserController::getRecentUsers');
	$routes->add('getUserData', 'v1/UserController::getUserData');
});

// Group categories 
$routes->group('v1/categories', function($routes){
	$routes->add('getAllCategories', 'v1/CategoriesController::getAllCategories');
});

// Group subcategories 
$routes->group('v1/subcategories', function($routes){
	$routes->add('getAllSubcategories', 'v1/SubcategoriesController::getAllSubcategories');
	$routes->add('getSubcategoriesByCategoryId', 'v1/SubcategoriesController::getSubcategoriesByCategoryId');
	$routes->add('getSubcategoriesOrganizedByCategory', 'v1/SubcategoriesController::getSubcategoriesOrganizedByCategory');
});

// Group userSubcategories 
$routes->group('v1/userSubcategories', function($routes){
	$routes->add('getUserSubcategoriesByUid', 'v1/UserSubcategoriesController::getUserSubcategoriesByUid');
	$routes->add('userSubcategoriesRegistry', 'v1/UserSubcategoriesController::userSubcategoriesRegistry');
});

// Group cities
$routes->group('v1/cities', function($routes){
	$routes->add('getAllCities', 'v1/CitiesController::getAllCities');
});

// Group ratings
$routes->group('v1/ratings', function($routes){
	$routes->add('createSiteRating', 'v1/RatingsController::createSiteRating');
	$routes->add('getAllSiteRatings', 'v1/RatingsController::getAllSiteRatings');
	$routes->add('getSiteRatingByUser', 'v1/RatingsController::getSiteRatingByUser');
});

// Group sites
$routes->group('v1/sites', function($routes){
	$routes->add('getRecommendSites', 'v1/SitesController::getRecommendSites');
	$routes->add('getUserVisitedSites', 'v1/SitesController::getUserVisitedSites');
	$routes->add('getRecentsSites', 'v1/SitesController::getRecentsSites');
	$routes->add('getMyFavoritesSites', 'v1/SitesController::getMyFavoritesSites');
	$routes->add('getSiteData', 'v1/SitesController::getSiteData');
	$routes->add('countUserVisitedSites', 'v1/SitesController::countUserVisitedSites');
});

// Group favorite sites
$routes->group('v1/favoriteSites', function($routes){
	$routes->add('createOrDeleteFavoritesSite', 'v1/FavoriteSitesController::createOrDeleteFavoritesSite');
	$routes->add('countUserFavoriteSites', 'v1/FavoriteSitesController::countUserFavoriteSites');
});

// Group user friendship
$routes->group('v1/userFriendship', function($routes){
	$routes->add('sendFriendRequest', 'v1/UserFriendshipController::sendFriendRequest');
	$routes->add('cancelFriendRequest', 'v1/UserFriendshipController::cancelFriendRequest');
	$routes->add('acceptFriendRequest', 'v1/UserFriendshipController::acceptFriendRequest');
	$routes->add('cancelFriendship', 'v1/UserFriendshipController::cancelFriendship');
	$routes->add('blockUser', 'v1/UserFriendshipController::blockUser');
	$routes->add('unblockUser', 'v1/UserFriendshipController::unblockUser');
	$routes->add('countFriendRequest', 'v1/UserFriendshipController::countFriendRequest');
	$routes->add('friendRequestList', 'v1/UserFriendshipController::friendRequestList');
	$routes->add('friendsList', 'v1/UserFriendshipController::friendsList');
	$routes->add('countAllFriends', 'v1/UserFriendshipController::countAllFriends');
	$routes->add('blockedUsersList', 'v1/UserFriendshipController::blockedUsersList');
	$routes->add('blockedUsersList', 'v1/UserFriendshipController::blockedUsersList');
});

// Group feedback
$routes->group('v1/feedback', function($routes){
	$routes->add('createFeedback', 'v1/FeedbackController::createFeedback');
});