# CodeIgniter 4 - Kubo S.A.S.


El siguiente README da a conocer las bases del Stack de Kubo.

## Servicios / Endpoints

https://www.getpostman.com/collections/ce8b89356a1dd1d65d3c

---
## Correos

Generador de credenciales para correos temporales
https://mailtrap.io/inboxes

Se modifican estos datos en: `app/Libraries/Kubo/Controller/v1/Email.php`

---

Configuración Base:

**ENV**
Dentro del archivo .env modificar los siguientes datos sea el caso.

```
# development testing production
CI_ENVIRONMENT = development

# APP
app.baseURL = 'http://localhost:8080'


# DATABASE

#database.default.hostname = localhost
#database.default.database = kubo
#database.default.username = root
#database.default.password = 
#database.default.DBDriver = MySQLi
#database.default.port = 3306

# AWS
# database.default.hostname = storent-dev.cwb5ajusgrsr.us-east-1.rds.amazonaws.com
# database.default.database = tmp
# database.default.username = admin
# database.default.password = storent2019*
# database.default.encrypt.ssl_capath = ./app/Libraries/Kubo/cert/aws.pem

```

Ir al archivo de Codeigniter: `app/Config/Routes.php` aniadir las siguientes lineas al final del archivo.

```
// Kubo URLS
if (file_exists(APPPATH . 'Libraries/Kubo/Controller/Core/Routes.php')) {
	require APPPATH . 'Libraries/Kubo/Controller/Core/Routes.php';
	new \Kubo\Routes($routes);
}
# Another URLS
```

Ir al archivo de Codeigiter: `app/Controllers/BaseController.php` modificar los siguientes datos, MANTENER los datos que tenga BaseController, simplemente extender a la libreria y llamar el parent en el constructor:

```
class BaseController extends \Kubo\Core\Kubo
{

	public function __construct() {
		parent::__construct();
	}
}
```

Digitar los comandos:

```
composer install
composer dump-autoload
php spark migrate
php spark db:seed DatabaseSeeder
php spark serve
```

----

Se genera el Access token para utilizar los servicios de Onboarding tal como:

* Login
* Signup
* Validate Email
* Recovery Password
* Change Password

Y a partir de realizar login se genera automaticamente un Auth Token el cual funciona para los demas endpoints
```
{
    "code": 100,
    "data": {
        "user": {
            "uid": "c3c814c8b710f5c669aa7cbfe3573f596c8b71c85e5d63951103a",
            "name": "Miguel Torres",
            "email": "mtorres@kubo.co",
            "password": "$2y$10$N6abzMCErg2ZNT0DrbwSYO7ZbnFdMtPEgMTd/jrchTKOF1Uj7E0M6",
            "tries": "0",
            "valid": "1",
            "hasCompany": "0",
            "role": {
                "roleId": 3,
                "name": "valid"
            }
        },
        "authToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY3R1YWxUaW1lIjoxNTgzMTc5MDYwLCJleHBpcmVBY2Nlc3MiOjE1ODMyNjI5NjUsImV4cGlyZUF1dGgiOjE1ODMyNjU0NTgsIm1heERhdGUiOiIyMDIwLTAzLTAyIDE4OjU3OjM4IiwiZGF0YSI6eyJhY2Nlc3MiOiJ4bFY3U1VsNE1kSUI3ODJ5VWxVWGpHdGpZWFJUSUMwZ2IySjFTdz09IiwidWlkIjoiYzNjODE0YzhiNzEwZjVjNjY5YWE3Y2JmZTM1NzNmNTk2YzhiNzFjODVlNWQ2Mzk1MTEwM2EiLCJ1c2VyIjp7ImV4cGlyZUFjY2VzcyI6MTU4MzI2Mjk2NSwiYWN0dWFsVGltZSI6MTU4MzE3OTA2MCwiZXhwaXJlQXV0aCI6MTU4MzI2NTQ1OCwibWF4RGF0ZSI6IjIwMjAtMDMtMDIgMTg6NTc6MzgiLCJwbGF0Zm9ybUlkIjoxLCJ2ZXJzaW9uIjoiMS4yIiwidWlkIjoiYzNjODE0YzhiNzEwZjVjNjY5YWE3Y2JmZTM1NzNmNTk2YzhiNzFjODVlNWQ2Mzk1MTEwM2EiLCJuYW1lIjoiTWlndWVsIFRvcnJlcyIsImVtYWlsIjoibXRvcnJlc0BrdWJvLmNvIiwicGFzc3dvcmQiOiIkMnkkMTAkTjZhYnpNQ0VyZzJaTlQwRHJid1NZTzdaYm5GZE10UEVnTVRkXC9qcmNoVEtPRjFVajdFME02IiwidHJpZXMiOiIwIiwidmFsaWQiOiIxIiwiaGFzQ29tcGFueSI6IjAiLCJyb2xlIjp7InJvbGVJZCI6IjMiLCJuYW1lIjoidmFsaWQiLCJzdGF0dXMiOiJBQ1RJVkUiLCJ1cGRhdGVkX2F0IjoiMjAyMC0wMy0wMiAxMzo0MzoxNSIsImNyZWF0ZWRfYXQiOiIyMDIwLTAzLTAyIDEzOjQzOjE1IiwiZGVsZXRlZF9hdCI6IjAwMDAtMDAtMDAgMDA6MDA6MDAifX19fQ.Yf5zZd4NFqrKTTfyi8xaL9kdaJL0_XcQmn73qQNVimQ"
    }
}
```

# Status, Código y Mensaje
## Casos correctos:
Status: 200, Códigos en rango de 100 a 299 
Mensaje: (Opcional)
Data: (Opcional) 

Ejemplo: Status 200
```
{
    "code": 100,
    "message": "El correo mtorres@kubo.co existe en la base de Datos "
}
```
## Casos Erroneos
Status: 400, Códigos en rango de 300 a 499 
Mensaje: (Opcional)
Data: (Opcional) 
Ejemplo: Status 400
```
{
    "code": 300,
    "message": "El correo electrónico NO existe en la base de datos."
}
```

Dependiendo el caso el mensaje y la data se pueden enviar a la vez o uno y uno separados (depende del back como lo desee enviar)

---

Si el endpoint solicita de alguna o varias imagenes/archivos se envia los datos en formdata de lo contrario se envia con formato json
Ejemplo: https://documenter.getpostman.com/view/8586244/SzKbLuu7?version=latest#a64781f6-a0b4-4837-9a8e-01cee38ef700

